{-# OPTIONS_GHC-fwarn-incomplete-patterns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}

module Language.Bash.Parser where

-- https://www.tldp.org/LDP/abs/html/opprecedence.html
-- https://ryanstutorials.net/bash-scripting-tutorial/bash-arithmetic.php

import           Control.Applicative            hiding (many)
import           Control.Monad
import           Control.Monad.Combinators.Expr
import           Data.Text                      (Text)
import qualified Data.Text                      as T
import qualified Data.Text.IO                   as TIO
import           Data.Void
import           Language.Bash.AST
import           Text.InterpolatedString.QM
import           Text.Megaparsec                hiding (State)
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer     as L
import           Text.Megaparsec.Debug
import           Text.Pretty.Simple             (pPrint)


-- | Parser synonym
type Parser = Parsec Void Text

-- | Space conAdditioner
sc :: Parser ()
sc = L.space
    space1
    (L.skipLineComment "#")
    empty

-- | lexeme is a wrapper for lexemes
-- that picks up all trailing white space
-- using the supplied space conAdditioner.
lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

-- | symbol is a parser that matches given text
-- using string internally and then
-- similarly picks up all trailing white space.
symbol :: Text -> Parser Text
symbol = L.symbol sc

-- | Parse integer
pInteger :: Parser Expr
pInteger = Int <$> lexeme L.decimal

-- | Parse variable
pVariable :: Parser Expr
pVariable = Var <$> lexeme
    ((:) <$> (try (char '$' *> letterChar) <|> letterChar) <*> many alphaNumChar)

-- | Parse command line variable (to function or global for program)
pCommandLineVariable :: Parser Expr
pCommandLineVariable = CommandLineVar <$> lexeme (char '$' *> L.decimal)

-- | Parse parens
parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

-- | Term parser
pTerm :: Parser Expr
pTerm = try pInteger
    <|> try pVariable
    <|> try pCommandLineVariable
    <|> parens pExpr

-- | Parse arithmetic expression
pExpr :: Parser Expr
pExpr = makeExprParser pTerm operatorTable

-- | Arithmetic operators
-- ordered in descending precedence
operatorTable :: [[Operator Parser Expr]]
operatorTable =
  [
    [ prefix "!" LogicalNegation
    , prefix "-" Negation
    , prefix "+" id
    ]
  , [ binary "*" Multiplication
    , binary "/" Division
    , binary "%" Modulo
    ]
  , [ binary "+" Addition
    , binary "-" Subtraction
    ]
  ,
    [ binary ">=" GreaterOrEqual
    , binary ">" Greater
    , binary "<=" LessOrEqual
    , binary "<" Less
    ]
  ,
    [ binary "==" Equality
    , binary "!=" Inequality
    ]
  ,
    [ binary "&&" LogicalAnd
    ]
  ,
    [ binary "||" LogicalOr
    ]
  ]

-- | Parse binary operation
binary :: Text -> (Expr -> Expr -> Expr) -> Operator Parser Expr
binary  name f = InfixL  (f <$ symbol name)

-- | Parse prefix or postfix operation
prefix, postfix :: Text -> (Expr -> Expr) -> Operator Parser Expr
prefix  name f = Prefix  (f <$ symbol name)
postfix name f = Postfix (f <$ symbol name)

-- | Parse strict assignment (eg 'a=1+2')
pStrictAssign :: Parser Expr
pStrictAssign = do
  assign <- pVariable
  void (char '=')
  result <- pExpr
  return (ArithmeticAssignment assign result)

-- | Parse assignment (eg 'a = 1 + - 1')
pAssign :: Parser Expr
pAssign = do
  assign <- pVariable
  void (symbol "=")
  result <- pExpr
  return (ArithmeticAssignment assign result)

-- | Parse integer assignment (eg 'a=42')
pIntAssign :: Parser Expr
pIntAssign = do
  assign <- pVariable
  void (char '=')
  result <- pInteger <* sc
  return (ArithmeticAssignment assign result)

-- | Parse let assignment (eg 'let "a = 42"' or 'let a=42')
pLetAssign :: Parser Expr
pLetAssign = try (between (symbol "let \"") (symbol "\"") pAssign)
         <|> try ((symbol "let ") *> pStrictAssign)

-- | Parse Double Parentheses assignment (eg 'a=$(( 7 * 6 ))')
pDoubleParenthesesAssign :: Parser Expr
pDoubleParenthesesAssign = do
  assign <- pVariable
  result <- between (symbol "=$((") (symbol "))") pExpr
  return (ArithmeticAssignment assign result)

-- | Parse quoted expr (eg 'expr "7 * 6"')
pQuotedExpr :: Parser Expr
pQuotedExpr = between (symbol "expr \"") (symbol "\"") pExpr

-- | Parse unquoted expr (eg 'expr 7 * 6')
pExpr' :: Parser Expr
pExpr' = (symbol "expr ") *> pExpr

-- | Parse quoted string (eg '"Hello world!"')
stringLiteral :: Parser Expr
stringLiteral = String <$> (char '\"' *> manyTill L.charLiteral (char '\"'))

-- | Parse string assignment (eg 'a="Hello Kakadu!"')
pStringAssign :: Parser Expr
pStringAssign = do
  assign <- pVariable
  void (char '=')
  result <- stringLiteral
  return (StringAssignment assign result)

-- | Parse if statement (eg 'if (( 1 < 2 )); then echo 1; fi')
pIf :: Parser Expr
pIf = do
  void (symbol "if ")
  cond <- between (symbol "((") (string "))") pExpr
  void (try (symbol ";") <|> symbol "")
  body <- between (symbol "then") (string "fi") parseAll
  return (If cond body)

-- | Parse if-else statement (eg 'if (( 1 < 2 )); then echo 1; else echo 2; fi')
pIfElse :: Parser Expr
pIfElse = do
  void (symbol "if ")
  cond <- between (symbol "((") (string "))") pExpr
  void (try (symbol ";") <|> symbol "")
  void (symbol "then")
  ifBody <- parseAll
  void (symbol "else")
  elseBody <- parseAll
  void (symbol "fi")
  return (IfElse cond ifBody elseBody)

-- | Parse while loop (eg 'while (( $i < 10 )) do ... done')
pWhile :: Parser Expr
pWhile = do
  void (symbol "while ")
  cond <- between (symbol "((") (symbol "))") pExpr
  void (try (symbol ";") <|> symbol "")
  body <- between (symbol "do") (symbol "done") parseAll
  return (While cond body)

-- | Parse echo (eg 'echo 42')
pEcho' :: Parser Expr
pEcho' = do
  void (symbol "echo ")
  body <- try (stringLiteral)
      <|> try (pExprs)
  return (Echo body)

pEcho :: Parser Expr
pEcho = between (symbol "(") (symbol ")") pEcho'

-- | Parse list of exprs for 'echo $a $b $1'
pExprs :: Parser Expr
pExprs = List <$> sepBy (try pExpr <|> stringLiteral) sc

pExprs' :: Parser Expr
pExprs' = List <$> sepBy (between (symbol "\"") (symbol "\"") pExpr) sc

-- | Parse function declaration (eg 'function main {...}')
pFunctionDecl :: Parser Expr
pFunctionDecl = do
  void (symbol "function ")
  name <- pVariable
  void (symbol "{")
  body <- parseAll
  void (symbol "}")
  return (FunctionDecl name body)

-- | Parse content of function call
pFunctionCall' :: Parser Expr
pFunctionCall' = do
  name <- pVariable
  args <- pExprs
  return (FunctionCall name args)

-- | Parse function call (eg '(sayHelloToKakadu)')
pFunctionCall :: Parser Expr
pFunctionCall = between (symbol "(") (symbol ")") pFunctionCall'

-- | Main function to parse whole text
parseAll :: Parser Expr
parseAll = List <$> sepEndBy parse' (try (symbol ";") <|> symbol "")

-- | Parse one of Bash commands
parse' :: Parser Expr
parse' = try pAssign
     <|> try pIntAssign
     <|> try pLetAssign
     <|> try pQuotedExpr
     <|> try pExpr'
     <|> try pDoubleParenthesesAssign
     <|> try pStringAssign
     <|> try pIfElse
     <|> try pIf
     <|> try pWhile
     <|> try pEcho
     <|> try pFunctionDecl
     <|> try pFunctionCall
