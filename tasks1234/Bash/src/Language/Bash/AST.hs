{-# OPTIONS_GHC-fwarn-incomplete-patterns #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE RecordWildCards    #-}

module Language.Bash.AST where

import           Data.Data     (Data)
import           Data.Typeable (Typeable)
import           GHC.Generics  (Generic)
import           Prelude

-- | Bash abstract syntax tree
data Expr
    -- | Bash variable. May be with '$' or without
    = Var String
    -- | Bash command line variable (eg '$1')
    | CommandLineVar Int
    -- | Bash while loop
    | While Expr Expr
    -- | Bash function declaration
    | FunctionDecl Expr Expr
    -- | Bash function call
    | FunctionCall Expr Expr
    -- | Bash echo command
    | Echo Expr
    -- | Bash arithmetic assignment (eg 'a=$(( 7 * 6 ))')
    | ArithmeticAssignment Expr Expr
    -- | Bash string assignment (eg 'a="Hello Kakadu!"')
    | StringAssignment Expr Expr
    -- | Bash if statement
    | If Expr Expr
    -- | Bash if-else statement
    | IfElse Expr Expr Expr
    -- | List of Bash commands
    | List [Expr]
    -- | Bash Int
    | Int Int
    -- | Bash String
    | String String
    -- | Bash arithmetic operations
    | Negation       Expr
    | Addition       Expr Expr
    | Subtraction    Expr Expr
    | Multiplication Expr Expr
    | Division       Expr Expr
    | Modulo         Expr Expr
    -- | Bash comparison operations
    | Greater        Expr Expr
    | Less           Expr Expr
    | GreaterOrEqual Expr Expr
    | LessOrEqual    Expr Expr
    | Equality       Expr Expr
    | Inequality     Expr Expr
    -- | Bash logical operations
    | LogicalNegation Expr
    | LogicalAnd      Expr Expr
    | LogicalOr       Expr Expr
    deriving (Data, Eq, Read, Show, Typeable, Generic)
