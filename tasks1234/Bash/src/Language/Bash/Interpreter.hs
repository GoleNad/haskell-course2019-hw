{-# OPTIONS_GHC-fwarn-incomplete-patterns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}

module Language.Bash.Interpreter where

import           Data.IORef
import           Data.List
import qualified Data.Text            as T
import           Data.Typeable
import           Language.Bash.AST
import           Language.Bash.Parser
import           System.Exit
import           Text.Megaparsec      hiding (State)
import           Text.Pretty.Simple   (pPrint)

iExpr' :: [(Expr, Expr)] -> Expr -> Maybe Int
iExpr' vars expr = case expr of
    Greater l r        -> (\x y -> min (max 0 (x - y)) 1) <$> (iExpr' vars l) <*> (iExpr' vars r)
    GreaterOrEqual l r -> (\x y -> min (max 0 (x - y + 1)) 1) <$> (iExpr' vars l) <*> (iExpr' vars r)
    Less l r           -> (iExpr' vars (Greater r l))
    LessOrEqual l r    -> (iExpr' vars (GreaterOrEqual r l))
    Equality l r       -> (iExpr' vars (LogicalAnd (GreaterOrEqual l r) (GreaterOrEqual r l)))
    Inequality l r     -> (iExpr' vars (LogicalNegation (Equality l r)))
    LogicalNegation l  -> (iExpr' vars (LessOrEqual (Greater l (Int 0)) (Int 0)))
    LogicalOr l r      -> (\x y -> min (max 0 (x + y)) 1) <$> (iExpr' vars (Greater l (Int 0))) <*> (iExpr' vars (Greater r (Int 0)))
    LogicalAnd l r     -> (iExpr' vars (LogicalNegation (LogicalOr (LogicalNegation l) (LogicalNegation r))))
    Var expr'          -> case lookup (Var expr') vars of
        Just (Int x) -> Just x
        otherwise    -> Nothing
    CommandLineVar expr' -> case lookup (CommandLineVar expr') vars of
        Just (Int x) -> Just x
        otherwise    -> Nothing
    Negation r         -> (-) <$> (Just 0) <*> (iExpr' vars r)
    Addition l r       -> (+) <$> (iExpr' vars l) <*> (iExpr' vars r)
    Subtraction l r    -> (-) <$> (iExpr' vars l) <*> (iExpr' vars r)
    Multiplication l r -> (*) <$> (iExpr' vars l) <*> (iExpr' vars r)
    Division l r       -> if ((iExpr' vars r) /= Just 0) then (div) <$> (iExpr' vars l) <*> (iExpr' vars r) else Nothing
    Modulo l r         -> if ((iExpr' vars r) /= Just 0) then (mod) <$> (iExpr' vars l) <*> (iExpr' vars r) else Nothing
    Int x              -> Just x
    otherwise          -> Nothing

iExpr :: [(Expr, Expr)] -> [(Expr, Expr)] -> [(Expr, Expr)] -> [Expr] -> IO ([(Expr, Expr)])
iExpr vars funcs global []           = return vars
iExpr vars funcs global (expr:exprs) = case expr of
    ArithmeticAssignment l r -> case (iExpr' vars r) of
        Just res  -> case lookup l vars of
            Just smth -> (iExpr ((delete (l, smth) vars) ++ [(l, (Int res))]) funcs global exprs)
            otherwise -> (iExpr (vars ++ [(l, (Int res))]) funcs global exprs)
        otherwise -> fail "Error in arithmetic assignment"
    StringAssignment l r -> case lookup l vars of
        Just smth -> (iExpr ((delete (l, smth) vars) ++ [(l, r)]) funcs global exprs)
        otherwise -> (iExpr (vars ++ [(l, r)]) funcs global exprs)
    Echo (String s) -> do
        putStrLn s
        iExpr vars funcs global exprs
    Echo (List e) -> do
        iEcho vars e
        iExpr vars funcs global exprs
    List e -> do
        vars' <- iExpr vars funcs global e
        iExpr vars' funcs global exprs
    If cond (List body) -> do
        case (iExpr' vars cond) of
            Just 1 -> do
                vars' <- iExpr vars funcs global body
                iExpr vars' funcs global exprs
            Just 0 -> do
                iExpr vars funcs global exprs
            otherwise -> fail "Error in if statement"
    IfElse cond (List ifBody) (List elseBody) -> do
        case (iExpr' vars cond) of
            Just 1 -> do
                vars' <- iExpr vars funcs global ifBody
                iExpr vars' funcs global exprs
            Just 0 -> do
                vars' <- iExpr vars funcs global elseBody
                iExpr vars' funcs global exprs
            otherwise -> fail "Error in if-else statement"
    While cond (List body) -> do
        case (iExpr' vars cond) of
            Just 1 -> do
                vars' <- iExpr vars funcs global body
                iExpr vars' funcs global  (expr:exprs)
            Just 0 -> do
                iExpr vars funcs global exprs
            otherwise -> fail "Error in while loop"
    FunctionDecl name body -> do
        iExpr vars (funcs ++ [(name, body)]) global exprs
    FunctionCall name (List args) -> case lookup name funcs of
        Just (List body) -> do
            vars' <- iExpr (addArgs vars args 1) funcs global body
            iExpr ((filter (not . isCommandLineVar) vars') ++ global) funcs global exprs
        otherwise -> fail $ "Error in function" ++ show name
    otherwise -> return vars

addArgs :: [(Expr, Expr)] -> [Expr] -> Int -> [(Expr, Expr)]
addArgs vars [] _ = vars
addArgs vars (arg:args) n = case lookup (CommandLineVar n) vars of
    Nothing -> case lookup arg vars of
        Just smth -> addArgs (vars ++ [(CommandLineVar n, smth)]) (args) (n + 1)
        Nothing   -> addArgs (vars) (args) (n + 1)
    Just smth -> case lookup arg vars of
        Just smth' -> addArgs ((delete (CommandLineVar n, smth) vars) ++ [(CommandLineVar n, smth')]) (args) (n + 1)
        Nothing   -> addArgs (vars) (args) (n + 1)

isCommandLineVar :: (Expr, Expr) -> Bool
isCommandLineVar x = case x of
    (CommandLineVar a, b) -> True
    otherwise             -> False

iEcho :: [(Expr, Expr)] -> [Expr] -> IO ()
iEcho vars [] = putStrLn ""
iEcho vars (expr:exprs) = do
    let cur = case expr of
            Int x -> show x
            String s -> s
            (Var x) -> case (lookup expr vars) of
                Just (Int y)    -> show y
                Just (String y) -> y
                otherwise       -> " "
            (CommandLineVar x) -> case (lookup expr vars) of
                Just (Int y)    -> show y
                Just (String y) -> y
                otherwise       -> " "
            otherwise -> " "
    putStr $ cur ++ " "
    iEcho vars exprs

iExprText :: [(Expr, Expr)] -> T.Text -> IO ([(Expr, Expr)])
iExprText globalArgs t = case parseMaybe parseAll t of
    Just (List x) -> (iExpr globalArgs [] globalArgs x)
    otherwise     -> fail "Error while parsing program"

argsToVars :: [String] -> [(Expr, Expr)] -> Int -> [(Expr, Expr)]
argsToVars [] vars _ = vars
argsToVars (arg:args) vars n = case parseMaybe pExpr (T.pack arg) of
    Just (Int x) -> argsToVars (args) (vars ++ [(CommandLineVar n, Int x)]) (n + 1)
    Just (String s) -> argsToVars (args) (vars ++ [(CommandLineVar n, String s)]) (n + 1)
    Just (Var x) -> argsToVars (args) (vars ++ [(CommandLineVar n, String $ show x)]) (n + 1)
    otherwise -> argsToVars (args) (vars) (n + 1)

iExprString :: [String] -> String -> IO ([(Expr, Expr)])
iExprString globalArgs s = iExprText (argsToVars globalArgs [] 1) (T.pack s)
