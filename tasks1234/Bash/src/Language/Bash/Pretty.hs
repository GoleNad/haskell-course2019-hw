{-# OPTIONS_GHC-fwarn-incomplete-patterns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}

module Language.Bash.Pretty where

import           Data.IORef
import           Data.List
import qualified Data.Text                 as T
import           Data.Typeable
import           Language.Bash.AST
import           Language.Bash.Interpreter
import           Language.Bash.Parser
import           System.Exit
import           Text.Megaparsec           hiding (State)
import           Text.Pretty.Simple        (pPrint)

prExpr' :: [(Expr, Expr)] -> Expr -> String
prExpr' vars expr = case expr of
    Int x        -> "( " ++ show x ++ " )"
    Var x         -> "$" ++ x
    CommandLineVar x -> "$" ++ show x
    String x -> "\"" ++ x ++ "\""
    Addition l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " + " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " + " ++ (prExpr' vars r) ++ " )"
    Multiplication l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " * " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " * " ++ (prExpr' vars r) ++ " )"
    Subtraction l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " - " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " - " ++ (prExpr' vars r) ++ " )"
    Division l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " / " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " / " ++ (prExpr' vars r) ++ " )"
    Modulo l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " % " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " % " ++ (prExpr' vars r) ++ " )"
    Greater l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " > " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " > " ++ (prExpr' vars r) ++ " )"
    GreaterOrEqual l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " >= " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " >= " ++ (prExpr' vars r) ++ " )"
    Less l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " < " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " < " ++ (prExpr' vars r) ++ " )"
    LessOrEqual l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " <= " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " <= " ++ (prExpr' vars r) ++ " )"
    Equality l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " == " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " == " ++ (prExpr' vars r) ++ " )"
    Inequality l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " != " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " != " ++ (prExpr' vars r) ++ " )"
    LogicalAnd l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " && " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " && " ++ (prExpr' vars r) ++ " )"
    LogicalOr l r -> if (iExpr' vars expr) == (iExpr' [] expr) then do
        case (iExpr' vars expr) of 
            Just x -> "( " ++ show x ++ " )"
            otherwise -> "( " ++ (prExpr' vars l) ++ " || " ++ (prExpr' vars r) ++ " )"
        else 
            "( " ++ (prExpr' vars l) ++ " || " ++ (prExpr' vars r) ++ " )"
    LogicalNegation l -> prExpr' vars (Equality l (Int 0))
    Negation r -> prExpr' vars (Subtraction (Int 0) r)

prExpr'' :: [Expr] -> String
prExpr'' [] = ""
prExpr'' [x] = prExpr' [] x
prExpr'' (expr:exprs) = (prExpr' [] expr) ++ " " ++ (prExpr'' exprs)
        
prExpr :: [(Expr, Expr)] -> String -> [Expr] -> String
prExpr vars _ []           = ""
prExpr vars prefix (expr:exprs) = case expr of
    ArithmeticAssignment (Var x) r -> (prefix ++ x ++ "=$(( ") ++ (prExpr' vars r) ++ " ))\n" ++ (prExpr vars prefix exprs)
    StringAssignment (Var x) (String s) -> (prefix ++ x ++ "=") ++ (prExpr' [] (String s)) ++ "\n" ++ (prExpr vars prefix exprs)
    Echo (String s) -> (prefix ++ "(echo ") ++ (prExpr' [] (String s)) ++ ")\n" ++ (prExpr vars prefix exprs)
    Echo (List e) -> (prefix ++ "(echo ") ++ (prExpr'' e) ++ ")\n" ++ (prExpr vars prefix exprs)  
    List e -> (prExpr vars (prefix ++ "  ") e) ++ (prExpr vars prefix exprs)
    If cond body -> prefix ++ "if (( " ++ (prExpr' vars cond) ++ " )); then\n" ++ (prExpr vars (prefix ++ "  ") [body]) ++
            prefix ++ "fi\n" ++ (prExpr vars prefix exprs)
    IfElse cond ifBody elseBody -> prefix ++ "if (( " ++ (prExpr' vars cond) ++ " )); then\n" ++ (prExpr vars (prefix ++ "  ") [ifBody]) ++ 
            prefix ++ "else\n" ++ (prExpr vars (prefix ++ "  ") [elseBody]) ++ prefix ++ "fi\n" ++ (prExpr vars prefix exprs)
    While cond body -> prefix ++ "while (( " ++ (prExpr' vars cond) ++ " )); do\n" ++ (prExpr vars (prefix ++ "  ") [body]) ++
            prefix ++ "done\n" ++ (prExpr vars prefix exprs)
    FunctionDecl (Var name) body -> ("function " ++ name ++ " {\n") ++ (prExpr vars (prefix ++ "  ") [body]) ++
            prefix ++ "}\n" ++ (prExpr vars prefix exprs)
    FunctionCall (Var name) (List e) -> prefix ++ "(" ++ name ++ " " ++ (prExpr'' e) ++ ")\n" ++ (prExpr vars prefix exprs)

prExprText :: [(Expr, Expr)] -> T.Text -> String
prExprText globalArgs t = case parseMaybe parseAll t of
    Just (List x) -> (prExpr globalArgs "" x)
    otherwise     -> fail "Error while parsing program"

prExprString :: [String] -> String -> String
prExprString globalArgs s = prExprText (argsToVars globalArgs [] 1) (T.pack s)