hello="Hello, Kakadu!"
(echo $hello)
function printRightEven {
    if (( ( ( $1 % ( 2 ) ) == ( 0 ) ) )); then
        a=$(( $1 ))
    else
        a=$(( ( $1 + ( 1 ) ) ))
    fi
    (echo $1 $a)
}
i=$(( $1 ))
n=$(( $2 ))
while (( ( $i <= $n ) )); do
    (printRightEven $i)
    i=$(( ( $i + ( 1 ) ) ))
done
goobye="Goodbye, Kakadu!"
(echo $goobye)
