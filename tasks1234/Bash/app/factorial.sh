function factorial {
    i=1; res=1
    while (( $i <= $1 )); do
        res=$(( res * $i ))
        i=$(( i + 1 ))
    done
    (echo $res)
}
(factorial $1)