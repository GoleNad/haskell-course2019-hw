module Parser where

import Text.Megaparsec
import Text.Megaparsec.Char
import Control.Monad (void)
import Control.Applicative (empty)
import Data.Void
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void String

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: String -> Parser String
symbol = L.symbol sc

sc :: Parser ()
sc = L.space (void (takeWhile1P Nothing f)) skipLineComment empty
  where
    f x = x == ' ' || x == '\t'

skipLineComment :: Parser ()
skipLineComment = L.skipLineComment "#"

storage :: Parser String
storage = (lexeme . try) (some hexDigitChar)

parseLabel :: Parser String
parseLabel = (lexeme . try) $ do
    l <- (:) <$> letterChar <*> many (alphaNumChar <|> char '_')
    _ <- symbol ":"
    return (l ++ ":")

instrList :: [String]
instrList =
          [ "mov"
          , "jmp"
          , "jne"
          , "add"
          , "sub"
          , "mul"
          , "cmp"
          ]

binInstr :: [String]
binInstr =
         [ "mov"
         , "add"
         , "sub"
         , "mul"
         , "cmp"
         ]

regList :: [String]
regList =
        [ "rax"
        , "rbx"
        , "rcx"
        , "rdx"
        , "rsi" 
        , "rdi" 
        , "rbp"
        , "rsp"
        , "eax"
        , "ebx"
        , "ecx"
        , "edx"
        , "esi"
        , "edi"
        , "ebp"
        , "esp"
        , "ax"
        , "bx"
        , "cx"
        , "dx"
        , "si"
        , "di"
        , "bp"
        , "sp"
        , "ah"
        , "bh"
        , "ch"
        , "dh"
        , "sil"
        , "dil"
        , "spl"
        , "bpl"
        , "al"
        , "bl"
        , "cl"
        , "dl"
        ]

parseInstr :: Parser String
parseInstr = (lexeme . try) $ do
    i <- some letterChar
    True <- return (i `elem` instrList)
    return i

parseInstr' :: Parser String
parseInstr' = (lexeme . try) $ do
    i <- some letterChar
    True <- return (i `elem` instrList)
    _ <- some asciiChar
    return i

register :: Parser String
register = (lexeme . try) $ do
    _ <- symbol "%"
    r <- some letterChar
    True <- return (r `elem` regList)
    return r

bool :: Parser String
bool = (lexeme . try) $ do
    b <- some letterChar
    True <- return (b == "True" || b == "False")
    return b

parseBinOp :: Parser [String]
parseBinOp = (lexeme . try) $ do
    i <- some letterChar
    True <- return (i `elem` binInstr) 
    sc
    o1 <- register 
    sc
    o2 <- storage <|> register
    return [i, o1, o2]

parseJmp :: Parser [String]
parseJmp = (lexeme . try) $ do
    i <- parseInstr
    True <- return (i == "jmp") 
    sc
    o1 <- parseLabel 
    return [i, o1]

parseJne :: Parser [String]
parseJne = (lexeme . try) $ do
    i <- parseInstr
    True <- return (i == "jne")
    sc
    o1 <- parseLabel 
    return [i, o1]

testParser :: IO ()
testParser = do
    parseTest parseLabel "start_:"
    parseTest storage "23Fb6"
    parseTest parseInstr "mov %rax 23Fb6"
    parseTest parseBinOp "mov %rax 23Fb6"
    parseTest parseBinOp "mov %rax %rbx"
    parseTest parseJmp "jmp start_:"
    parseTest parseJne "jne start_:"
