module Main where

import Parser


import Text.Megaparsec
import Data.Bits
import Data.Int
import Data.List
import Data.Maybe
import Data.Char
import Data.Tuple.Update
import Data.Tuple.Select

data Memory = Memory { registers :: (Int64, Int64, Int64, Int64, Int64, Int64, Int64, Int64)
                                     --rax|   rbx|   rcx|   rdx|   rsi|   rdi|   rbp|   rsp
                     , rip :: Int64
                     , labels :: [(String, Int64)]
                     , instructions :: [(String, Int64)]
                     , permission :: Bool
                     , cmpRes :: Bool
                     }

data Storage =
    Byte Int8
  | Word Int16
  | Double Int32
  | Quad Int64
  | Reg Int deriving Show

instance Show Memory where
  show mem = show (registers mem) ++ show (rip mem) ++ show (labels mem) ++ show (instructions mem) ++ show (permission mem) ++ show (cmpRes mem)

--start инициализирует экземпляр типа Мemory в начале исполнения
start :: Memory
start = Memory { registers = (0, 0, 0, 0, 0, 0, 0, 0)
               , rip = 0
               , labels = []
               , instructions = [] 
               , permission = True
               , cmpRes = True
               } 

--addInstr сохраняет инструкцию
addInstr :: Memory -> String -> String -> String -> Memory
addInstr mem i o1 o2 = if (permission mem) then mem { instructions = (instructions mem ++ [(i ++ " " ++ o1 ++ (if (o2 /= "") then (" " ++ o2) else o2), rip mem + 1)]) 
                                                    , rip = rip mem + 1 
                                                    }
                       else mem {rip = rip mem +1}

--removeInstr удаляет инструкцию
removeInstr :: Memory -> String -> String -> String -> Memory
removeInstr mem i o1 o2 = let delete' (s, ad) | ad == (-1) = error "Instruction is not found" 
                                              | otherwise = delete (s, ad)
                          in mem { instructions = delete' (i ++ " " ++ o1 ++ " " ++ o2, fromMaybe (-1) (lookup (i ++ " " ++ o1 ++ " " ++ o2) (instructions mem))) (instructions mem) }

--updateReg и updatePart изменяют значение регистра (или его части) в кортеже регистров
updateReg :: Memory -> Int -> Int64 -> Memory
updateReg mem i n | i `mod` 8 == 0 = mem {registers = upd1 (updatePart (registers mem) i n) (registers mem)} 
                  | i `mod` 8 == 1 = mem {registers = upd2 (updatePart (registers mem) i n) (registers mem)}  
                  | i `mod` 8 == 2 = mem {registers = upd3 (updatePart (registers mem) i n) (registers mem)}  
                  | i `mod` 8 == 3 = mem {registers = upd4 (updatePart (registers mem) i n) (registers mem)}  
                  | i `mod` 8 == 4 = mem {registers = upd5 (updatePart (registers mem) i n) (registers mem)}  
                  | i `mod` 8 == 5 = mem {registers = upd6 (updatePart (registers mem) i n) (registers mem)}  
                  | i `mod` 8 == 6 = mem {registers = upd7 (updatePart (registers mem) i n) (registers mem)}  
updateReg mem i n = mem {registers = upd8 (updatePart (registers mem) i n) (registers mem)} 

updatePart :: (Int64, Int64, Int64, Int64, Int64, Int64, Int64, Int64) -> Int -> Int64 -> Int64
updatePart reg i n | i `div` 8 == 1 = clearBits (selectReg reg i) 31 0 + n
                   | i `div` 8 == 2 = clearBits (selectReg reg i) 15 0 + n
                   | i `div` 8 == 3 = clearBits (selectReg reg i) 7 0 + n
                   | (i `div` 8 == 4) && (i `mod` 8 < 4) = clearBits (selectReg reg i) 15 8 + (n * 2 ^ (8 :: Integer))
updatePart _ _ n = n 

--selectReg возвращает значение 64-битного регистра по его номеру или номеру его части
selectReg :: (Int64, Int64, Int64, Int64, Int64, Int64, Int64, Int64) -> Int -> Int64
selectReg reg i | i `mod` 8 == 0 = sel1 reg 
                | i `mod` 8 == 1 = sel2 reg  
                | i `mod` 8 == 2 = sel3 reg  
                | i `mod` 8 == 3 = sel4 reg   
                | i `mod` 8 == 4 = sel5 reg   
                | i `mod` 8 == 5 = sel6 reg   
                | i `mod` 8 == 6 = sel7 reg   
selectReg reg _ = sel8 reg
                     
--clearBits обнуляет все биты числа между двумя указанными номерами битов (включительно)
clearBits :: Int64 -> Int -> Int -> Int64
clearBits n b e | (b > e) = clearBit (clearBits n (b-1) e) b
                | (b < e) = clearBits n e b 
clearBits n b _ = clearBit n b

--toInt64 и toStr переводят Storage в Int64 и String соответственно       
toInt64 :: Memory -> Storage -> Int64
toInt64 _ (Byte n) = fromIntegral n
toInt64 _ (Word n) = fromIntegral n  
toInt64 _ (Double n) = fromIntegral n  
toInt64 _ (Quad n) = n 
toInt64 mem (Reg n) = selectReg (registers mem) n    

toStr :: Storage -> String
toStr (Byte n) = show n
toStr (Word n) = show n  
toStr (Double n) = show n  
toStr (Quad n) = show n 
toStr (Reg n) = "%" ++ (!!) regList n

--addLabel добавляет метку
addLabel :: Memory -> String -> Memory
addLabel mem s = if (permission mem) then mem { labels = ((s, rip mem + 1) : labels mem) 
                                              , rip = rip mem + 1
                                              }
                 else mem {rip = rip mem + 1}

--removeLabel удаляет метку 
removeLabel :: Memory -> String -> Memory
removeLabel mem s = let delete' (str, i) | i == (-1) = error "Label is not found" 
                                         | otherwise = delete (str, i)
                    in mem {labels = delete' (s, fromMaybe (-1) (lookup s (labels mem))) (labels mem)}

--findLabel выдаёт индекс метки
findLabel :: Memory -> Int
findLabel mem = fromMaybe (error "Label is not found") (elemIndex (underLabel (instructions mem) (rip mem)) (instructions mem))
  where underLabel (x:xs) ri = if (snd x) > ri then x else underLabel xs ri
        underLabel [] _ = error "Label is not found"

mov :: Memory -> Int -> Storage -> Memory
mov mem reg stor = addInstr (updateReg mem reg (toInt64 mem stor)) "mov" ("%" ++ (!!) regList reg) (toStr stor)

add :: Memory -> Int -> Storage -> Memory
add mem reg stor = addInstr (updateReg mem reg (selectReg (registers mem) reg + toInt64 mem stor)) "add" ("%" ++ (!!) regList reg) (toStr stor)

sub :: Memory -> Int -> Storage -> Memory
sub mem reg stor = addInstr (updateReg mem reg (selectReg (registers mem) reg - toInt64 mem stor)) "sub" ("%" ++ (!!) regList reg) (toStr stor)

mul :: Memory -> Int -> Storage -> Memory
mul mem reg stor = addInstr (updateReg mem reg (selectReg (registers mem) reg * toInt64 mem stor)) "mul" ("%" ++ (!!) regList reg) (toStr stor)

cmp :: Memory -> Int -> Storage -> Memory
cmp mem reg stor = addInstr (if (toInt64 mem (Reg reg) == toInt64 mem stor) then mem {cmpRes = True} 
                             else mem {cmpRes = False}) "cmp" ("%" ++ (!!) regList reg) (toStr stor)

jmp :: Memory -> String -> Memory
jmp mem s = interpretUnderLabel (addInstr (mem { rip = fromMaybe (rip mem) (lookup s (labels mem)) }) "jmp" s "")

jne :: Memory -> String -> Memory
jne mem s = if (not (cmpRes mem)) then interpretUnderLabel (addInstr (mem { rip = fromMaybe (rip mem) (lookup s (labels mem))}) "jne" s "") {permission = False}
            else ((addInstr mem "jne" s "") {permission = True})

--Interpreter

--interpretProgram интерпретирует программу
interpretProgram :: Memory -> [String] -> Memory
interpretProgram mem = foldl interpretLine mem

--interpretUnderLabel интерпретирует все инструкции после метки
interpretUnderLabel :: Memory -> Memory
interpretUnderLabel mem = helper mem (map fst (drop (findLabel mem - 1) (instructions mem)))
  where helper memo = foldl interpretLine memo

--hexChar и hexToDec переводят 16-ричное число в 10-тичное
hexChar :: Char -> Int
hexChar ch = fromMaybe (error "Invalid character") (elemIndex ch (if (isUpper ch) then "0123456789ABCDEF" else "0123456789abcdef"))

hexToDec :: String -> Int
hexToDec hex = foldl' f 0 hex where
    f n c = 10 * n + hexChar c

--whatStorage переводит строку числа в экземпляр типа Storage
whatStorage :: String -> Storage
whatStorage str = if (str `elem` regList) then Reg (regNumber str)
                  else if (shiftR (hexToDec str) 7) == 0 then Byte (read $ show $ hexToDec str) 
                  else if (shiftR (hexToDec str) 15) == 0 then Word (read $ show $ hexToDec str)
                  else if (shiftR (hexToDec str) 31) == 0 then Double (read $ show $ hexToDec str)
                  else Quad (read $ show $ hexToDec str)

--regNumber возвращает номер регистра или его части
regNumber :: String -> Int
regNumber name = fromMaybe (error "Register with this name does not exist") (elemIndex name regList)

--interpretLine интерпретирует инструкцию/метку
interpretLine :: Memory -> String -> Memory
interpretLine mem line = do let instr = fromMaybe [] (parseMaybe parseInstr' line)
                            case instr of
                                 "mov" -> let list = fromMaybe (error "Incorrect instruction") (parseMaybe parseBinOp line) in (mov mem (regNumber (head $ tail list)) (whatStorage $ last list))
                                 "jmp" -> let list = fromMaybe (error "Incorrect instruction") (parseMaybe parseJmp line) in (jmp mem (last list)) 
                                 "jne" -> let list = fromMaybe (error "Incorrect instruction") (parseMaybe parseJne line) in (jne mem (last list)) 
                                 "add" -> let list = fromMaybe (error "Incorrect instruction") (parseMaybe parseBinOp line) in (add mem (regNumber (head $ tail list)) (whatStorage $ last list))
                                 "sub" -> let list = fromMaybe (error "Incorrect instruction") (parseMaybe parseBinOp line) in (sub mem (regNumber (head $ tail list)) (whatStorage $ last list))
                                 "mul" -> let list = fromMaybe (error "Incorrect instruction") (parseMaybe parseBinOp line) in (mul mem (regNumber (head $ tail list)) (whatStorage $ last list))
                                 "cmp" -> let list = fromMaybe (error "Incorrect instruction") (parseMaybe parseBinOp line) in (cmp mem (regNumber (head $ tail list)) (whatStorage $ last list))
                                 _ -> addLabel mem line

--factorial вычисляет факториал
factorial :: IO Memory
factorial = do
    let mem = start
    return (interpretProgram mem ["mov %rax 1"     --кладём в rax 1 (в rax будет лежать результат вычисления) 
                                 ,"mov %rbx 10"    --кладём в rbx число, факториал которого хотим получить
                                 ,"factCycle:"     --метка цикла
                                 ,"mul %rax %rbx"  --умножаем значение rax на значение rbx
                                 ,"sub %rbx 1"     --декрементируем значение rbx
                                 ,"cmp %rbx 1"     --сравниваем значение rbx с 1
                                 ,"jne factCycle:" --если в rbx 1 - останавливаемся, иначе - переходим на следующую итерацию
                                 ]) 

--vectorization изменяет исходную программу на программу с использованием векторных инструкций
vectorization :: Memory -> [String] -> [String]
vectorization mem = map (vectorLine mem)

vectorLine :: Memory -> String -> String
vectorLine mem line = do let instr = fromMaybe [] (parseMaybe parseInstr' line)
                         case instr of
                              "add" -> (vectorAdd mem (regNumber (head $ tail list)) (whatStorage $ last list)) where
                                         list = fromMaybe (error "Incorrect instruction") (parseMaybe parseBinOp line)
                              _ -> line

--vectorAdd меняет обычное сложение на сложение с использованием векторных инструкций
vectorAdd :: Memory -> Int -> Storage -> String
vectorAdd mem r stor = if (list2 == [2,3,4]) then "mov %" ++ (!!) regList r ++ " _mm_add_ss(%" ++ (!!) regList r ++ ", " ++ toStr stor ++ ")"
                          else if (list1 == [2,3,4]) then "mov %" ++ (!!) regList r ++ " _mm_add_ss(" ++ toStr stor ++ ", %" ++ (!!) regList r ++ ")"
                          else if (length list == 4) then "mov %" ++ (!!) regList r ++ " _mm_max_ps(%" ++ (!!) regList r ++ ", " ++ toStr stor ++ ")"  
                          else "mov %" ++ (!!) regList r ++ " _mm_add_ps(%" ++ (!!) regList r ++ ", " ++ toStr stor ++ ")"  where 
--проверяем, есть ли смысл осуществлять попарное сложение кусочков регистров, или некоторые кусочки нулевые 
    list = union list1 list2
    list1 = freeQr (selectReg (registers mem) (r `mod` 8)) 4 []
    list2 = freeQr (toInt64 mem stor) 4 [] 

--freeQr выдаёт список свободных четвертей 32-битного регистра или числа
freeQr :: Int64 -> Int -> [Int] -> [Int]
freeQr _ 0 l = l
freeQr rs i l = if (selectQr rs i == 0) then freeQr rs (i - 1) (i : l) else freeQr rs (i - 1) l

--selectQr возвращает значение четверти 32-битного регистра или числа
selectQr :: Int64 -> Int -> Int64
selectQr rs 4 = shiftR rs 24
selectQr rs 1 = clearBits rs 31 8
selectQr rs n = shiftR (clearBits rs 31 (n * 8)) ((n-1) * 8)

printList :: [String] -> IO ()
printList = mapM_ print

--main тестирует vectorization
main :: IO ()
main = do
   let mem = Memory { registers = (256, 65536, 190000, 128, 0, 0, 0, 0)
                    , rip = 0
                    , labels = []
                    , instructions = [] 
                    , permission = True
                    , cmpRes = True
                    }
   printList (vectorization mem ["add %eax %edx"
                                ,"add %edx %eax"
                                ,"add %eax %ebx"
                                ,"add %ebx %ecx"
                                ,"add %ecx 1"
                                ,"add %esi 4525"
                                ,"add %ecx 4525"
                                ])
