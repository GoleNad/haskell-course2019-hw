module DataTypes where

data CType 
  = CTInt | CTChar | CTBool | CTVoid | CTPtr CType | RetExpr 
  --RetExpr is used for typechecking when function is declared
  deriving (Eq)
instance Show CType where
  show CTInt = "Integer"
  show CTChar = "Char"
  show CTBool = "Bool"
  show CTVoid = "Void"
  show (CTPtr x) = "*" ++ show x
  show RetExpr = "_" --it's not for show
  
data CVal
  = CInt Integer
  | CChar Char 
  | CBool Bool
  | CPtr 
  | CVoid 
  | CNull 
  deriving (Eq)
instance Show CVal where
  show (CInt num) = show num
  show (CChar ch) = show ch
  show (CBool bl) = show bl
  show (CVoid)   = "(void)"
  show (CPtr)     = "(pointer)"
  show (CNull)    = "Null"
  --last 3 are not for show, actually

data Expr
  = Literal CVal
  | Var String
  | VarIndexed (String, Expr) 
  | Neg Expr
  | Call String [Expr] 
  | Memory Expr
  | ExprBinary ExprOp Expr Expr
  | ExprUnary ExprOpUnary Expr
  deriving (Show, Eq)
    
data ExprOpUnary
  = Inc
  | Dec
  | Indirection
  | Address
  deriving (Show, Eq)
    
data ExprOp
  = Add
  | Subtract
  | Multiply
  | Divide
  | Mod
  | BitShl 
  | BitShr
  | BitAnd 
  | BitOr
  | BitXor
  deriving (Show, Eq)
  
data Stmt 
  = Assign Expr Expr
  | Block [Stmt]
  | IfElse BoolExpr Stmt Stmt 
  | If BoolExpr Stmt
  | While BoolExpr Stmt
  | Decl Declaration 
  | Ret RetStmt
  | FCall Expr
  | Print [Expr]
  deriving (Show)
  
showStmt :: Stmt -> String
--showStmt (Assign a b)                     = "Assigned: " ++ show a ++ "\n"
--showStmt (IfElse cond a b)                = "If-then-else expression\n"
--showStmt (If cond a)                      = "if condition\n"
--showStmt (While cond a)                   = "While loop\n"
--showStmt (Decl (Var' typ name body))      = "Var declared: " ++ name ++ "\n"
--showStmt (Decl (Func typ name args body)) = "Function declared: " ++ name ++ "\n"
--showStmt (Decl (Mem name))                = "Memory allocated for [" ++ name ++ "]\n"
--showStmt (Ret retStmt)                    = "Return: " ++ (show retStmt) ++ "\n"
--showStmt (FCall (Call name args))         = "Function called: " ++ name ++ "\n"
showStmt (Print (p@(Literal val):[]))     = show val ++ "\n"
showStmt (Print (p@(Literal val):oth))    = show val ++ ", " ++ (showStmt $ Print oth)
showStmt (Print _)                        = "Error: only values can be printed"
showStmt (Ret (Returned val))             = "Returned: " ++ show val
showStmt (Block [])                       = ""
showStmt (Block list@(a:oth))             = (showStmt a) ++ (showStmt $ Block oth)
showStmt _                                = ""
-- it only shows what is printed or returned from function call
  
data RetStmt 
  = RVoid
  | RBExpr BoolExpr
  | RExpr Expr
  | Returned CVal -- for simpler show
  deriving (Show)
  
data Declaration 
  = Var' CType String Expr -- type name body
  | Func CType String [(CType, String)] Stmt -- type name [(type, arg)} body 
  | Mem String --it's for show, for use there is an expression
  deriving (Show)
  
data BoolExpr
  = BoolConst Bool
  | Not BoolExpr
  | BBinary BoolBinOp BoolExpr BoolExpr
  | RBinary RelationBinOp Expr Expr
  | BoolExpr Expr
  deriving (Show)
  
data BoolBinOp
  = And
  | Or
  deriving (Show)
  
data RelationBinOp
  = Greater
  | Less
  | Equal
  | NotEq
  | GtEq
  | LtEq
  deriving (Show)
  
data CError 
  = NotInScope String
  | WrongNumArg String
  | TypeMismatch String 
  | Redefine String 
  | WrongReturn String
  | WrongExpr String
  | OutOfRange String
  | Default String 
   deriving (Show)

type Throwable = Either CError