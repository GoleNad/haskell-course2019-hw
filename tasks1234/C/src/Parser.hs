﻿{-# OPTIONS_GHC -fwarn-incomplete-patterns -fwarn-incomplete-uni-patterns #-}

module Parser where

import           Control.Monad                  ( void )
import           Control.Monad.Combinators.Expr
import           Data.Void
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer    as L

import DataTypes
-------------------------- Lexer part

type Parser = Parsec Void String

sc :: Parser ()
sc = L.space (space1) lineCmnt blockCmnt
 where
  lineCmnt  = L.skipLineComment "//"
  blockCmnt = L.skipBlockComment "/*" "*/"

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: String -> Parser String
symbol = L.symbol sc

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

curlyOpen :: Parser String
curlyOpen = symbol "{" <* (optional spaceChar)

curlyClose :: Parser String
curlyClose = (optional spaceChar) *> symbol "}"

curly :: Parser a -> Parser a
curly p = curlyOpen *> p

--  | curly doesn't end by curlyClose because symbol "}" might show end of EXPR (it will be used in StmtParser)

semi :: Parser String
semi = symbol ";" <* (optional newline)

comma :: Parser String
comma = symbol ","

rword :: String -> Parser ()
rword w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)

keywords :: [String] 
keywords =
  [ "if"   , "then"   , "else"
  , "while", "do"     , "for"
  , "true" , "false"  , "return"
  , "break", "continue"
  , "int"  , "void"   , "bool"
  , "char" , "malloc"         ]

identifier :: Parser String
identifier = (lexeme . try) (p >>= check)
 where
  p       = (:) <$> (letterChar <|> char '_') <*> many (alphaNumChar <|> char '_')
  check x = if x `elem` keywords
     then fail $ "keyword " ++ show x ++ " cannot be an identifier"
     else return x
 
indexedIdent :: Parser (String, Expr)
indexedIdent = do
  name <- identifier
  i <- between (char '[') (char ']') numExpr
  void space
  return (name, i)

anyIdent :: Parser (String, Expr)
anyIdent = 
  try indexedIdent
  <|> do 
         void (char '*')
         name <- identifier
         return (name, Literal CPtr)
  <|> do
         name <- identifier
         return (name, Literal CNull)

whileParser :: Parser Stmt
whileParser = between sc eof stmtParser

-------------------------- Parser part

stmtParser :: Parser Stmt
stmtParser =  Block <$> sepEndBy1 stmt (semi <|> curlyClose)

--   | sepEndBy1 stmt (semi <|> curlyClose) : because of C syntax
--   | it might end like EXPR; or { EXPR; }

stmt :: Parser Stmt
stmt = parens stmtParser
  <|> incStmt <|> decStmt <|> try whileStmt  
  <|> try ifElseStmt <|> try ifStmt
  <|> try funcDeclStmt <|> try varDeclStmt
  <|> try (anyIdent >>= (\var -> assignStmt var))
  <|> try retStmt <|> try printStmt
  <|> (FCall <$> funCall)



ifElseStmt :: Parser Stmt
ifElseStmt = do
  rword "if"
  cond <- parens boolExpr
  stmt1 <- (optional spaceChar) *> curly stmtParser
  void curlyClose
  (optional spaceChar) *> rword "else"
  stmt2 <- (optional spaceChar) *> curly stmtParser
  return (IfElse cond stmt1 stmt2)

ifStmt :: Parser Stmt
ifStmt = do 
  rword "if"
  cond <- parens boolExpr
  stmt1 <- (optional spaceChar) *> curly stmtParser
  return (If cond stmt1)

whileStmt :: Parser Stmt
whileStmt = do
  rword "while"
  cond <-  parens boolExpr
  stmt1 <- (optional spaceChar) *> curly stmtParser
  return (While cond stmt1)

assignStmt :: (String, Expr) -> Parser Stmt
assignStmt var@(name, ind) = 
      do     
      void (symbol "=")
      expr <- try memParser <|> numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) expr)
        else return (Assign (VarIndexed var) expr)         
  <|> do 
      void (symbol "*=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary Multiply (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary Multiply (VarIndexed var) (expr)))
  <|> do 
      void (symbol "/=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary Divide (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary Divide (VarIndexed var) (expr)))
  <|> do 
      void (symbol "+=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary Add (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary Add (VarIndexed var) (expr)))      
  <|> do 
      void (symbol "-=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary Subtract (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary Subtract (VarIndexed var) (expr)))
  <|> do 
      void (symbol "%=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary Mod (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary Mod (VarIndexed var) (expr)))
  <|> do 
      void (symbol "<<=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary BitShl (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary BitShl (VarIndexed var) (expr)))
  <|> do 
      void (symbol ">>=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary BitShr (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary BitShr (VarIndexed var) (expr)))
  <|> do 
      void (symbol "&=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary BitAnd (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary BitAnd (VarIndexed var) (expr)))
  <|> do 
      void (symbol "|=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary BitOr (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary BitOr (VarIndexed var) (expr)))
  <|> do 
      void (symbol "^=")
      expr <- numExpr
      if (ind == Literal CNull)
        then return (Assign (Var name) (ExprBinary BitXor (Var name) (expr)))
        else return (Assign (VarIndexed var) (ExprBinary BitXor (VarIndexed var) (expr)))

retStmt :: Parser Stmt
retStmt = 
  try (do
        rword "return"
        retStmt <- numExpr
        return (Ret $ RExpr retStmt))
  <|> try (do
        rword "return"
        bStmt <- boolExpr
        return (Ret $ RBExpr bStmt))
  <|> do
        rword "return"
        return (Ret $ (RVoid))
  
funcDeclStmt :: Parser Stmt
funcDeclStmt = do
  ftype <- parseType
  void space
  name <- identifier
  args <- parens $ sepBy parseArg comma 
  body <- curly stmtParser
  return (Decl $ Func ftype name args body)
  
varDeclStmt :: Parser Stmt
varDeclStmt = do
  vtype <- parseType
  void space
  name <- identifier
  symbol "="
  body <- try memParser <|> numExpr
  return (Decl $ Var' vtype name body)  

parseType :: Parser CType
parseType = 
  try (do     rword "int*"
              return $ CTPtr CTInt)
  <|> do      rword "int"
              return $ CTInt
  <|> try (do rword "char*"
              return $ CTPtr CTChar)
  <|> do      rword "char"
              return $ CTChar
  <|> try (do rword "bool*"
              return $ CTPtr CTBool)
  <|> do      rword "bool"
              return $ CTBool
  <|> try (do rword "void*"
              return $ CTPtr CTVoid)
  <|> do      rword "void"
              return $ CTVoid
    
parseArg :: Parser (CType, String)
parseArg = do
  t1 <- parseType
  v1 <- identifier
  return (t1, v1)

memParser :: Parser Expr
memParser = do
  rword "malloc"
  symbol "*"
  number <- numExpr
  return $ Memory number
  
parseInt :: Parser Expr
parseInt = Literal <$> CInt <$> lexeme L.decimal

parseChar :: Parser Expr
parseChar = do
  void $ symbol "'"
  ch <- lexeme L.charLiteral
  void $ char '\''
  return $ Literal $ CChar ch

parseBool :: Parser Expr
parseBool = do 
        symbol "true"
        return $ Literal $ CBool True
  <|> do 
        symbol "false"
        return $ Literal $ CBool False 
      
funCall :: Parser Expr
funCall = do
  fname <- identifier
  void $ char '('
  args <- sepBy numExpr comma 
  void $ symbol ")"
  return (Call fname args)
  
incStmt :: Parser Stmt
incStmt = do
  void (symbol "++")
  var@(name, ind) <- anyIdent
  if (ind == Literal CNull)
    then return $ Assign (Var name) (ExprUnary Inc (Var name))
    else return $ Assign (VarIndexed var) (ExprUnary Inc (VarIndexed var))  
    
decStmt :: Parser Stmt
decStmt = do
  void (symbol "--")
  var@(name, ind) <- anyIdent
  if (ind == Literal CNull)
    then return $ Assign (Var name) (ExprUnary Dec (Var name))
    else return $ Assign (VarIndexed var) (ExprUnary Dec (VarIndexed var))  
  
printStmt :: Parser Stmt
printStmt = do
  rword "print"
  expr <- parens $ sepBy numExpr comma 
  return (Print expr)
-------------------------- Expression part  
  
numExpr :: Parser Expr
numExpr = try (makeExprParser numTerm numOperators) 
  <|> numTerm

boolExpr :: Parser BoolExpr
boolExpr = try (makeExprParser boolTerm boolOperators)
  <|> BoolExpr <$> numExpr

numOperators :: [[Operator Parser Expr]]
numOperators =
  [ [ Prefix (Neg <$ symbol "-")
    , Postfix (ExprUnary Inc <$ symbol "++")
    , Postfix (ExprUnary Dec <$ symbol "--")
    , Prefix (ExprUnary Indirection <$ symbol "*")
    , Prefix (ExprUnary Address <$ symbol "&")
    ]
  , [ InfixL (ExprBinary Multiply <$ symbol "*")
    , InfixL (ExprBinary Divide <$ symbol "/")
    , InfixL (ExprBinary Mod <$ symbol "%")
    ]
  , [ InfixL (ExprBinary Add <$ symbol "+")
    , InfixL (ExprBinary Subtract <$ symbol "-")
    ]
  , [ InfixL (ExprBinary BitShl <$ symbol "<<")
    , InfixL (ExprBinary BitShr <$ symbol ">>")
    ]
  , [ InfixL (ExprBinary BitAnd <$ symbol "&")
    ]
  , [ InfixL (ExprBinary BitXor <$ symbol "^")
    ]
  , [ InfixL (ExprBinary BitOr <$ symbol "|")
    ]
  ]

boolOperators :: [[Operator Parser BoolExpr]]
boolOperators =
  [ [Prefix (Not <$ symbol "!")]
  , [InfixL (BBinary And <$ symbol "&&"), InfixL (BBinary Or <$ symbol "||")]
  ]

numTerm :: Parser Expr
numTerm = try funCall
  <|> parens numExpr 
  <|> parseChar  
  <|> try parseInt <|> try parseBool
  <|> try (VarIndexed <$> indexedIdent)
  <|> try (Var <$> identifier) 

boolTerm :: Parser BoolExpr
boolTerm =
  parens boolExpr
    <|> (BoolConst True <$ rword "true")
    <|> (BoolConst False <$ rword "false")
    <|> relExpr

relExpr :: Parser BoolExpr
relExpr = do
  a1 <- numExpr
  op <- relation
  a2 <- numExpr
  return (RBinary op a1 a2)

relation :: Parser RelationBinOp
relation = try  (LtEq <$ symbol "<=") <|> (Less <$ symbol "<") 
  <|> (Equal <$ symbol "==") <|> (NotEq <$ symbol "!=")
  <|> try (GtEq <$ symbol ">=") <|> (Greater <$ symbol ">")