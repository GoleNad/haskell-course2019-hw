module Main where

import DParser
import AST
import Interpreter
import Data.Maybe
import Data.List.Utils (genericJoin)
import Text.Megaparsec
import Text.Pretty.Simple

runIt = runDatalog . fromJust . parseMaybe datalogParser


peanoIn = Constant (Name "peano math")
mathIn = Constant (Name "basic math")
includes = Predicate { phead = Name "#- running with ", terms = [peanoIn, mathIn] }

printOut (Right output) = putStrLn $ genericDJoin "\n\n" "\n" ([includes]:output)
printOut (Left error) = putStrLn $ genericJoin "\n" error

interprete = printOut . runIt

main :: IO ()
main = do
    interprete (""
             ++ "fib(0, 0)."
             ++ "fib(1, 1)."

             ++ "fib(N, F) :- N != 1,"
             ++              "N != 0,"
             ++              "N1 :- -(N, 1),"
             ++              "N2 :- -(N, 2),"
             ++              "fib(N1, F1),"
             ++              "fib(N2, F2),"
             ++              "F :- +(F1, F2)."

             ++ "fib(10, F)?")
                            
    putStrLn ""
                            
    interprete (""
             ++ "edge(a, b)."
             ++ "edge(b, c)."
             ++ "edge(c, d)."
             ++ "edge(d, a)."
             ++ "path(A, B) :- edge(A, B)."
             ++ "path(A, B) :- edge(A, X), path(X, B)."
             ++ "path(A, B)?"
             ++ "path(b, X)?"
             ++ "edge(d, a)~"
             ++ "path(b, X)?")
             
    putStrLn ""
             
    interprete (""
             ++ "true."
             ++ "true?")
             
    putStrLn ""
    
    interprete (""
             ++ "false() :- Succ Zero = Succ Succ Zero."
             ++ "true() :- Succ Succ Zero = Succ Succ Zero."
             ++ "true?"
             ++ "false?"
             ++ "true?")
