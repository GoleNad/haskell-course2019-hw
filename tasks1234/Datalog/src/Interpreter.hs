module Interpreter where

import Prelude hiding (zipWith)
import AST
import Control.Monad
import Control.Monad.HT (liftJoin2, lift, zipWith)
import Data.Maybe (fromJust, catMaybes)
import Data.List (nub)

--             callstack (called : defined); list of clauses; list of bindings
type Global = ([(Literal, Literal)],         [Clause],        [(Name, Term)])

--           list of clauses; output, a list per each query
type Prog = ([Clause],        [[Literal]])

type IError a = Either [String] a


evalExprTree :: [(Name, Term)] -> ExprTree -> Maybe Int
evalExprTree _     (Value x)         = Just x

evalExprTree assoc (Var s)           = retIVal (lookup s assoc)
                                       where
                                           retIVal (Just (IVal x)) = Just x
                                           retIVal _ = Nothing
                                           

evalExprTree assoc (Plus left right) = liftM2 (+) (evalExprTree assoc left) (evalExprTree assoc right)

evalExprTree assoc (Minus left right)= liftM2 (-) (evalExprTree assoc left) (evalExprTree assoc right)


evalTerm :: [(Name, Term)] -> Term -> IError Term
evalTerm assoc (Expr tr) = retVal $ evalExprTree assoc tr
                           where retVal Nothing = Left ["Error while evaluating: ", (show tr), "   assoc list: ", (show assoc)]
                                 retVal (Just x) = Right $ IVal x
                                     
evalTerm assoc (Variable name) = retVal (lookup name assoc)
                                 where
                                     retVal (Just t) = Right t
                                     retVal _ = Left ["Evaluating non-associated variable"]

evalTerm _ x = Right x


evalTermVarFree :: [(Name, Term)] -> Term -> IError Term
evalTermVarFree assoc var@(Variable name) = retVal (lookup name assoc)
                                            where
                                                retVal (Just t) = Right t
                                                retVal _ = Right var
                                                
evalTermVarFree assoc x = evalTerm assoc x


checkTermList :: [Term] -> [Term] -> Maybe [(Name, Term)]
checkTermList [] [] = Just []

checkTermList ((Variable _):xs) (_:ps) = checkTermList xs ps

checkTermList ((Constant x):xs) ((Constant p):ps) = if (x == p) then checkTermList xs ps else Nothing

checkTermList ((IVal x):xs) ((IVal p):ps) = if x == p then checkTermList xs ps else Nothing

checkTermList (x@(Constant _):xs) ((Variable p):ps) = liftM2 (:) (Just (p, x)) (checkTermList xs ps)

checkTermList (val@(IVal _):xs) ((Variable p):ps) = liftM2 (:) (Just (p, val)) (checkTermList xs ps)

checkTermList _ _ = Nothing


checkPredicate :: [(Literal, Literal)] -> [Clause] -> Literal -> Clause -> IError (Maybe (Global, [Literal]))
checkPredicate callstack clauses lit@(Predicate head list) (Clause pr@(Predicate shead slist) body)
    = if (head == shead) && ((lookup lit callstack) == Nothing) && (not (checked == Nothing))
      then Right (Just (((lit, pr):callstack, clauses, fromJust checked), body))
      else Right Nothing
      where checked = checkTermList list slist
                                                                          
checkPredicate _ _ wrongLit _ = Left ["Expected predicate, found: ", show wrongLit]


getGoodClauses :: [(Literal, Literal)] -> [Clause] -> Literal -> IError [(Global, [Literal])]
getGoodClauses callstack clauses literal = lift catMaybes ((mapM (checkPredicate callstack clauses literal)) clauses)


isEqEvTerms :: Term -> Term -> Bool
isEqEvTerms (Constant a) (Constant b) = a == b

isEqEvTerms (IVal a) (IVal b) = a == b

isEqEvTerms _ _ = False


proceedLiteral :: Literal -> Global -> IError [Global]
proceedLiteral (Equ left right) g@(_, _, assoc) 
    = liftJoin2 doEq (eT left) (eT right)
      where eT = evalTerm assoc
            doEq x y | isEqEvTerms x y = Right [g]
                     | otherwise       = Right []
                                                 
proceedLiteral (Neq left right) g@(_, _, assoc)
    = liftJoin2 doEq (eT left) (eT right)
      where eT = evalTerm assoc
            doEq x y | isEqEvTerms x y = Right []
                     | otherwise       = Right [g]
             
                                                  
proceedLiteral (Assoc (Variable name) term) (cs, cl, assoc)
    = (eT term) >>= makeAssoc
      where eT = evalTerm assoc
            
            makeAssoc x = Right [(cs, cl, (name, x):assoc)]
                                                              
proceedLiteral (Assoc wrongTerm with) _ = Left ["Can't associate: ", (show wrongTerm), "   and", (show with)]

proceedLiteral (Predicate phead terms) g@(callstack, clauses, assoc)
    = (mapM (evalTermVarFree assoc) terms) >>= makelit >>= getit
      where makelit x = Right (Predicate phead x)
            getit x = (lift concat ((getGoodClauses callstack clauses x) >>= (mapM (uncurry evalPred)))) >>= (mapM (complete g))


proceedLitOnList :: Literal -> [Global] -> IError [Global]
proceedLitOnList lit gs = lift concat (mapM (proceedLiteral lit) gs)


evalPred :: Global -> [Literal] -> IError [Global]
evalPred global body = foldl (liftJoin2 (flip proceedLitOnList)) (Right [global]) (map Right body)


complete :: Global -> Global -> IError Global
complete (cs, cl, assocOuter) ((Predicate _ called, Predicate _ defined):_, _, assocInter) 
    = assocRes >>= join
      where assocRes = lift (filter (\(Name name, _) -> not (name == ""))) (zipWith (resolve assocInter) called defined)
            join x = Right (cs, cl, x ++ assocOuter)


resolve :: [(Name, Term)] -> Term -> Term -> IError (Name, Term)
resolve assoc (Variable call) (Variable def) 
    = (retVal $ lookup def assoc) >>= makeAssoc
      where retVal (Just t) = Right t
            retVal _ = Left ["Unresolved variable in clause"]
            
            makeAssoc x = Right (call, x)
                                              
resolve _ (Variable call) def = Right (call, def)
                                              
resolve _ _ _ = Right (Name "", IVal (-1))


runQuery :: [Clause] -> Literal -> IError [Literal]
runQuery clauses pr@(Predicate _ _)
    = lift nub ((proceedLiteral pr ([], clauses, [])) >>= mapM (makeResolvedPred pr))

runQuery _ smth = Left ["Expected predicate, found ", show smth]


makeResolvedPred :: Literal -> Global -> IError Literal
makeResolvedPred (Predicate phead terms) (_, _, assoc) 
    = (mapM (evalTerm assoc) terms) >>= makePred
      where makePred x = Right (Predicate phead x)

makeResolvedPred _ _ = Left ["Expected predicate, but how did we end up here if the error comes in @runQuery as well..."]



qualifyClause :: Clause -> IError Clause
qualifyClause (Clause (Predicate phead terms) body)
    = (mapM (evalTermVarFree []) terms) >>= makeClause
      where makeClause x = Right (Clause (Predicate phead x) body)

qualifyClause wrongClause = Left ["Wrong clause definition", show wrongClause]


runStatement :: Prog -> Statement -> IError Prog
runStatement (clauses, output) (Assertion newClause)
    = (qualifyClause newClause) >>= add
      where add x = Right (x:clauses, output)

runStatement (clauses, output) (Retraction toRet)
    = (qualifyClause toRet) >>= del
      where del x = Right ((filter (\y -> not (y == x)) clauses), output)

runStatement (clauses, output) (Query ask)
    = (runQuery clauses ask) >>= out
      where out x = Right (clauses, x:output)
      

runDatalog :: Program -> IError [[Literal]]
runDatalog (Body prog) = (lift reverse) $ (lift snd) $ (foldl (liftJoin2 runStatement) (Right ([], [])) (map Right prog))



