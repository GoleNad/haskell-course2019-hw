module AST where

import Data.Maybe
import Data.List.Utils (genericJoin)

data Program = Body [Statement]
    deriving (Show, Eq)


data Statement = 
      Assertion Clause
    | Retraction Clause
    | Query Literal
    deriving (Show, Eq)


data Clause = 
      Clause { lit :: Literal, body :: [Literal] }
    deriving (Show, Eq)


data Literal = 
      Equ { left :: Term, right :: Term }
    | Neq { left :: Term, right :: Term }
    | Assoc { name :: Term, value :: Term }
    | Predicate { phead :: Name, terms :: [Term] }

genericDJoin :: Show a => String -> String -> [[a]] -> String
genericDJoin _    _    []     = ""
genericDJoin _    sepS (x:[]) = genericJoin sepS x
genericDJoin sepF sepS (x:xs) = genericJoin sepS x ++ sepF ++ genericDJoin sepF sepS xs
    
instance Show Literal where
    show (Equ left right) = concat [show left, " == ", show right]
    
    show (Neq left right) = concat [show left, " != ", show right]
    
    show (Assoc name val) = concat [show name, " :- ", show val]
    
    show (Predicate (Name str) terms) = concat [str, "(", genericJoin ", " terms, ")"]
    
instance Eq Literal where
    (Predicate h1 l1) == (Predicate h2 l2) = (h1 == h2) && (sl1 == sl2) where
        simplify assoc _  [] = []
        simplify assoc id ((x@(Variable _)):xs) = join $ lookup x assoc
                                                  where join Nothing = (Left id):(simplify ((x, Left id):assoc) (id + 1) xs)
                                                        join (Just bind) = bind:(simplify assoc id xs)
        simplify assoc id (x:xs) = (Right x):(simplify assoc id xs)
        
        sl1 = simplify [] 1 l1
        sl2 = simplify [] 1 l2
        
    (Equ l1 r1) == (Equ l2 r2) = (l1 == l2) && (r1 == r2)
    
    (Neq l1 r1) == (Neq l2 r2) = (l1 == l2) && (r1 == r2)
    
    (Assoc n1 v1) == (Assoc n2 v2) = (n1 == n2) && (v1 == v2)
    
    _ == _ = False
    
    
data Term = 
      Variable Name
    | Constant Name
    | Expr ExprTree
    | IVal Int
    deriving (Eq)
    
instance Show Term where
    show (Variable (Name str)) = str
    
    show (Constant (Name str)) = str
    
    show (IVal x) = show x
    
    show (Expr tree) = show tree
    
    
data ExprTree = 
      Value Int
    | Var Name
    | Plus ExprTree ExprTree
    | Minus ExprTree ExprTree
    deriving (Show, Eq)
    
    
data Name = Name String
    deriving (Show, Eq)
