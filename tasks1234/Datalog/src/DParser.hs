{-# LANGUAGE NamedFieldPuns #-}

module DParser where

import AST
import Control.Monad (void)
import Control.Monad.Combinators.Expr
import Data.Void
import Data.Char (isLetter, isUpper, isLower)
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

type DgParser = Parsec Void String

lineComment = L.skipLineComment "% "

base = L.space (skipSome spaceChar) lineComment empty

symbol    = L.symbol base
word      = L.lexeme base

parens    = (between) (symbol "(") (symbol ")")
brackets  = (between) (symbol "[") (symbol "]")
quotes    = (between) (symbol "\"") (symbol "\"")
popen     = symbol "("
pclose    = symbol ")"
comma     = symbol ","
dot       = symbol "."
tilde     = symbol "~"
plus      = symbol "+"
minus     = symbol "-"
equal     = symbol "="
query     = symbol "?"
peano     = symbol "'"
peanoSucc = word $ string "Succ"
peanoZero = word $ string "Zero"
headSep   = word $ string ":-"
notEqual  = word $ string "!="
varSymbol = try alphaNumChar <|> char '_'

datalogParser = between base eof (Body <$> many statement)

statement :: DgParser Statement
statement = try parseAssert
        <|> try parseRetract
        <|> try parseQuery

parseAssert :: DgParser Statement
parseAssert = do
    toAssert <- parseClause
    void (dot)
    return (Assertion toAssert)
    
parseRetract :: DgParser Statement
parseRetract = do
    toRetract <- parseClause
    void (tilde)
    return (Retraction toRetract)
    
parseQuery :: DgParser Statement
parseQuery = do
    toAsk <- parsePred
    void (query)
    return (Query toAsk)
    
parseClause :: DgParser Clause
parseClause = do
    lit <- parsePred
    body <- try (parseLits) <|> (return [])
    return (Clause { lit, body })

parseLits :: DgParser [Literal]
parseLits = do
    void (headSep)
    sepBy1 parseLiteral comma
    
parseLiteral :: DgParser Literal
parseLiteral = try parseEq
           <|> try parseNeq
           <|> try parseAssoc
           <|> try parsePred
           
parseEq :: DgParser Literal
parseEq = do
    left <- parseTerm
    void (equal)
    right <- parseTerm
    return (Equ { left, right })
    
parseNeq :: DgParser Literal
parseNeq = do
    left <- parseTerm
    void (notEqual)
    right <- parseTerm
    return (Neq { left, right })
    
parseAssoc :: DgParser Literal
parseAssoc = do
    name <- parseVariable
    void (headSep)
    value <- parseTerm
    return (Assoc { name, value })
    
parsePred :: DgParser Literal
parsePred = do
    phead <- (parseIdent)
    terms <- (try (parens (sepBy parseTerm comma))) <|> (return [])
    return (Predicate { phead, terms })
    
parseTerm :: DgParser Term
parseTerm = try parsePeano
        <|> try parseConstant
        <|> try parseVariable
        <|> try parseExpr
        
parsePeano :: DgParser Term
parsePeano = do
    raw <- parsePeanoRaw
    return (Expr raw)
    
parsePeanoRaw :: DgParser ExprTree
parsePeanoRaw = try parsePeanoSucc
            <|> try parsePeanoZero

parsePeanoSucc :: DgParser ExprTree
parsePeanoSucc = do
    void (peanoSucc)
    toSucc <- parsePeanoRaw
    return (Plus (Value 1) toSucc)
    
parsePeanoZero :: DgParser ExprTree
parsePeanoZero = do
    void (peanoZero)
    return (Value 0)

parseVariable :: DgParser Term
parseVariable = do
    var <- parseVariableS
    return (Variable (Name var))

parseVariableS :: DgParser String
parseVariableS = (word . try) (((:) <$> (upperChar)) <*> many varSymbol)

parseConstant :: DgParser Term
parseConstant = do
    id <- parseIdent
    return (Constant id)

parseIdent :: DgParser Name
parseIdent = do
    id <- parseIdentS
    return (Name id)

parseIdentS :: DgParser String
parseIdentS = (word . try) (((:) <$> (try lowerChar <|> char '_') <*> many varSymbol))
            
parseExpr :: DgParser Term
parseExpr = do
    tree <- parseExprTree
    return (Expr tree)
           
parseExprTree :: DgParser ExprTree
parseExprTree = try parseExprValue
            <|> try parseExprVar
            <|> try parseExprPlus
            <|> try parseExprMinus

parseExprVar :: DgParser ExprTree
parseExprVar = do
    (Variable var) <- parseVariable
    return (Var var)
        
parseExprValue :: DgParser ExprTree
parseExprValue = do
    val <- word L.decimal
    return (Value val)

parseExprPlus :: DgParser ExprTree
parseExprPlus = do
    void (plus)
    (lf, rg) <- parseParExpr
    return (Plus lf rg)
    
parseExprMinus :: DgParser ExprTree
parseExprMinus = do
    void (minus)
    (lf, rg) <- parseParExpr
    return (Minus lf rg)

parseParExpr :: DgParser (ExprTree, ExprTree)
parseParExpr = do
    (expr1:expr2:[]) <- parens (sepBy parseExprTree comma)
    return (expr1, expr2) 
