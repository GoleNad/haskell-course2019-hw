usage: (file name) [test | go | optimize]

* Some implementation of AST
* Some implementation of parser
* Some implementation of interpreter
* Some optimizations:
    * Simple constant folding
    * Elimination of unreachable sentences
    * Elimination of conditions that are always true
