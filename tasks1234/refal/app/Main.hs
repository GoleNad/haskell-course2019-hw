module Main where

import RefalAst
import RefalEntry
import RefalInterpreter
import RefalOptimizer
import RefalArithmetics

import Data.List (partition)
import System.Environment

import Text.Pretty.Simple (pPrint)

main :: IO ()
main = do
    args <- getArgs
    case args of
        [] -> putStrLn "usage: (file name) [test | go | optimize]"
        fname : rargs -> do
            res <- parseRefal <$> readFile fname :: IO (Either String (Scope Function))
            case res of
                Left err    -> putStrLn err
                Right scope -> case rargs of
                    []     -> putStrLn $ showRefal scope
                    op : _ -> case op of
                        "test"     -> pPrint $ test scope
                        "go"       -> pPrint $ go scope
                        "optimize" -> putStrLn $ showRefal $ optimizeRefal (arithmeticFuns <> scope) scope
                        _          -> putStrLn $ showRefal scope

run :: String -> (Scope Function -> a) -> Scope Function -> [a]
run name op (Scope funs) = map helper choosen where
    (choosen, other) = partition (\(fname, _) -> fname == Name name) funs

    helper fun = op (addToScope fun (arithmeticFuns <> Scope other))

test :: Scope Function -> [String]
test = run "Test" helper where
    helper scope = case interpret scope startResExpr of
        Left err  -> "Result error: " ++ show err
        Right res -> case interpret scope startExpr of
            Left err1  -> "Error: " ++ show err1
            Right res1 -> if res == res1 then
                    "Ok: " ++ showRefal res1
                else
                    "?: " ++ showRefal res1
    
    startExpr = simpleExpr $ FunctionApplication (Name "Test") mempty
    startResExpr = simpleExpr $ FunctionApplication (Name "Test") $ simpleExpr $ Evaluable $ Unbound $ Identifier $ Name "result"

go :: Scope Function -> [Either Error String]
go = run "Go" helper where
    helper scope = fmap showRefal $ interpret scope startExpr

    startExpr = simpleExpr $ FunctionApplication (Name "Go") mempty
