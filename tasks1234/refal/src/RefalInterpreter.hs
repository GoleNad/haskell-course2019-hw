module RefalInterpreter (interpret) where

import RefalAst

import Control.Monad.Except
import Control.Monad.Reader
import Data.Foldable

interpret :: Scope Function -> Expression Evaluable -> Either Error Value
interpret funs expr = runReader (runExceptT $ eval expr) (funs, Scope [])

class Interpretable a where
    eval :: a -> Interpreter Value

instance Interpretable a => Interpretable (Expression a) where
    eval = fmap joinExpr . traverse eval

instance Interpretable Evaluable where
    eval (Evaluable unbound)             = eval unbound
    eval (FunctionApplication name expr) = do
        fun <- getFun name
        arg <- eval expr
        case fun of
            BuiltIn f           -> f arg
            Defined (Block blk) -> chngVals (evalBlock blk arg) (const $ Scope [])

instance Interpretable Unbound where
    eval (Unbound symbol)         = return $ simpleExpr symbol
    eval (UnboundVariable name _) = getVal name

instance Interpretable Substitution where
    eval (ExpressionSubstitution expr)        = eval expr
    eval (BlockSubstitution (Block blk) expr) = eval expr >>= evalBlock blk

evalBlock :: [Sentence] -> Expression Symbol -> Interpreter Value
evalBlock [] _ = throwError NotExhaustiveMatching
evalBlock (Sentence pattern conds subst : ss) arg = try `catchError` catcher where
    try = match pattern arg >>= chngVals (check conds >> eval subst) . const

    catcher NotMatches = evalBlock ss arg
    catcher e          = throwError e

check :: [Condition] -> Interpreter ()
check conds = traverse_ (\(Condition pattern expr) -> eval expr >>= match pattern) conds

match :: Expression Unbound -> Expression Symbol -> Interpreter (Scope Value)
match (Expression uts) (Expression ts) = matchTerms uts ts

matchTerms :: [Term Unbound] -> [Term Symbol] -> Interpreter (Scope Value)
matchTerms [] [] = fmap snd ask
matchTerms (Term (Unbound us) : uts) (Term s : ts)
    | us == s   = matchTerms uts ts
    | otherwise = throwError NotMatches
matchTerms (Structure expr1 : uts) (Structure expr2 : ts) = match expr1 expr2 >>= chngVals (matchTerms uts ts) . (<>)
matchTerms (Term (UnboundVariable vname vtype) : uts) ts = try `catchError` catcher where
    try = do
        Expression val <- getVal vname
        remain <- helper ts val
        matchTerms uts remain

    helper [] [] = return [] :: Interpreter [Term Symbol]
    helper (x:xs) (y:ys)
        | x == y    = helper xs ys
        | otherwise = throwError NotMatches
    helper xs [] = return xs
    helper _ _   = throwError NotMatches

    catcher (NotInScope _) = bindThenMatch vtype vname uts ts
    catcher e              = throwError e
matchTerms _ _ = throwError NotMatches

bindThenMatch :: Type -> Name -> [Term Unbound] -> [Term Symbol] -> Interpreter (Scope Value)
bindThenMatch ExpressionType vname uts ts = helper [] ts where
    helper cur remain = try `catchError` catcher where
        try = chngVals (matchTerms uts remain) $ addToScope (vname, Expression cur)
        catcher :: Error -> Interpreter (Scope Value)
        catcher _ = case remain of
            [] -> throwError NotMatches
            _  -> helper (cur ++ [head remain]) (tail remain)
bindThenMatch vtype vname uts ts = case ts of
        []   -> throwError NotMatches
        t:remain -> case vtype of
            TermType -> chngVals (matchTerms uts remain) $ addToScope (vname, Expression [t])
            _        -> case t of
                Term _ -> chngVals (matchTerms uts remain) $ addToScope (vname, Expression [t])
                _      -> throwError NotMatches

lookupScope :: Name -> Scope a -> Interpreter a
lookupScope name (Scope list) = case lookup name list of
    Nothing -> throwError $ NotInScope name
    Just a  -> return a

getFun :: Name -> Interpreter Function
getFun name = do
    (funs, _) <- lift ask
    lookupScope name funs

getVal :: Name -> Interpreter Value
getVal name = do
    (_, vals) <- lift ask
    lookupScope name vals

chngVals :: Interpreter a -> (Scope Value -> Scope Value) -> Interpreter a
chngVals ipr chng = mapExceptT (local (fmap chng)) ipr
