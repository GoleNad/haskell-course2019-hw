module RefalArithmetics (arithmeticFuns) where

import RefalAst

import Control.Monad.Except

import Data.List

arithmeticFuns :: Scope Function
arithmeticFuns = Scope
    [ (Name "+"      , createFun $ operator (+)          )
    , (Name "-"      , createFun $ operator (-)          )
    , (Name "*"      , createFun $ operator (*)          )
    , (Name "/"      , createFun $ operator (div)        )
    , (Name "%"      , createFun $ operator (mod)        )
    , (Name "Divmod" , createFun divmod                  )
    , (Name "Compare", createFun RefalArithmetics.compare)
    ]

createFun :: (Integer -> Integer -> Value) -> Function
createFun f = BuiltIn $ \expr -> takeArgs expr >>= (return . uncurry f)

operator :: (Integer -> Integer -> Integer) -> Integer -> Integer -> Value
operator op v1 v2 = convertBack $ v1 `op` v2

compare :: Integer -> Integer -> Value
compare v1 v2
    | v1 < v2   = simpleExpr $ Identifier $ Name "-"
    | v1 > v2   = simpleExpr $ Identifier $ Name "+"
    | otherwise = simpleExpr $ Number 0

divmod :: Integer -> Integer -> Value
divmod v1 v2 = Expression $ (Structure quotient) : remainder where
    quotient = convertBack $ v1 `div` v2
    Expression remainder = convertBack $ v1 `mod` v2

takeArgs :: Expression Symbol -> Interpreter (Integer, Integer)
takeArgs (Expression (Structure expr : ts))                   = convertArgs expr $ Expression ts
takeArgs (Expression (Term (Identifier (Name "-")) : t : ts)) = fmap (\(v1, v2) -> (-v1, v2)) $ convertArgs (Expression [t]) $ Expression ts
takeArgs (Expression (Term (Identifier (Name "+")) : t : ts)) = convertArgs (Expression [t]) $ Expression ts
takeArgs (Expression (t : ts))                                = convertArgs (Expression [t]) $ Expression ts
takeArgs _                                                    = throwError $ Error "Invalid arguments' format"

convertArgs :: Expression Symbol -> Expression Symbol -> Interpreter (Integer, Integer)
convertArgs expr1 expr2 = do
    v1 <- convert expr1
    v2 <- convert expr2
    return (v1, v2)

convert :: Expression Symbol -> Interpreter Integer
convert = fmap fst . foldr op (return (0, 0)) where
    op :: Symbol -> Interpreter (Integer, Integer) -> Interpreter (Integer, Integer)
    op (Number n) v = fmap (\(result, offset) -> (result + n * 2^offset, offset + 32)) v
    op _ _          = throwError $ Error "Invalid argument: NaN"

convertBack :: Integer -> Expression Symbol
convertBack n | n < 0     = Expression $ Term (Identifier (Name "-")) : v
              | otherwise = convertBackUnsigned n where
                  Expression v = convertBackUnsigned (-n)

convertBackUnsigned :: Integer -> Expression Symbol
convertBackUnsigned n
    | n == 0    = simpleExpr $ Number 0
    | otherwise = Expression $ unfoldr op n where
        m = 0x100000000 :: Integer
        op remain | remain > 0 = Just (Term $ Number $ remain `mod` m, remain `div` m)
                  | otherwise  = Nothing
