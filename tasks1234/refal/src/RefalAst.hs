module RefalAst where

import Control.Monad.Except
import Control.Monad.Reader
import Data.Foldable

newtype Scope a = Scope [(Name, a)]
    deriving Show

type Value = Expression Symbol

type Interpreter = ExceptT Error (Reader (Scope Function, Scope Value))

data Error = Error String
           | NotInScope Name
           | NotExhaustiveMatching
           | NotMatches
           deriving (Show, Eq)

data Function = BuiltIn (Value -> Interpreter Value)
              | Defined Block

newtype Block = Block [Sentence]
    deriving Show

newtype Name = Name String
    deriving (Show, Eq)

data Sentence = Sentence (Expression Unbound) [Condition] Substitution
    deriving Show

data Condition = Condition (Expression Unbound) (Expression Evaluable)
    deriving (Show, Eq)

data Substitution = ExpressionSubstitution (Expression Evaluable)
                  | BlockSubstitution Block (Expression Evaluable)
                  deriving Show

newtype Expression a = Expression [Term a]
    deriving (Show, Eq)

data Term a = Term a
            | Structure (Expression a)
            deriving (Show, Eq)

data Unbound = Unbound Symbol
             | UnboundVariable Name Type
             deriving (Show, Eq)

data Evaluable = Evaluable Unbound
               | FunctionApplication Name (Expression Evaluable)
               deriving (Show, Eq)

data Symbol = Character Char
            | Identifier Name
            | Number Integer
            | RealNumber Double
            deriving (Show, Eq)

data Type = SymbolType | TermType | ExpressionType
    deriving (Show, Eq)

simpleExpr :: a -> Expression a
simpleExpr = Expression . (:[]) . Term

joinExpr :: Expression (Expression a) -> Expression a
joinExpr (Expression terms) = fold $ fmap joinTerm terms

joinTerm :: Term (Expression a) -> Expression a
joinTerm (Term expr) = expr
joinTerm (Structure expr) = Expression [Structure $ joinExpr expr]

addToScope :: (Name, a) -> Scope a -> Scope a
addToScope pair = (Scope [pair] <>)

instance Show Function where
    show (BuiltIn _)   = "builtin"
    show (Defined blk) = "Defined " ++ show blk

instance Semigroup (Scope a) where
    (Scope a) <> (Scope b) = Scope $ a ++ b

instance Semigroup (Expression a) where
    (Expression terms1) <> (Expression terms2) = Expression $ terms1 ++ terms2

instance Monoid (Expression a) where
    mempty = Expression []

instance Functor Expression where
    fmap f (Expression terms) = Expression $ fmap (fmap f) terms

instance Foldable Expression where
    foldr f b (Expression terms) = foldr (\term r -> foldr f r term) b terms

instance Traversable Expression where
    traverse f (Expression terms) = fmap Expression $ traverse (traverse f) terms

instance Functor Term where
    fmap f (Term a) = Term $ f a
    fmap f (Structure expr) = Structure $ fmap f expr

instance Foldable Term where
    foldr f b (Term a) = f a b
    foldr f b (Structure expr) = foldr f b expr

instance Traversable Term where
    traverse f (Term a) = fmap Term $ f a
    traverse f (Structure expr) = fmap Structure $ traverse f expr
