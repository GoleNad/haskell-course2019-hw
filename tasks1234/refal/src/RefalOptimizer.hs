module RefalOptimizer (optimizeRefal) where

import RefalAst
import RefalInterpreter

import Control.Monad.Reader
import Control.Monad.Writer

------------------------------------------------------------

optimizeRefal :: Optimizable a => Scope Function -> a -> a
optimizeRefal funs a = fst $ runReader (runWriterT $ optimizer a) (funs, [])

------------------------------------------------------------

class Optimizable a where
    optimizer :: a -> Optimizer a

instance Optimizable a => Optimizable (Scope a) where
    optimizer (Scope decls) = fmap Scope $ traverse (traverse optimizer) decls

instance Optimizable Function where
    optimizer (Defined blk) = fmap Defined $ optimizer blk
    optimizer f = return f

instance Optimizable Block where
    optimizer (Block ss) = do
        let sensIsFeasible (Sentence pattern conds _) = do
                let condIsNotFeasible (Condition p e) = fmap (== AlwaysFalse) $ checker e p
                notFeasible <- addVars pattern $ filterM condIsNotFeasible conds
                return $ notFeasible == []

        optSens <- traverse optimizer ss
        feasibleSens <- filterM sensIsFeasible optSens

        fmap Block $ takeUntilM (\(Sentence pattern _ _) -> isUltimate pattern) feasibleSens

instance Optimizable Sentence where
    optimizer (Sentence pattern conds subst) = do
        optconds <- traverse optimizer conds
        addVars pattern $ do
            optsubst <- optimizer subst
            let notUltimate (Condition p e) = do
                    res <- isUltimate p
                    fmap ((&& not res) . (/= AlwaysTrue)) $ checker e p
            nuConds <- filterM notUltimate optconds
            return $ Sentence pattern nuConds optsubst

instance Optimizable Condition where
    optimizer (Condition pattern expr) = fmap (Condition pattern) $ optimizer expr

instance Optimizable Substitution where
    optimizer (ExpressionSubstitution expr) = fmap ExpressionSubstitution $ optimizer expr
    optimizer (BlockSubstitution (Block sens) arg) = do
        fsens <- filterM (\(Sentence pattern _ _) -> fmap (/= AlwaysFalse) $ checker arg pattern) sens
        optblk <- optimizer (Block fsens)
        optarg <- optimizer arg
        return $ BlockSubstitution optblk optarg

instance ExprPart a => Optimizable (Expression a) where
    optimizer = fmap joinExpr . traverse partOptimizer

------------------------------------------------------------

class ExprPart a where
    partOptimizer :: a -> Optimizer (Expression a)

instance ExprPart Symbol where
    partOptimizer a = addSize 1 >> (return $ simpleExpr a)

instance ExprPart Unbound where
    partOptimizer a@UnboundVariable {} = setFalse >> addSize 1 >> (return $ simpleExpr a)
    partOptimizer (Unbound s)          = fmap (fmap Unbound) $ partOptimizer s

instance ExprPart Evaluable where
    partOptimizer (Evaluable ub) = fmap (fmap Evaluable) $ partOptimizer ub
    partOptimizer (FunctionApplication fname arg) = do
        (optArg, OptiData flag size) <- listen $ optimizer arg
        scope <- getFuns

        let fa = simpleExpr $ FunctionApplication fname optArg
            retfa = setFalse >> addSize size >> return fa

        if not flag then retfa else case interpret scope fa of
            Left _    -> retfa
            Right opt -> let
                    optSize = foldl (const . (+1)) 0 opt :: Integer
                    optExpr = fmap (Evaluable . Unbound) opt
                in
                    if optSize <= size then
                        addSize optSize >> return optExpr
                    else
                        retfa

------------------------------------------------------------

class Checkable a where
    checker :: a Evaluable -> a Unbound -> Optimizer CheckResult

instance Checkable Expression where
    checker (Expression e) (Expression u) = fmap (foldl (<>) AlwaysTrue) $ traverse (uncurry checker) $ zip e u

instance Checkable Term where
    checker (Term (Evaluable ev)) (Term un)
        | ev == un  = return AlwaysTrue
        | otherwise = helper ev un where
            helper :: Unbound -> Unbound -> Optimizer CheckResult
            helper Unbound{} Unbound{}                  = return AlwaysFalse
            helper UnboundVariable{} Unbound{}          = return Possible
            helper _ (UnboundVariable _ ExpressionType) = return Possible
            helper (UnboundVariable _ ExpressionType) _ = return Possible
            helper _ (UnboundVariable name _) = do
                vars <- getVars
                return $ if name `elem` vars then Possible else AlwaysTrue
    checker Structure{} Term{} = return AlwaysFalse
    checker Term{} Structure{} = return AlwaysFalse
    checker (Structure eve) (Structure une) = checker eve une
    checker _ _ = return Possible

isUltimate :: Expression Unbound -> Optimizer Bool
isUltimate (Expression terms) = do
    let isBound (Term (UnboundVariable name _)) = getVars >>= return . elem name
        isBound _ = return False

        isExpr (Term (UnboundVariable _ ExpressionType)) = True
        isExpr _ = False

        isTerm (Term (UnboundVariable _ TermType)) = True
        isTerm _ = False

    bound <- filterM isBound terms
    return $ bound == [] && any isExpr terms && all (\x -> isExpr x || isTerm x) terms

------------------------------------------------------------

takeUntilM :: Monad m => (a -> m Bool) -> [a] -> m [a]
takeUntilM _ [] = return []
takeUntilM p (x : xs) = do
    res <- p x
    if res == True then
        return [x]
    else
        takeUntilM p xs >>= return . (x :)

addSize :: Integer -> Optimizer ()
addSize = tell . OptiData True

setFalse :: Optimizer ()
setFalse = tell $ OptiData False 0

getFuns :: Optimizer (Scope Function)
getFuns = fmap fst ask

getVars :: Optimizer [Name]
getVars = fmap snd ask

addVars :: Expression Unbound -> Optimizer a -> Optimizer a
addVars expr ipr = do
    oldVars <- getVars
    let helper vars Unbound{} = vars
        helper vars (UnboundVariable name _)
            | name `elem` vars = vars
            | otherwise = name : vars

    mapWriterT (local (fmap $ const $ foldl helper oldVars expr)) ipr

------------------------------------------------------------

type Optimizer = WriterT OptiData (Reader (Scope Function, [Name]))

data OptiData = OptiData Bool Integer

instance Semigroup OptiData where
    (OptiData f1 s1) <> (OptiData f2 s2) = OptiData (f1 && f2) (s1 + s2)

instance Monoid OptiData where
    mempty = OptiData True 0

data CheckResult = AlwaysFalse
                 | Possible
                 | AlwaysTrue
                 deriving Eq

instance Semigroup CheckResult where
    AlwaysFalse <> _           = AlwaysFalse
    _           <> AlwaysFalse = AlwaysFalse
    Possible    <> _           = Possible
    _           <> Possible    = Possible
    _           <> _           = AlwaysTrue
