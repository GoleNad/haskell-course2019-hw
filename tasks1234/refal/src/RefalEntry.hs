module RefalEntry (parseRefal, showRefal) where

import RefalAst

import Control.Monad.Reader
import Control.Monad.Writer
import Control.Monad.State
import Data.Foldable
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

------------------------------------------------------------

parseRefal :: RefalEntry a => String -> Either String a
parseRefal s = case fst $ runState (runParserT parser "" s) Nothing of
    Left e  -> Left $ show e
    Right a -> Right a

showRefal :: RefalEntry a => a -> String
showRefal a = snd $ runReader (runWriterT $ printer a) ""

------------------------------------------------------------

class RefalEntry a where
    parser    :: Parser a
    printer   :: a -> Printer

instance RefalEntry a => RefalEntry (Scope a) where
    parser = fmap Scope $ space >> many parser

    printer (Scope [])           = return ()
    printer (Scope (decl:decls)) = printer decl >> traverse_ (\x -> tell "\n\n" >> printer x) decls

instance (RefalEntry a, RefalEntry n) => RefalEntry (n, a) where
    parser = do
        n <- parser
        a <- parser
        return $ (n, a)

    printer (n, a) = printer n >> tell " " >> printer a

instance RefalEntry Function where
    parser = fmap Defined parser

    printer (BuiltIn _)   = tell "builtin"
    printer (Defined blk) = printer blk

instance RefalEntry Block where
    parser = fmap Block $ between blockBegin blockEnd $ many parser

    printer (Block blk) = do
        tell "{\n"
        incPrefix $ traverse_ (\x -> printer x >> tell "\n") blk
        prefix >> tell "}"

instance RefalEntry Sentence where
    parser = do
        p <- parser
        cs <- many parser
        s <- parser
        return $ Sentence p cs s

    printer (Sentence pattern conds subst) = do
        prefix >> printer pattern
        traverse_ printer conds
        printer subst

instance RefalEntry Condition where
    parser = do
        e <- symbolWhere >> parser
        p <- symbolIs >> parser
        return $ Condition p e

    printer (Condition pattern expr) = tell " & " >> printer expr >> tell " : " >> printer pattern

instance RefalEntry Substitution where
    parser = exprSubst <|> blkSubst where
        exprSubst = do
            symbolReplaceBy
            expr <- parser
            symbolOtherwise
            return $ ExpressionSubstitution expr
        blkSubst = do
            e <- symbolWith >> parser
            b <- symbolIs >> parser
            return $ BlockSubstitution b e

    printer (ExpressionSubstitution expr) = tell " = " >> printer expr >> tell ";"
    printer (BlockSubstitution blk expr)  = tell ", " >> printer expr >> tell " : " >> printer blk

instance RefalEntry a => RefalEntry (Expression a) where
    parser = fmap Expression $ many parser

    printer (Expression [])           = return ()
    printer (Expression (term:terms)) = printer term >> traverse_ (\x -> tell " " >> printer x) terms

instance RefalEntry a => RefalEntry (Term a) where
    parser = fmap Term parser <|> (fmap Structure $ between structureBegin structureEnd $ parser)

    printer (Term a)         = printer a
    printer (Structure expr) = tell "(" >> printer expr >> tell ")"

instance RefalEntry Evaluable where
    parser = fmap Evaluable parser <|> funApp where
        funApp = between evaluationBegin evaluationEnd $ do
            n <- parser
            e <- parser
            return $ FunctionApplication n e

    printer (Evaluable a)                  = printer a
    printer (FunctionApplication name arg) = do
        tell "<" >> printer name
        case arg of
            Expression [] -> return ()
            _             -> tell " " >> printer arg
        tell ">"

instance RefalEntry Unbound where
    parser = try var <|> (fmap Unbound $ parser) where
        var = do
            t <- parser
            n <- symbolIndexFollows >> parser
            return $ UnboundVariable n t

    printer (Unbound a)                   = printer a
    printer (UnboundVariable vname vtype) = printer vtype >> tell "." >> printer vname

instance RefalEntry Symbol where
    parser = get >>= \ch -> case ch of
        Nothing -> do
            c <- char '\'' <|> char '"' <|> return 'a'
            if c /= 'a' then
                put (Just c) >> parser
            else try (fmap Number     $ lexeme $ L.decimal)
             <|> try (fmap RealNumber $ lexeme $ L.float  )
             <|> try (fmap Identifier $ parser            )
        Just c  -> do
            res <- fmap Character $ L.charLiteral
            (char c >> put Nothing >> space) <|> return ()
            return res

    printer (Character c)   = tell $ show c
    printer (Identifier s)  = printer s
    printer (Number n)      = tell $ show n
    printer (RealNumber rn) = tell $ show rn

instance RefalEntry Type where
    parser = (symbol "s" >> return SymbolType    )
         <|> (symbol "t" >> return TermType      )
         <|> (symbol "e" >> return ExpressionType)

    printer SymbolType     = tell "s"
    printer TermType       = tell "t"
    printer ExpressionType = tell "e"

instance RefalEntry Name where
    parser = fmap Name $ lexeme $ some notSpecialChar

    printer (Name s) = tell s

------------------------------------------------------------

specialChars :: [Char]
specialChars = ['(', ')', ',', '&', '\'', '"', '=', '{', '}', '.', '<', '>', ':', ';']

notSpecialChar :: Parser Char
notSpecialChar = do
    nc <- lookAhead (alphaNumChar <|> symbolChar <|> punctuationChar)
    if nc `elem` specialChars then
        fail ""
    else
        (alphaNumChar <|> symbolChar <|> punctuationChar)

symbolReplaceBy :: Parser ()
symbolReplaceBy = symbol "="

symbolOtherwise :: Parser ()
symbolOtherwise = symbol ";"

symbolIs :: Parser ()
symbolIs = symbol ":"

symbolWhere :: Parser ()
symbolWhere = symbol "&"

symbolWith :: Parser ()
symbolWith = symbol ","

symbolIndexFollows :: Parser ()
symbolIndexFollows = symbol "."

blockBegin :: Parser ()
blockBegin = symbol "{"

blockEnd :: Parser ()
blockEnd = symbol "}"

evaluationBegin :: Parser ()
evaluationBegin = symbol "<"

evaluationEnd :: Parser ()
evaluationEnd = symbol ">"

structureBegin :: Parser ()
structureBegin = symbol "("

structureEnd :: Parser ()
structureEnd = symbol ")"

lexeme :: Parser a -> Parser a
lexeme = L.lexeme space

symbol :: String -> Parser ()
symbol s = L.symbol space s >> return ()

------------------------------------------------------------

prefix :: Printer
prefix = ask >>= tell

incPrefix :: Printer -> Printer
incPrefix = mapWriterT (local ("    " ++))

------------------------------------------------------------

type Parser = ParsecT Void String (Control.Monad.State.State (Maybe Char))
type Printer = WriterT String (Reader String) ()
