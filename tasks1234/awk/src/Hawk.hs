module Hawk where

{-data Section
  = Begin {beforeAll :: Stmt}
  | Middle {forLines :: Stmt}
  | End {afterAll :: Stmt}
  deriving (Show)
-}
data AValue 
  = VInt Integer
  | VDouble Double
  | VString String
  | PosVar {position :: Integer} -- positional variable
  | FS                     -- field separator
  | RS                     -- row separaator
  | OFS                    -- output field separator
  | ORS                    -- output row separator
  | NF                     -- number of fields (on a line)
  | NR                     -- number of rows (in file)
  deriving (Show)

{- data BuiltINVars
  = PositionalVar {valPos :: Int}
  | FS {valFS :: String}
  | OFS {valOFS :: String}
  | NF {valNF :: String}
  | NR {valNR :: Int}
  | RS {valRS :: Int}
  deriving (Show)
-}

data BBinOp
  = Add
  | Subt
  | Mul
  | Div
  | Mod
  | Concat
  deriving (Show)

data RBinOp
  = Eq
  | NotEq
  | Less
  | Gr
  | LE
  | GE
  deriving (Show)

data Expr
  = Literal AValue
  | Var String
  | IndVar (String, Expr)
  | BExpr BBinOp Expr Expr
  | RExpr RBinOp Expr Expr
  | Neg Expr
  | FCall {funcName :: String, funcArgs :: [Expr]}
  deriving (Show)

data Stmt
  = Seq [Stmt]
  | BeginSection {beforeAll :: Stmt}
  | MiddleSection {forLines :: Stmt}
  | EndSection {afterAll :: Stmt}
  | FSAssign {valueFS :: String}
  | RSAssign {valueRS :: String}
  | OFSAssign {valueOFS :: String}
  | ORSAssign {valueORS :: String}
  | While {cond :: Expr, body :: Stmt}
  | If {cond :: Expr, trueCase :: Stmt, falseCase :: Stmt}
  | VarAssign {name :: String, value :: Expr}
  | IndVarAssign {name :: String, index :: Expr, value :: Expr}
  | FuncDeclaration {name :: String, argNames :: [String], body :: Stmt}
  | FuncCall {name :: String, args :: [Expr]}
  | Return {value :: Expr}
  | Print {valsToPrint :: [Expr]}
  deriving (Show)
