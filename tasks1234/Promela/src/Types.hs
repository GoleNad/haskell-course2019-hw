module Types where

    import Data.Int
    
    data Types =
       TInt
     | TBool
     | TBit
     | TByte
     | TShort
     | Tmtype
     deriving (Show, Eq)

    data Type =
       IntVal Int
     | BoolVal Bool
     | BitVal Bool
     | ByteVal Int8
     | MtypeVal String
     | ShortVal Int16
     | Array Int
     deriving (Show, Eq)
    
    data Exp =     
       Const Type
     | ArrayVal String Int
     | Variable String
     | BinExp BinOp Exp Exp
     | UnExp UnOp Exp
     deriving (Show, Eq)

    data BinOp = 
       Add
     | Sub                                         
     | Mul                                        
     | Div
     | Equal
     | Greater
     | Less
     | And 
     | Or
     deriving (Show, Eq)
    
    data UnOp = 
       Not
     | Neg
     deriving (Show, Eq)

    data Statement = 
       Assig Types String Exp  -- type name value
     | VarDecl Types String -- type name
     | ChangeVar String Exp -- name new value
     | ArrayDec Types String Int -- type name size
     | ArrayChange String Int Exp -- name index new value
     | MtypeDecl [String]
     | Proctype { procName::String, activeNum::Int, args::[(Types, String)], procBody::[Statement] }
     | Init [Statement]
     | Chan { chanName::String, size::Int, types::[Types] }
     | If [(Exp, Statement)]  
     | Loop [(Exp, Statement)]
     | ConditionStmt (Exp, Statement)
     | Send String [Exp] -- name of chanel, list of sending values (chanel can be like: chan ch = [1] of { int, bool, int };)
     | Receive String [String] -- name of chanel, name of varuables to receive
     | Atomic [Statement]
     | RunProc String [Exp]
     | GotoLoop String -- name of Loop (example - again: ...)
     | Goto String -- name of loop 
     | Break
     | Skip
     deriving (Show, Eq)

    data Error = 
       VariableNotInScope String
     | WrongOperation String
     | VarAlreadyExist String
     | VarNotExist String
     | TypeMismatch String
     | ProctypeAlreadyExist String
     | NeedMoreArgs String
     | ProcNotExist String
     | ExprCanNotEval Exp
     | IndexMoreThanSize String
     | NoSuchIndex Int
     | BreakNotInLoop
     | GotoNotInProc
     | ProgWillNotEnd
     deriving Show

    type Program = [Statement]