module Parser where
import Text.Megaparsec
import Text.Megaparsec.Char
import Control.Monad.Combinators.Expr
import Data.Void
import qualified Text.Megaparsec.Char.Lexer as L
import Types

type Parser = Parsec Void String

spaceParser :: Parser()
spaceParser = L.space space1 lineComment blockComment 
    where
    lineComment = L.skipLineComment "//"
    blockComment = L.skipBlockComment "/*" "*/"

lexeme :: Parser a -> Parser a
lexeme = L.lexeme spaceParser

symbol :: String -> Parser String
symbol = L.symbol spaceParser

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

braces :: Parser a -> Parser a
braces = between (symbol "{") (symbol "}")

brackets :: Parser a -> Parser a
brackets  = between (symbol "[") (symbol "]")

semicolon :: Parser String
semicolon = symbol ";"

comma :: Parser String
comma = symbol ","

word :: String -> Parser ()
word w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)

keywords :: [String] 
keywords =
    [ "if", "else", "fi", "od", "do",
    "true", "false", "int", "bool",
    "bit", "short", "mtype", "byte",
    "chan", "break", "skip", "goto",
    "proctype", "atomic"]


identifier :: Parser String
identifier = (lexeme . try) (p >>= check)
    where
    p       = (:) <$> letterChar <*> many (alphaNumChar <|> char '_' <|> char '-')
    check x =
        if x `elem` keywords
        then fail $ "keyword " ++ show x ++ " cannot be an identifier"
        else return x

typeParser :: Parser Types
typeParser = do
    word "int"
    return $ TInt
    <|> do
        word "bool"
        return $ TBool
    <|> do
        word "short"
        return $ TShort
    <|> do
        word "bit"
        return $ TBit
    <|> do
        word "byte"
        return $ TByte
    <|> do
        word "mtype"
        return $ Tmtype

programParser :: Parser Program
programParser = do
    spaceParser
    sepEndBy declParser semicolon

declParser :: Parser Statement
declParser = try assigParser
    <|> try mtypeParser
    <|> try varDeclParser
    <|> try changeVarParser
    <|> try arrayDecParser
    <|> try arrayChangeParser
    <|> try procParser
    <|> try initParser
    <|> try chanParser
    <|> try sendParser
    <|> try receiveParser

assigParser :: Parser Statement
assigParser = do
    varType <- typeParser
    name <- identifier
    symbol "="
    exp <- expParser
    return ( Assig varType name exp ) 

varDeclParser :: Parser Statement
varDeclParser = do
    varType <- typeParser
    name <- identifier
    return ( VarDecl varType name )

changeVarParser :: Parser Statement
changeVarParser = do
    name <- identifier
    symbol "="
    exp <- expParser
    return ( ChangeVar name exp )

arrayDecParser :: Parser Statement
arrayDecParser = do
    varType <- typeParser
    name <- identifier
    size <- brackets L.decimal
    return ( ArrayDec varType name size )

arrayChangeParser :: Parser Statement
arrayChangeParser = do
    name <- identifier
    index <- brackets L.decimal
    symbol "="
    exp <- expParser
    return ( ArrayChange name index exp )

procParser :: Parser Statement
procParser =  do
        word "active"
        amount <- brackets L.decimal
        word "proctype"
        name <- identifier
        args <- parens $ sepBy argParser comma
        procBody <- braces ( many (stmtParser <* (optional semicolon) <|> gotoLoopParser))
        return ( Proctype name amount args procBody )
    <|> do 
        word "proctype"
        name <- identifier
        args <- parens $ sepBy argParser comma
        procBody <- braces ( many (stmtParser <* (optional semicolon) <|> gotoLoopParser))
        return ( Proctype name 0 args procBody )

initParser :: Parser Statement
initParser =  do 
    word "init"
    initBody <- braces ( many (stmtParser <* (optional semicolon) <|> gotoLoopParser))
    return ( Init initBody)

chanParser :: Parser Statement
chanParser =  do 
    word "chan"
    name <- identifier
    symbol "="
    size <- brackets L.decimal
    word "of"
    types <- braces $ sepBy typeParser comma
    return ( Chan name size types )

mtypeParser :: Parser Statement
mtypeParser = do
    word "mtype"
    symbol "="
    enums <- braces $ sepBy identifier comma
    return ( MtypeDecl enums ) 


argParser :: Parser (Types, String)
argParser = do
    varType <- typeParser
    name <- identifier
    return (varType, name)

stmtParser :: Parser Statement
stmtParser = try assigParser
    <|> try varDeclParser
    <|> try changeVarParser
    <|> try arrayDecParser
    <|> try arrayChangeParser
    <|> try ifParser
    <|> try loopParser
    <|> try conditionStmtParser
    <|> try sendParser
    <|> try receiveParser
    <|> try atomicParser
    <|> try runProcParser
    <|> try gotoParser
    <|> try breakParser
    <|> try skipParser

ifParser :: Parser Statement
ifParser = do
    word "if"
    cond_stmts <- sepEndBy cond_stmtParser semicolon
    word "fi"
    return ( If cond_stmts )

loopParser :: Parser Statement
loopParser = do
    word "do"
    cond_stmts <- sepEndBy cond_stmtParser semicolon
    word "od"
    return ( Loop cond_stmts )

conditionStmtParser :: Parser Statement
conditionStmtParser = do
    exp <- parens expParser
    word "->"
    stmt <- stmtParser
    return (ConditionStmt (exp, stmt))

sendParser :: Parser Statement
sendParser = do
    name <- identifier
    symbol "!"
    vars <- sepBy expParser comma
    return ( Send name vars )

receiveParser :: Parser Statement
receiveParser = do
    name <- identifier
    symbol "?"
    vars <- sepBy identifier comma
    return ( Receive name vars )

atomicParser :: Parser Statement
atomicParser = do
    word "atomic"
    body <- braces $ sepEndBy stmtParser semicolon
    return ( Atomic body )

runProcParser :: Parser Statement
runProcParser = do
    word "run"
    name <- identifier
    args <- parens $ sepBy expParser comma
    return ( RunProc name args )

gotoLoopParser :: Parser Statement
gotoLoopParser = do
    name <- identifier
    symbol ":"
    return ( GotoLoop name )


gotoParser :: Parser Statement
gotoParser = do
    word "goto"
    name <- identifier
    return ( Goto name)

breakParser :: Parser Statement
breakParser = do
    word "break"
    return ( Break )

skipParser :: Parser Statement
skipParser = do
    word "skip"
    return ( Skip )

cond_stmtParser :: Parser (Exp, Statement)
cond_stmtParser = try ( do
        word "::"
        exp <- parens expParser
        word "->"
        stmt <- stmtParser
        return (exp, stmt) )
    <|> try ( do
        word "::"
        stmt <- stmtParser
        return ( Const $ BoolVal True, stmt ) )
    <|> try ( do
        word "::"
        word "else"
        word "->"
        stmt <- stmtParser
        return ( Const $ BoolVal True, stmt ) )

term :: Parser Exp
term = do 
    try space
    try $ parens expParser
    <|> try arrayValParser
    <|> try variableParser
    <|> try constParser

expParser :: Parser Exp
expParser = makeExprParser term table

table :: [[ Operator Parser Exp ]]
table = [
    [ Prefix (UnExp Neg <$ symbol "-") 
    , Prefix (UnExp Not <$ symbol "!") ]
    , 
    [ InfixL (BinExp Mul <$ symbol "*") 
    , InfixL (BinExp Div <$ symbol "/") ]
    , 
    [ InfixL (BinExp Add <$ symbol "+")
    , InfixL (BinExp Sub <$ symbol "-")   ]
    ,
    [ InfixN (BinExp Less  <$ symbol "<" )
    , InfixN (BinExp Greater  <$ symbol ">" )  ]
    ,
    [ InfixN (BinExp Equal  <$ symbol "==") ]
    ,
    [ InfixL (BinExp And <$ symbol "&&") ]
    ,
    [ InfixL (BinExp Or <$ symbol "||" ) ]
    ]


constParser :: Parser Exp
constParser = do
    word "true"
    return ( Const $ BoolVal True )
    <|> do
        word "false"
        return ( Const $ BoolVal False )
    <|> do
        int <- L.decimal
        return ( Const $ IntVal int )
    <|> do
        mtype <- identifier
        return ( Const $ MtypeVal mtype )

arrayValParser :: Parser Exp
arrayValParser = do
    name <- identifier
    index <- brackets L.decimal
    return ( ArrayVal name index )

variableParser :: Parser Exp
variableParser = do
    name <- identifier
    return ( Variable name )