{-# OPTIONS_GHC -fwarn-incomplete-patterns -fwarn-incomplete-uni-patterns #-}

module Interpreter where
import qualified Data.Map.Strict as Map
import Data.List
import Types
import Parser

type Var = (Types, Type)
type Proc = ([(Types, String)], [Statement]) -- (args, body)
type Chanel = ([Types], [[Exp]], Int) 

data Env = Env (Map.Map String Var) (Map.Map String Proc) (Map.Map String Chanel) [(Proc, Int)] -- look in README
    deriving Show

type ErrExp = Either Error (Env, Exp)
type ErrStmt = Either Error (Env, Bool)

createEnv :: Env
createEnv = Env Map.empty Map.empty Map.empty []

manager :: Program -> Env -> IO ()

manager [] env = progManager env 0

manager stmtList env = case exec env (head stmtList) of
    Left error -> putStrLn $ show error
    Right (newEnv, True) -> manager (tail stmtList) newEnv
    Right _ -> putStrLn "wrong program - it will not end"

progManager :: Env -> Int -> IO ()

progManager (Env vars procs chanels []) numberOfPair = 
    putStrLn ("vars - " ++ (show vars) ++ " chanels - " ++ (show chanels) ++ " program ended")

progManager env@(Env vars procs chanels list) numberOfPair = 
    case (lengthOfCurrentStmtList list numberOfPair, currentStmtNumber list numberOfPair) of
        (Just a, Just b) -> if b >= a then
            case currentPair list numberOfPair of
                Nothing -> putStrLn (show (NoSuchIndex numberOfPair))
                Just a -> toNextPair1 (Env vars procs chanels (delete a list)) numberOfPair
        else
            case currentStmt list numberOfPair of
                Nothing -> putStrLn (show (NoSuchIndex numberOfPair))
                Just a -> case exec env (a) of
                    Left error -> putStrLn $ show error
                    Right (_, False) -> toNextPair env numberOfPair
                    Right ((Env newVars newProcs newChanels newList), True) -> do
                        putStrLn ("vars - " ++ (show vars) ++ " chanels - " ++ (show chanels))
                        progManager (Env newVars newProcs newChanels (changeList (splitAt numberOfPair newList))) numberOfPair
        _ -> putStrLn (show (NoSuchIndex numberOfPair))
    where
        currentPair list numberOfPair = listIndex list numberOfPair
        currentProc list numberOfPair = do
            currPair <- currentPair list numberOfPair
            case currPair of
                a -> return (fst a)
        currentStmtList list numberOfPair = do
            currProc <- currentProc list numberOfPair
            case currProc of
                a -> return (snd a) 
        currentStmtNumber list numberOfPair = do
            currPair <- currentPair list numberOfPair
            case currPair of
                a -> return (snd a) 
        currentStmt list numberOfPair = do
            currStmt <- currentStmtList list numberOfPair
            num <- currentStmtNumber list numberOfPair
            case (currStmt, num) of
                (a,b) -> listIndex a b
            
        lengthOfCurrentStmtList list numberOfPair = do
            currStmt <- currentStmtList list numberOfPair
            case currStmt of
                a -> return (length a)
        toNextPair env@(Env vars procs chanels []) numberOfPair = 
            putStrLn ("vars - " ++ (show vars) ++ " chanels - " ++ (show chanels) ++ " program ended")
        toNextPair env@(Env vars procs chanels list) numberOfPair = 
            if length list <= (numberOfPair+1) then
                progManager env 0
            else
                progManager env (numberOfPair+1)
        toNextPair1 env@(Env vars procs chanels []) numberOfPair = 
            putStrLn ("vars - " ++ (show vars) ++ " chanels - " ++ (show chanels) ++ " program ended")
        toNextPair1 env@(Env vars procs chanels list) numberOfPair = 
            if length list <= (numberOfPair+1) then
                progManager env 0
            else
                progManager env numberOfPair -- not numberOfPair + 1 because we deleted before calling
        changeList (list1, (proc, num) : list2) = list1 ++ ((proc, (num+1)) : list2)
        changeList (list1, []) = list1
        listIndex [] index = Nothing
        listIndex (x : xs) 0 = Just x
        listIndex (x : xs) index = listIndex xs (index-1)


exec :: Env -> Statement -> ErrStmt

exec env@(Env vars procs chanels list) (Assig varType name exp) = do
    var <- eval env exp
    case Map.lookup name vars of
        Just _ -> Left (VarAlreadyExist name)
        Nothing -> case var of 
            (_, Const a) -> return (Env (Map.insert name (varType, a) vars)  procs chanels list, True)
            _ -> Left (ExprCanNotEval exp)

exec (Env vars procs chanels list) (VarDecl varType name) = case Map.lookup name vars of
    Just _ -> Left (VarAlreadyExist name)
    Nothing -> case varType of
        TInt -> Right (Env (Map.insert name (varType, IntVal 0) vars)  procs chanels list, True)
        TBool -> Right (Env (Map.insert name (varType, BoolVal False) vars)  procs chanels list, True)
        TBit -> Right (Env (Map.insert name (varType, BitVal False) vars)  procs chanels list, True)
        TByte -> Right (Env (Map.insert name (varType, ByteVal 0) vars)  procs chanels list, True)
        TShort -> Right (Env (Map.insert name (varType, ShortVal 0) vars)  procs chanels list, True)
        Tmtype -> Right (Env (Map.insert name (varType, MtypeVal "") vars)  procs chanels list, True)

exec env@(Env vars procs chanels list) (ChangeVar name exp) = do
    var <- eval env exp
    case (var, Map.lookup name vars) of
        (_, Nothing) -> Left (VarNotExist name)
        ((_, Const (IntVal a)), Just (TInt, _)) -> 
            return (Env (Map.insert name (TInt, IntVal a) vars)  procs chanels list, True)
        ((_, Const (BoolVal a)), Just (TBool, _)) ->
            return (Env (Map.insert name (TBool, BoolVal a) vars)  procs chanels list, True)
        _ -> Left (TypeMismatch ("type mismatch with varuable" ++ name))

exec (Env vars procs chanels list) (ArrayDec varType name size) = case Map.lookup name vars of
    Just _ -> Left (VarAlreadyExist name)
    Nothing -> Right (Env (Map.insert name (varType, Array size) vars)  procs chanels list, True)

exec env@(Env vars procs chanels list) (ArrayChange name index exp) = case Map.lookup name vars of
    Nothing -> Left (VarNotExist name)
    Just (varType, Array size) -> if index >= size then
        Left (IndexMoreThanSize name)
        else
            case exec env (Assig varType (name ++ "[" ++ show index ++ "]") exp) of 
                Left (VarAlreadyExist _) -> exec env (ChangeVar (name ++ "[" ++ show index ++ "]") exp)
                Right (newEnv, _) -> Right (newEnv, True)
                Left error -> Left error
    Just _ -> Left (VariableNotInScope name)

exec (Env vars procs chanels list) (Proctype name activeNum args procBody) = case Map.lookup name procs of
    Just _ -> Left (ProctypeAlreadyExist name)
    Nothing -> Right (Env vars (Map.insert name (args, procBody) procs) chanels (helper list activeNum [((args, procBody), 0)]), True)
        where 
            helper list1 num list2 
                | (num == 0) = list1
                | otherwise = helper (list1 ++ list2) (num - 1) list2

exec (Env vars procs chanels list) (Init initBody) = case Map.lookup "init" procs of
    Just _ -> Left (ProctypeAlreadyExist "init")
    Nothing -> Right (Env vars (Map.insert "init" ([], initBody) procs) chanels (list ++ [(([], initBody), 0)]), True)

exec (Env vars procs chanels list) (Chan name size types) = case Map.lookup name chanels of
    Just _ -> Left (VarAlreadyExist name)
    Nothing -> Right (Env vars procs (Map.insert name (types, [], size) chanels) list, True)

exec env@(Env vars procs chanels list) (ConditionStmt (exp, stmt)) = do
    var <- eval env exp
    case var of
        (_, Const (BoolVal True)) -> exec env stmt
        (_, Const (BoolVal False)) -> return (env, False)
        _ -> Left (TypeMismatch "expected bool expr")

exec env@(Env vars procs chanels list) (If ( (exp, stmt) : ifList)) = do 
    result <- exec env (ConditionStmt (exp, stmt))
    case result of
        (newEnv, True) -> return (newEnv, True)
        (_, False) -> if null ifList then
            return (env, False)
            else exec env (If ifList)

exec env@(Env vars procs chanels list) (If []) = Left (ProgWillNotEnd)

exec env@(Env vars procs chanels list) (Loop loopList) = helper env loopList loopList
    where
        helper currentEnv initialList ((exp, stmt) : currentList) = case eval currentEnv exp of
            Left error -> Left error
            Right (_, Const (BoolVal False)) -> if null currentList then
                Right (env, False)
                else helper currentEnv initialList currentList
            Right (_, Const (BoolVal True)) -> if stmt == Break then
                Right (currentEnv, True)
                else
                    case exec currentEnv stmt of
                    Left error -> Left error
                    Right (_, False) -> if null currentList then
                        Right (currentEnv, False)
                        else helper currentEnv initialList currentList
                    Right(newEnv, True) -> helper newEnv initialList initialList
            Right _ -> Left (TypeMismatch "expected bool expr")
        helper _ _ [] = Left ProgWillNotEnd

exec env@(Env vars procs chanels list) (Send name exps) = case Map.lookup name chanels of
    Nothing -> Left (VarNotExist name)
    Just (types, expList, size) -> if length expList == size then
        Right (env, False)
        else Right (Env vars procs (Map.insert name (types, exps : expList, size) chanels) list, True)

exec env@(Env vars procs chanels list) (Receive name varNames) = case Map.lookup name chanels of
    Nothing -> Left (VarNotExist name)
    Just (types, [], size) -> Right (env, False)
    Just (types, expList, size) -> case length varNames == length (head expList) of
        False -> Left (NeedMoreArgs name)
        True -> helper (Env vars procs (Map.insert name (types, (tail expList), size ) chanels) list) (head expList) varNames (length varNames)
            where
                helper currentEnv exprList names num
                    | (num == 0) = Right (currentEnv, True)
                    | otherwise = case exec currentEnv (ChangeVar (head names) (head exprList)) of
                        Left error -> Left error
                        Right (newEnv, _) -> helper newEnv (tail exprList) (tail names) (num - 1)

exec env@(Env vars procs chanels list) (Atomic stmtList) = helper env env stmtList (length stmtList)
    where
        helper initialEnv newEnv stmtList num
            | (num == 0) = Right (newEnv, True)
            | otherwise = case exec newEnv (head stmtList) of
                Left error -> Left error
                Right(_, False) -> Right(initialEnv, False)
                Right(nextEnv, True) -> helper initialEnv nextEnv (tail stmtList) (num - 1)

exec env (MtypeDecl list) = helper env list
    where
        helper env [] = Right (env, True)
        helper env (a:list) = do
            result <- exec env (VarDecl Tmtype a)
            case result of
                (newEnv, _) -> helper newEnv list

exec (Env vars procs chanels list) (RunProc name exprs) = case Map.lookup name procs of 
    Nothing -> Left (ProcNotExist name)
    Just (pairList, stmtList) -> Right (Env vars procs chanels (list ++ [((pairList, stmtList), 0)]), True)

exec (Env vars procs chanels ((proc, num) : list)) (Goto name) = 
    Right (Env vars procs chanels ((proc, (findLoop proc name 0 num)) : list), True)
        where
            findLoop (fst, stmt : stmtList) name num initial = case stmt == GotoLoop name of
                True -> num
                False -> findLoop (fst, stmtList) name (num+1) initial
            findLoop (_, []) _ _ initial = initial

exec (Env _ _ _ []) (Goto name) = Left (GotoNotInProc)
    
exec env (GotoLoop name) = Right (env, True)

exec env (Break) = Left (BreakNotInLoop)

exec env (Skip) = Right (env, True)

eval :: Env -> Exp -> ErrExp

eval env (Const a) = Right (env, Const a) 

eval env@(Env vars _ _ _) (Variable s) = case Map.lookup s vars of
    Just a -> Right ( env, Const (snd a) )
    Nothing -> Left (VariableNotInScope s)

eval env@(Env vars _ _ _) (ArrayVal name index) = case Map.lookup (name ++ "[" ++ show index ++ "]") vars of
    Just a -> Right ( env, Const (snd a) )
    Nothing -> Left (VariableNotInScope name)

eval env (BinExp Add l r) = do
    int1 <- eval env l
    int2 <- eval env r
    case (int1,int2) of
        ((_, Const (IntVal left)), (_, Const (IntVal right))) -> return (env, Const (IntVal (left + right)))
        _ -> Left (WrongOperation "add not to ints")
    
eval env (BinExp Sub l r) = do
    int1 <- eval env l
    int2 <- eval env r
    case (int1,int2) of
        ((_, Const (IntVal left)), (_, Const (IntVal right))) -> return (env, Const (IntVal (left - right)))
        _ -> Left (WrongOperation "sub not to ints")

eval env (BinExp Mul l r) = do
    int1 <- eval env l
    int2 <- eval env r
    case (int1,int2) of
        ((_, Const (IntVal left)), (_, Const (IntVal right))) -> return (env, Const (IntVal (left * right)))
        _ -> Left (WrongOperation "mul not to ints")

eval env (BinExp Div l r) = do
    int1 <- eval env l
    int2 <- eval env r
    case (int1,int2) of
        ((_, Const (IntVal left)), (_, Const (IntVal right))) -> return (env, Const (IntVal (left `div` right)))
        _ -> Left (WrongOperation "div not to ints")

eval env (BinExp Greater l r) = do
    int1 <- eval env l
    int2 <- eval env r
    case (int1,int2) of
        ((_, Const (IntVal left)), (_, Const (IntVal right))) -> return (env, Const (BoolVal (left > right)))
        _ -> Left (WrongOperation "> not to ints")

eval env (BinExp Less l r) = do
    int1 <- eval env l
    int2 <- eval env r
    case (int1,int2) of
        ((_, Const (IntVal left)), (_, Const (IntVal right))) -> return (env, Const (BoolVal (left < right)))
        _ -> Left (WrongOperation "< not to ints")

eval env (BinExp Equal l r) = do
    smth1 <- eval env l
    smth2 <- eval env r
    case (smth1, smth2) of 
        ((_, Const left), (_, Const right)) -> return (env, Const (BoolVal (left == right)))
        _ -> Left (WrongOperation "== not to ints or bools")

eval env (BinExp And l r) = do
    bool1 <- eval env l
    bool2 <- eval env r
    case (bool1, bool2) of 
        ((_, Const (BoolVal left)), (_, Const (BoolVal right))) -> return (env, Const (BoolVal (left && right)))
        _ -> Left (WrongOperation "and not to bools")

eval env (BinExp Or l r) = do
    bool1 <- eval env l
    bool2 <- eval env r
    case (bool1, bool2) of 
        ((_, Const (BoolVal left)), (_, Const (BoolVal right))) -> return (env, Const (BoolVal (left || right)))
        _ -> Left (WrongOperation "or not to bools")

eval env (UnExp Neg a) = do
    int1 <- eval env a
    case int1 of
        (_, Const (IntVal b)) -> return (env, Const (IntVal (-b)))
        _ -> Left (WrongOperation "- not to int")

eval env (UnExp Not a) = do
    bool1 <- eval env a
    case bool1 of
        (_, Const (BoolVal b)) -> return (env, Const (BoolVal (not b)))
        _ -> Left (WrongOperation "not operation not to bool")