module Main where
import Types
import Parser
import Interpreter
import Text.Megaparsec
import Text.Pretty.Simple
import qualified Data.Map.Strict as Map

main :: IO ()
main = do
    parser <- runParser programParser "app/prog" <$> readFile "app/prog"
    results parser where
    results (Left error) = putStrLn $ show error
    results (Right parser) = do
        pPrint parser
        manager parser createEnv