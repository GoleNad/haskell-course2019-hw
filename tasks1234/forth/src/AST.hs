{-# OPTIONS_GHC -Wall #-}

module AST where

import qualified Data.Map as M
import Data.Function

type Stack = [Integer]
type Forth = [Expr]
type Dict = M.Map String Forth

data VM = VM {
  output :: String,
  stack :: Stack,
  rstack :: Stack,
  dict :: M.Map String Forth
} deriving Show

data FError = 
        StackUnderflow
        | LookupError String
        | UndefinedPrimError String
        deriving Show

data Expr = 
        Push Integer
      | Output
      | Word String
      | Cond [Expr] [Expr]
      | Loop [Expr]
      | Prim String
      | Def String [Expr]
      deriving Show

liftOp :: (Integer -> Integer -> Integer) -> VM -> Maybe VM
liftOp f vm = case vm & stack of 
  (x:y:xs) -> Just $ vm {stack = (f x y) : xs}
  _ -> Nothing

liftBoolOp :: (Integer -> Integer -> Bool) -> VM -> Maybe VM
liftBoolOp f vm = case vm & stack of 
  (x:y:xs) -> Just $ vm {stack = if f y x then -1 : xs else 0 : xs}
  _ -> Nothing

binOpDict :: M.Map Char (Stack -> Maybe Stack)
binOpDict = M.fromList binOps

binOps :: [(Char, Stack -> Maybe Stack)] 
binOps = []

primDict :: M.Map String (VM -> Maybe VM)
primDict = M.fromList primOps

-- semantics description can be found in https://forth-standard.org/standard/core
primOps :: [(String, VM -> Maybe VM)]
primOps = [
            ("+", liftOp (+)),

            ("-", liftOp (-)),

            ("*", liftOp (*)),

            ("<", liftBoolOp (<)),

            (">", liftBoolOp (>)),

            ("=", liftBoolOp (==)),

            ("dup", 
              \vm -> case vm & stack of 
                (x:xs) -> return $ vm {stack = (x:x:xs)}
                _ -> Nothing
            ),
            ("drop", \vm -> case vm & stack of 
              (_:xs) -> return $ vm {stack = xs}
              _ -> Nothing
            ),
            ("swap", \vm -> case vm & stack of 
              (x:y:xs) -> return $ vm {stack = (y:x:xs)}
              _ -> Nothing
            ),
            ("over", \vm -> case vm & stack of 
              (x:y:xs) -> return $ vm {stack = (y:x:y:xs)}
              _ -> Nothing
            ),
            ("rot", \vm -> case vm & stack of 
              (z:y:x:xs) -> return $ vm {stack = (x:z:y:xs)}
              _ -> Nothing
            ),
            (">r", \vm -> case vm & stack of 
              (x:xs) -> return $ vm {
                  rstack = x:(vm & rstack),
                  stack = xs
              }
              _ -> Nothing
            ),
            ("r>", \vm -> case vm & rstack of 
              (x:xs) -> return $ vm {
                  stack = x:(vm & stack),
                  rstack = xs
              }
              _ -> Nothing
            ),
            ("2rot", \vm -> case vm & stack of 
              (z:z':y:y':x:x':xs) -> return $ vm {stack = (x:x':z:z':y:y':xs)}
              _ -> Nothing
            ),
            ("2dup", 
              \vm -> case vm & stack of 
                (x:x':xs) -> return $ vm {stack = (x:x':x:x':xs)}
                _ -> Nothing
            ),
            ("2drop", \vm -> case vm & stack of 
              (_:_:xs) -> return $ vm {stack = xs}
              _ -> Nothing
            ),
            ("2swap", \vm -> case vm & stack of 
              (x:x':y:y':xs) -> return $ vm {stack = (y:y':x:x':xs)}
              _ -> Nothing
            ),
            ("2over", \vm -> case vm & stack of 
              (x:x':y:y':xs) -> return $ vm {stack = (y:y':x:x':y:y':xs)}
              _ -> Nothing
            ),
            ("nip", \vm -> case vm & stack of 
              (x:_:xs) -> return $ vm {stack = (x:xs)}
              _ -> Nothing
            ),
            ("tuck", \vm -> case vm & stack of 
              (x:y:xs) -> return $ vm {stack = (x:y:x:xs)}
              _ -> Nothing
            ),
            ("r@", \vm -> case vm & rstack of 
              (x:_) -> return $ vm {stack = x:(vm & rstack)}
              _ -> Nothing
            ),
            ("2>r", \vm -> case vm & stack of 
              (x:x':xs) -> return $ vm {
                  rstack = x:x':(vm & rstack),
                  stack = xs
              }
              _ -> Nothing
            ),
            ("2r>", \vm -> case vm & rstack of 
              (x:x':xs) -> return $ vm {
                  stack = x:x':(vm & stack),
                  rstack = xs
              }
              _ -> Nothing
            ),
            ("2r@", \vm -> case vm & rstack of 
              (x:x':_) -> return $ vm {stack = x:x':(vm & rstack)}
              _ -> Nothing
            )]

controls :: [String]
controls = ["do", "loop", "if", "else", "then"]
