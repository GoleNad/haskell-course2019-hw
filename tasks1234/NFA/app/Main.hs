module Main where

import Lib
import AST
import MyParser
import Interpreter
import Constr
import Data.List (head, tail)

snd3 :: (a,b,c) -> b
snd3 (a,b,c) = b

getInitial :: ((([State],[State],[State]),[Transition]),(Int,Bool)) -> State
getInitial a = head(snd3(fst(fst a)))

main :: IO()
main = do
    putStr "Write name of file with automaton (without .txt)\n"
    in1 <- getLine
    putStr "Write name of file with string (without .txt)\n"
    in2 <- getLine
    contents <- readFile (in1 ++ ".txt")
    let a = snd(head( run mainparser contents))
    let c = Automaton (fst a)
    print c
    putStr "Do you want to transform your automaton to determinate? (Y/N)\n(It only works with common automatons)\n"
    ans <- getLine
    let d = if (ans == "Y")
            then if ((snd a) == (-1,False)) then construct c else (Automaton (([],[],[]),[]))
            else c
    print d
    inputstring <- readFile (in2 ++ ".txt")
    let input = map transform inputstring
    let b = if not(check d)
        then 
            if (dcheck d) then "Automaton contains register or stack" else "Icorrect Automaton"
        else 
            case (snd a) of (-1,False) -> "Answer: " ++ (show $ interpret d input [getInitial a])
                            (-1,True) -> "Answer: " ++ (show $ interpretS d input [(getInitial a,(Stack []))])
                            (x,False) -> "Answer: " ++ (show $ interpretR d input [(getInitial a,0)] x)
                            (x,True) -> "Answer: " ++ (show $ interpretRS d input [(getInitial a,(0,(Stack [])))] x)
    print b