module Constr where

import AST
import Data.List (head, tail)

suc :: State -> State
suc (State a) = State (a+1)

fst5 :: (a,b,c,d,e) -> a
fst5 (a,b,c,d,e) = a

snd5 :: (a,b,c,d,e) -> b
snd5 (a,b,c,d,e) = b

thr5 :: (a,b,c,d,e) -> c
thr5 (a,b,c,d,e) = c

frt5 :: (a,b,c,d,e) -> d
frt5 (a,b,c,d,e) = d

fft5 :: (a,b,c,d,e) -> e
fft5 (a,b,c,d,e) = e

fromState :: State -> Int
fromState (State a) = a


alphabet :: [Transition] -> [AType]
alphabet [] = []
alphabet ((Transition _ _ c):xs) = d
    where   y = alphabet xs
            d | elem c y = y
              | otherwise = c : y
              
start :: Automaton -> [State]
start (Automaton ((a,b,c),t)) = b

cfindl :: [State] -> [(State,[State])] -> State
cfindl a [] = (State 0)
cfindl a (e:ex) = l
    where l | a == (snd e) = fst e
            | otherwise = cfindl a ex
            
cfindr :: State -> [(State,[State])] -> [State]
cfindr a [] = [(State (-1))]
cfindr a (e:ex) = l
    where l | a == (fst e) = snd e
            | otherwise = cfindr a ex
            
unz :: [([State],Bool)] -> ([State],Bool)
unz a = (concat (fst $ unzip a), or (snd $ unzip a))


citeration :: [State] -> AType -> [Transition] -> State -> ([State],Bool)
citeration c a [] d = ([],False)
citeration c a ((Transition l m n):ts) d = y
      where qq = (citeration c a ts d)
            y | (l == (fromState d)) && (n == a) && (elem (State m) c) = ((State m):(fst qq),True)
              | (l == (fromState d)) && (n == a) = ((State m):(fst qq),(snd qq))
              | otherwise = qq


citerate :: [(State,[State])] -> State -> [State] -> State -> [AType] -> [Transition] -> ([(State,[State])], [State], [State], State, [Transition])
citerate ex d c z [] t = (ex,[],[],z,[])
citerate ex d c z (a:alph) t = g
    where j = unz (map (citeration c a t) (cfindr d ex))
          qq = (citerate ex d c z alph t)
          qx = (citerate ((z,(fst j)):ex) d c (suc z) alph t)
          g | (not ((fst j) == [])) && (elem (fst j) (snd (unzip ex))) = (fst5 qq,snd5 qq,thr5 qq,frt5 qx, (Transition (fromState d) (fromState (cfindl (fst j) ex)) a):(fft5 qx))
            | (not ((fst j) == [])) && (snd j) = (fst5 qx,z:(snd5 qx),z:(thr5 qx),frt5 qx, (Transition (fromState d) (fromState z) a):(fft5 qx))
            | (not ((fst j) == [])) = (fst5 qx,z:(snd5 qx),thr5 qx,frt5 qx, (Transition (fromState d) (fromState z) a):(fft5 qx))
            | otherwise = qq
    
bunch :: Automaton -> [(State,[State])] -> [State] -> [State] -> State -> [Transition] -> [AType] ->  Automaton
bunch (Automaton ((a,b,c),t)) ex [] end z t' alph = Automaton (([z],[head b],end),t')
bunch (Automaton ((a,b,c),t)) ex (d:ds) end z t' alph = bunch (Automaton ((a,b,c),t)) ex' dds end' z' t'' alph
          where l = (citerate ex d c z alph t)
                ex' = fst5 l
                dds = ds ++ (snd5 l)
                end' = end ++ (thr5 l)
                z' = frt5 l
                t'' = t' ++ (fft5 l)
                
            
construct :: Automaton -> Automaton
construct (Automaton ((a,b,c),t)) = bunch (Automaton ((a,b,c),t)) [(head b,b)] [head b] [] x [] (alphabet t)
    where x = State ((fromState (head b)) + 1)