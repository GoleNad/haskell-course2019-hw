module Interpreter where

import AST
import Data.Char (isDigit, digitToInt)
import Data.List (head, tail)

transform :: Char -> AType
transform s = c
    where
        c | isDigit s = IType (digitToInt s)
          | otherwise = CType s

check :: Automaton -> Bool
check (Automaton ((r,l,a),t)) = b
    where
        b | (null r) || (null l) || (null a) || (null t) = False
          | otherwise = True
          
dcheck :: Automaton -> Bool
dcheck (Automaton (l,t)) = b
    where
        b | (null t) = True
          | otherwise = False

    
interpret :: Automaton -> [AType] -> [State] -> Bool
interpret a b s = or (map (nextiteration a b) s)
        
nextiteration :: Automaton -> [AType] -> State -> Bool
nextiteration (Automaton ((r,l,a),t)) c (State s) = b
    where
        b | s == -1 = False
          | (c == []) && (elem (State s) a) = True
          | c == [] = False
          | otherwise = epsiteration (Automaton ((r,l,a),t)) c (State s)
          
epsiteration :: Automaton -> [AType] -> State -> Bool
epsiteration (Automaton (u, t1)) cs s = b
    where
        t2 = map (epstrans s) t1
        b | length (filter (== State (-1)) t2) == length t2 = iteration (Automaton (u, t1)) cs s 
          | otherwise = interpret (Automaton (u, t1)) cs t2
        
epstrans :: State -> Transition -> State
epstrans (State a) (Transition x y z) = State s
    where
        s | (a == x) && (CType '#' == z) = y
          | otherwise = -1
        
iteration :: Automaton -> [AType] -> State -> Bool
iteration (Automaton (u, t1)) (c:cs) s = interpret (Automaton (u, t1)) cs t2
    where
        t2 = map (trans c s) t1

trans :: AType -> State -> Transition -> State
trans c (State a) (Transition x y z) = State s
    where
        s | (a == x) && (c == z) = y
          | otherwise = -1
          
          
interpretR :: Automaton -> [AType] -> [(State,Int)] -> Int -> Bool
interpretR a b s x = or (map (nextiterationR a b x) s)
        
nextiterationR :: Automaton -> [AType] -> Int -> (State,Int) -> Bool
nextiterationR (Automaton ((r,l,a),t)) c x2 ((State s),x1) = b
    where
        b | s == -1 = False
          | (c == []) && (elem (State s) a) && (x1==x2) = True
          | c == [] = False
          | otherwise = epsiterationR (Automaton ((r,l,a),t)) c (State s,x1) x2
          
epsiterationR :: Automaton -> [AType] -> (State, Int) -> Int -> Bool
epsiterationR (Automaton (u, t1)) cs (s,x1) x2 = b
    where
        t2 = map (epstransR s x1) t1
        b | length (filter (== (State (-1),x1)) t2) == length t2 = iterationR (Automaton (u, t1)) cs (s,x1) x2 
          | otherwise = interpretR (Automaton (u, t1)) cs t2 x2
        
epstransR :: State -> Int -> Transition -> (State,Int)
epstransR (State a) x1 (Transition x y z) = s
    where
        s | (a == x) && (CType '#' == z) = (State y,x1)
          | otherwise = (State (-1),x1)
epstransR (State a) x1 (TransitionR x y z b) = s
    where
        s | (a == x) && (CType '#' == z) && (b == False) = (State y,x1-1)
          | (a == x) && (CType '#' == z) && (b == True) = (State y,x1+1)
          | otherwise = (State (-1),x1)
        
iterationR :: Automaton -> [AType] -> (State,Int) -> Int -> Bool
iterationR (Automaton (u, t1)) (c:cs) (s,x1) x2 = interpretR (Automaton (u, t1)) cs t2 x2
    where
        t2 = map (transR c s x1) t1

transR :: AType -> State -> Int -> Transition -> (State,Int)
transR c (State a) x1 (Transition x y z) = s
    where
        s | (a == x) && (c == z) = (State y,x1)
          | otherwise = (State (-1),x1)
transR c (State a) x1 (TransitionR x y z b) = s
    where
        s | (a == x) && (c == z) && (b == False) = (State y,x1-1)
          | (a == x) && (c == z) && (b == True) = (State y,x1+1)
          | otherwise = (State (-1),x1)
          
          
interpretS :: Automaton -> [AType] -> [(State,Stack)] -> Bool
interpretS a b s = or (map (nextiterationS a b) s)
        
nextiterationS :: Automaton -> [AType] -> (State,Stack) -> Bool
nextiterationS (Automaton ((r,l,a),t)) c ((State s),(Stack f)) = b
    where
        b | s == -1 = False
          | (c == []) && (elem (State s) a) && (f == []) = True
          | c == [] = False
          | otherwise = epsiterationS (Automaton ((r,l,a),t)) c (State s,(Stack f))
          
epsiterationS :: Automaton -> [AType] -> (State,Stack) -> Bool
epsiterationS (Automaton (u, t1)) cs (s,f) = b
    where
        t2 = map (epstransS s f) t1
        b | length (filter (== (State (-1),f)) t2) == length t2 = iterationS (Automaton (u, t1)) cs (s,f)
          | otherwise = interpretS (Automaton (u, t1)) cs t2
        
epstransS :: State -> Stack -> Transition -> (State,Stack)
epstransS (State a) f (Transition x y z) = s
    where
        s | (a == x) && (CType '#' == z) = (State y,f)
          | otherwise = (State (-1),f)
epstransS (State a) (Stack f) (TransitionS x y z b) = s
    where
        s | (a == x) && (CType '#' == z) && (b == CType 'e') = (State y, Stack (tail f))
          | (a == x) && (CType '#' == z) = (State y, Stack (b:f))
          | otherwise = (State (-1),Stack f)
        
iterationS :: Automaton -> [AType] -> (State,Stack) -> Bool
iterationS (Automaton (u, t1)) (c:cs) (s,f) = interpretS (Automaton (u, t1)) cs t2
    where
        t2 = map (transS c s f) t1

transS :: AType -> State -> Stack -> Transition -> (State,Stack)
transS c (State a) f (Transition x y z) = s
    where
        s | (a == x) && (c == z) = (State y,f)
          | otherwise = (State (-1),f)
transS c (State a) (Stack f) (TransitionS x y z b) = s
    where
        s | (a == x) && (c == z) && (b == CType 'e') = (State y, Stack (tail f))
          | (a == x) && (c == z) = (State y, Stack (b:f))
          | otherwise = (State (-1),Stack f)
          
          
interpretRS :: Automaton -> [AType] -> [(State,(Int, Stack))] -> Int -> Bool
interpretRS a b s x = or (map (nextiterationRS a b x) s)
        
nextiterationRS :: Automaton -> [AType] -> Int -> (State,(Int, Stack)) -> Bool
nextiterationRS (Automaton ((r,l,a),t)) c x2 ((State s),(x1, (Stack f))) = b
    where
        b | s == -1 = False
          | (c == []) && (elem (State s) a) && (f == []) && (x1==x2) = True
          | c == [] = False
          | otherwise = epsiterationRS (Automaton ((r,l,a),t)) c (State s,(x1, (Stack f))) x2
          
epsiterationRS :: Automaton -> [AType] -> (State,(Int, Stack)) -> Int -> Bool
epsiterationRS (Automaton (u, t1)) cs (s,f) x2 = b
    where
        t2 = map (epstransRS s f x2) t1
        b | length (filter (== (State (-1),f)) t2) == length t2 = iterationRS (Automaton (u, t1)) cs (s,f) x2
          | otherwise = interpretRS (Automaton (u, t1)) cs t2 x2
        
epstransRS :: State -> (Int, Stack) -> Int -> Transition -> (State,(Int, Stack))
epstransRS (State a) (x1,f) x2 (Transition x y z) = s
    where
        s | (a == x) && (CType '#' == z) = (State y,(x1,f))
          | otherwise = (State (-1),(x1,f))
epstransRS (State a) (x1,f) x2 (TransitionR x y z b) = s
    where
        s | (a == x) && (CType '#' == z) && (b == False) = (State y,(x1-1,f))
          | (a == x) && (CType '#' == z) && (b == True) = (State y,(x1+1,f))
          | otherwise = (State (-1),(x1,f))
epstransRS (State a) (x1,(Stack f)) x2 (TransitionS x y z b) = s
    where
        s | (a == x) && (CType '#' == z) && (b == CType 'e') = (State y, (x1,Stack (tail f)))
          | (a == x) && (CType '#' == z) = (State y, (x1,Stack (b:f)))
          | otherwise = (State (-1),(x1,Stack f))
epstransRS (State a) (x1,(Stack f)) x2 (TransitionRS x y z d b) = s
    where
        s | (a == x) && (CType '#' == z) && (b == CType 'e') && (d == True) = (State y, (x1+1,Stack (tail f)))
          | (a == x) && (CType '#' == z) && (b == CType 'e') && (d == False) = (State y, (x1-1,Stack (tail f)))
          | (a == x) && (CType '#' == z) && (d == True) = (State y, (x1+1,Stack (b:f)))
          | (a == x) && (CType '#' == z) && (d == False) = (State y, (x1-1,Stack (b:f)))
          | otherwise = (State (-1),(x1,Stack f))
        
iterationRS :: Automaton -> [AType] -> (State,(Int, Stack)) -> Int -> Bool
iterationRS (Automaton (u, t1)) (c:cs) (s,f) x2 = interpretRS (Automaton (u, t1)) cs t2 x2
    where
        t2 = map (transRS c s f x2) t1

transRS :: AType -> State -> (Int, Stack) -> Int -> Transition -> (State,(Int, Stack))
transRS c (State a) (x1,f) x2 (Transition x y z) = s
    where
        s | (a == x) && (c == z) = (State y,(x1,f))
          | otherwise = (State (-1),(x1,f))
transRS c (State a) (x1,f) x2 (TransitionR x y z b) = s
    where
        s | (a == x) && (c == z) && (b == False) = (State y,(x1-1,f))
          | (a == x) && (c == z) && (b == True) = (State y,(x1+1,f))
          | otherwise = (State (-1),(x1,f))
transRS c (State a) (x1,(Stack f)) x2 (TransitionS x y z b) = s
    where
        s | (a == x) && (c == z) && (b == CType 'e') = (State y, (x1,Stack (tail f)))
          | (a == x) && (c == z) = (State y, (x1,Stack (b:f)))
          | otherwise = (State (-1),(x1,Stack f))
transRS c (State a) (x1,(Stack f)) x2 (TransitionRS x y z d b) = s
    where
        s | (a == x) && (c == z) && (b == CType 'e') && (d == True) = (State y, (x1+1,Stack (tail f)))
          | (a == x) && (c == z) && (b == CType 'e') && (d == False) = (State y, (x1-1,Stack (tail f)))
          | (a == x) && (c == z) && (d == True) = (State y, (x1+1,Stack (b:f)))
          | (a == x) && (c == z) && (d == False) = (State y, (x1-1,Stack (b:f)))
          | otherwise = (State (-1),(x1,Stack f))