module AST where

data AType = IType Int | CType Char deriving (Show, Eq)

data NumberOfState = NumberOfState Int deriving (Show, Eq)

data State = State Int deriving (Show, Eq)

data Transition = Transition Int Int AType | TransitionR Int Int AType Bool | TransitionS Int Int AType AType | TransitionRS Int Int AType Bool AType deriving Show
--R means Register, S means Stack, RS means Register with Stack

data FinalStates = FinalStates [State] deriving Show

data InitialStates = InitialStates [State] deriving Show

data Register = Register Int deriving Show

data Stack = Stack [AType] deriving (Show, Eq)

data Automaton = Automaton (([State],[State],[State]),[Transition])

tshow :: [Transition] -> String
tshow [] = "\n"
tshow (t:ts) = "\n      " ++ show t ++ tshow ts  

instance Show Automaton where
    show (Automaton ((a,b,c),t)) = "Automaton:\n    StartValues: " ++ show b ++ "\n    EndValues: " ++ show c ++ "\n    Transitions: " ++ tshow t ++ "\n"

{-
Examle of automaton
1;
2;
3;
1 -> 3 [label='a ++'];
1 -> 2 [label='b e'];
2 -> 3 [label='c -- c'];
//1;
//3 1 -;
Accept strings: "a" and "bc"
-}

{-
Автомат, допускающий скобочные последовательности
2;
1 -> 2 [label='( ++'];
2 -> 2 [label='( ++'];
2 -> 1 [label=') --'];
1 -> 1 [label=') --'];
//1;
//1 0;
-}