module MyParser where

import AST
import Control.Applicative
import Data.List (isPrefixOf)
import Data.Char (isDigit, digitToInt)
import Data.List (head)

newtype Parser a = Parser {doParse :: String -> [(String, a)]}

instance Functor Parser where
  fmap f (Parser p1) = Parser p2
    where
      p2 s = convert (p1 s)
      convert results = fmap (\(s, val) -> (s, f val)) results

instance Applicative Parser where
  pure a = Parser (\s -> [(s, a)])
  pf <*> px = Parser (\s -> [ (sx, f x) | (sf, f) <- doParse pf $ s,
                                          (sx, x) <- doParse px $ sf])

instance Alternative Parser where
  empty = Parser (const [])
  px <|> py = Parser (\s -> doParse px s ++ doParse py s)
  
instance Monad Parser where
  return a = Parser (\s -> [(s, a)])
  Parser p >>= p' = Parser (\inp -> 
        flip concatMap (p inp) (\(tl, x) ->
        run (p' x) tl))

run (Parser f) = f

eqChar :: Char -> Parser Char
eqChar a = Parser f
  where
    f "" = []
    f (c : cs) | a == c = [(cs, c)]
               | otherwise = []

prefixP :: String -> Parser String
prefixP s = Parser f
  where
    f input = if s `isPrefixOf` input
                then [(drop (length s) input, s)]
                else []

digit :: Parser Char
digit = Parser f
  where
    f "" = []
    f (c : cs) | isDigit c = [(cs, c)]
               | otherwise = []
               
pm :: Parser Bool
pm = Parser f
  where
    f "" = []
    f (c : cs) | c == '+' = [(cs, True)]
               | c == '-' = [(cs, False)]
               | otherwise = []

digits :: Parser String
digits = (:) <$> digit <*> digits <|> pure ""

anyChar :: Parser Char
anyChar = Parser f
  where
    f "" = []
    f (c : cs) = [(cs, c)]

rInt :: String -> Int
rInt = read

number = do
    d <- rInt <$> digits
    r1 <- eqChar ';'
    r2 <- eqChar '\n'
    return (State d)
    
numbers = do
    n <- (:) <$> number <*> numbers <|> pure []
    return n;
    
cnumber = do
    r1 <- eqChar '/'
    r2 <- eqChar '/'
    d <- rInt <$> digits
    r3 <- eqChar ';'
    r4 <- eqChar '\n'
    return [State d]
    
cnumb = do
    r1 <- eqChar '/'
    r2 <- eqChar '/'
    d <- rInt <$> digits
    return [State d]

ender = do
    t <- end <|> endR <|> endS <|> endRS
    return t
    
end = do
    r1 <- eqChar ';'
    return (-1,False)
    
endR = do
    r1 <- eqChar ' '
    r <- rInt <$> digits
    r1 <- eqChar ';'
    return (r,False)
    
endS = do
    r1 <- eqChar ' '
    r <- eqChar '-'
    r1 <- eqChar ';'
    return (-1,True)
    
endRS = do
    r1 <- eqChar ' '
    r <- rInt <$> digits
    r1 <- eqChar ' '
    l <- eqChar '-'
    r1 <- eqChar ';'
    return (r,True)

lastLabel = do
    l <- anyChar
    r0 <- eqChar '\''
    r1 <- eqChar ']'
    r1 <- eqChar ';'
    r2 <- eqChar '\n'
    return (if isDigit l then IType (digitToInt l) else CType l)
    
label = do
    l <- anyChar
    r1 <- eqChar ' '
    return (if isDigit l then IType (digitToInt l) else CType l)

transition = do
    d1 <- rInt <$> digits
    r1 <- prefixP " -> "
    d2 <- rInt <$> digits
    r2 <- prefixP " [label='"
    l <- lastLabel
    return (Transition d1 d2 l)
    
transitionR = do
    d1 <- rInt <$> digits
    r1 <- prefixP " -> "
    d2 <- rInt <$> digits
    r2 <- prefixP " [label='"
    l <- label
    b <- pm
    b1 <- pm
    r0 <- eqChar '\''
    r1 <- eqChar ']'
    r1 <- eqChar ';'
    r2 <- eqChar '\n'
    return (TransitionR d1 d2 l b)
    
transitionS = do
    d1 <- rInt <$> digits
    r1 <- prefixP " -> "
    d2 <- rInt <$> digits
    r2 <- prefixP " [label='"
    l <- label
    c <- lastLabel
    return (TransitionS d1 d2 l c)
    
transitionRS = do
    d1 <- rInt <$> digits
    r1 <- prefixP " -> "
    d2 <- rInt <$> digits
    r2 <- prefixP " [label='"
    l <- label
    b <- pm
    b1 <- pm
    r1 <- eqChar ' '
    c <- lastLabel
    return (TransitionRS d1 d2 l b c)
    
dtransition = do
    t <- transition <|> transitionR <|> transitionS <|> transitionRS
    return t

transitions = do
    t <- (:) <$> dtransition <*> transitions <|> pure []
    return t

mainparser = do
    nos <- numbers
    t <- transitions
    is <- cnumber
    fs <- cnumb
    e <- ender
    return (((nos, is, fs),t),e)