AST corrected.
Comleted Parser and Interpreter for automatons with one stack and register for dot language.
Type of automaton depend on last string:
//2; - Common
//2 0; - Register, where last number is eligible value
//2 -; - Stack
//2 0 -; - Register & Stack

To remove smth from Stack use
label='b e'

To use epsilon
label='#'


Тесты:
t1&&i1: Тест детерминированного автомата
t2&&i2: Ошибка в преобразовании (некорректный автомат)
t3&&i3: Простенький тест преобразования
t4&&i4: Второй простенький тест преобразования
t5&&i5: Большой тест, принимающий последовательности (a^n,c^(1||2),b^k,a)