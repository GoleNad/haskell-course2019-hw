{-# LANGUAGE QuasiQuotes, OverloadedStrings #-}
{-# LANGUAGE TupleSections     #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE ExplicitForAll, ScopedTypeVariables, RankNTypes #-}

module Main where

import AST
import Parser
import Interpreter
import Text.Megaparsec
import Text.Pretty.Simple (pPrint)
import NeatInterpolation (text)
import qualified Data.Text.IO
import Data.Text (Text, unpack)

mulListTest :: Text
mulListTest = [text|
[1, 2] * 3
|]

ifTest :: Text
ifTest = [text|
module main
    if (3 < 4):
      a = 3 + 4
    else:
      a = 5      
|]

defFunTest :: Text
defFunTest = [text|
module main
    a = 3    
    def main(a, b):     
      a = 0     
      if (2 < 3):      
        a += 4
      else:      
        a = 5     
      return a    
    fun = main(1, 10)
    b = a
|]

lambdaTest :: Text
lambdaTest = [text|
module main
    def y(func):
      return func(lambda x: y(func)(x))
    fact = lambda f:lambda n: 1 if n==0  else  n*f(n-1)
//Y-combinator
    y(fact)(5)
|]

lambdaTest1 :: Text
lambdaTest1 = [text|
module main
    def z(func):  
      return func(lambda x: z(func)(x))
    def y(func):
      b = z(fact)(2)    
      return func(lambda x: y(func)(x))
    fact = lambda f:lambda n: 1 if n==0  else  n*f(n-1)
//Y-combinator
    y(fact)(5)
|]

assignAddTest :: Text
assignAddTest = [text|
module main
    a = 0
    a += 3
|]

forTest :: Text
forTest = [text|
module main    
    a = 0
    for i in [2, 3]:
      a = a + i
|]

whileTest :: Text
whileTest = [text|
module main    
    a = 0    
    while (a < 10):     
      a = a + 1
|]

classTest :: Text
classTest = [text|
module main
    class Sum:
      a = 3
      b = 4

      def sum(a, b):
        return a + b

    a = Sum()
    b = a.sum(2, 5)
    Sum.sum(2, 5)

    five = 5
    def sub(b):
      return five - b

/* Defining class level methods dynamically with closures */
    Sum.sub = sub
    a.sub(5)


|]

main :: IO ()
main = do
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser "'str' + 'ing'"
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser "[1, 2] * 3"
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser "3 * [1, 2]"
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser (unpack whileTest)
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser (unpack forTest)
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser (unpack assignAddTest)
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser (unpack defFunTest)
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser (unpack classTest)
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser (unpack lambdaTest)
  pPrint $ evalProg $ fromMaybe $ parseMaybe whileParser (unpack lambdaTest1)






