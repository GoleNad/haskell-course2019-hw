module HRuby where

import Data.Char
import Prelude

data Type = BOOL | INT | CHAR | FLOAT | ARRAY | OBJECT
    deriving (Show, Eq)

data RValue = VBool Bool
            | VInt Int
            | VChar Char
            | VFloat Float
            | VArray [RValue]
            | VObject String [Binding]
    deriving (Show)

getValueType::RValue -> Type 
getValueType (VBool _) = BOOL
getValueType (VInt _) = INT
getValueType (VChar _) = CHAR
getValueType (VFloat _) = FLOAT
getValueType (VArray _) = ARRAY
getValueType (VObject _ _) = OBJECT

getBoolValue::RValue -> Bool
getBoolValue (VBool x) = x
getBoolValue (VInt x) = x /= 0
getBoolValue (VChar x) = x /= '\0'
getBoolValue (VFloat x) = x /= 0
getBoolValue (VArray x) = not (null x)
getBoolValue (VObject s _) = not (null s) --is actually an object

-- data RCLass = RClass { name::String, fields::[String], methods::[(String, [String])] }

infixl 6 #+ 
(#+)::RValue -> RValue -> Maybe RValue
(#+) (VBool a) (VBool b) = Just $ VBool (a || b)
(#+) (VInt a) (VInt b) = Just $ VInt (a + b)
(#+) (VFloat a) (VFloat b) = Just $ VFloat (a + b)
(#+) (VArray a) (VArray b) = Just $ VArray (a ++ b)
(#+) _ _ = Nothing

infixl 5 #==
(#==)::RValue -> RValue -> Maybe RValue
(#==) (VBool a) (VBool b) = Just $ VBool (a == b)
(#==) (VInt a) (VInt b) = Just $ VBool (a == b)
(#==) (VFloat a) (VFloat b) = Just $ VBool (a == b)
(#==) _ _ = Nothing

infixl 5 #<
(#<)::RValue -> RValue -> Maybe RValue
(#<) (VBool a) (VBool b) = Just $ VBool (a < b)
(#<) (VInt a) (VInt b) = Just $ VBool (a < b)
(#<) (VFloat a) (VFloat b) = Just $ VBool (a < b)
(#<) _ _ = Nothing


infixl 6 #-
(#-)::RValue -> RValue -> Maybe RValue
(#-) (VInt a) (VInt b) = Just $ VInt (a - b)
(#-) (VFloat a) (VFloat b) = Just $ VFloat (a - b)
(#-) _ _ = Nothing

infixl 7 #*
(#*)::RValue -> RValue -> Maybe RValue
(#*) (VBool a) (VBool b) = Just $ VBool (a && b)
(#*) (VInt a) (VInt b) = Just $ VInt (a * b)
(#*) (VFloat a) (VFloat b) = Just $ VFloat (a * b)
(#*) _ _ = Nothing

infixl 7 #/
(#/)::RValue -> RValue -> Maybe RValue
(#/) (VInt a) (VInt b) = Just $ VInt (a `div` b)
(#/) (VFloat a) (VFloat b) = Just $ VFloat (a / b)
(#/) _ _ = Nothing

rNegate::RValue -> Maybe RValue 
rNegate (VBool a) = Just $ VBool (not a)
rNegate (VInt a) = Just $ VInt (-a)
rNegate (VFloat a) = Just $ VFloat (-a)
rNegate _ = Nothing

data Binding = FunBinding String [String] Program
             | ClassBinding String [String] [Binding]
             | VarBinding String RValue
    deriving (Show)

type Context = [[Binding]]

lookupVar::Context -> String -> Maybe RValue
lookupVar [] _ = Nothing
lookupVar ([]:ctx) name = lookupVar ctx name
lookupVar (((VarBinding vn vl) : bnds):ctx) name = if (vn == name) then Just vl else lookupVar (bnds:ctx) name
lookupVar ((_:bnds):ctx) name = lookupVar (bnds:ctx) name

lookupFun::Context -> String -> Maybe ([String], [Instruction])
lookupFun [] _ = Nothing
lookupFun ([]:ctx) name = lookupFun ctx name
lookupFun (((FunBinding fn fp fb) : bnds):ctx) name = if (fn == name) then Just (fp, fb) else lookupFun (bnds:ctx) name
lookupFun ((_:bnds):ctx) name = lookupFun (bnds:ctx) name

lookupClass::Context -> String -> Maybe ([String], [Binding])
lookupClass [] _ = Nothing
lookupClass ([]:ctx) name = lookupClass ctx name
lookupClass (((ClassBinding cn cf cm):bnds):ctx) name = if (cn == name) then Just (cf, cm) else lookupClass (bnds:ctx) name
lookupClass ((_:bnds):ctx) name = lookupClass (bnds:ctx) name


getMember::RValue -> String -> Maybe RValue
getMember (VObject _ members) memName = lookupVar [members] memName
getMember _ _ = Nothing

getMethod::Context -> RValue -> String -> Maybe ([String], [Instruction])
getMethod ctx (VObject ofClass members) methName = do
    ofClass' <- snd <$> lookupClass ctx ofClass
    lookupFun (ofClass':[members]) methName
getMethod _ _ _ = Nothing

getValue::Binding -> Maybe RValue
getValue (VarBinding _ v) = Just v
getValue _ = Nothing

getRetValue::Context -> ([String], Program) -> [Expression] -> Maybe RValue
getRetValue ctx (paramNames, body) args = do
    ctx' <- callFun ctx (paramNames, body) args
    getValue (head (head ctx'))

genParams :: Context -> [String] -> [Expression] -> Maybe [Binding]
genParams _ [] [] = Just []
genParams ctx (nm:nms) (arg:args) = do
    rest <- genParams ctx nms args
    evaluated <- eval ctx arg
    return $ (VarBinding nm evaluated):rest
genParams _ _ _ = Nothing

callFun::Context -> ([String], Program) -> [Expression] -> Maybe Context
callFun ctx (paramNames, body) args = do
    params <- genParams ctx paramNames args
    executeProgram (params:ctx) body
        

data Expression = Val RValue 
                | Bracketed Expression
                | Expression :+: Expression 
                | Expression :*: Expression 
                | Expression :/: Expression 
                | Expression :-: Expression
                | Expression :==: Expression
                | Expression :<: Expression
                | Neg Expression 
                | Var String 
                | Fun String [Expression]
                | Member Expression String
                | Method Expression String [Expression]
                | Cons String [Expression]
    deriving (Show)


eval::Context -> Expression -> Maybe RValue
eval ctx (Bracketed e) = eval ctx e
eval ctx (Neg e) = (eval ctx e) >>= rNegate
eval _ (Val x) = Just x
eval ctx (Var s) = lookupVar ctx s
eval ctx (e1 :+: e2) = do
    lft <- eval ctx e1
    rght <- eval ctx e2
    lft #+ rght
eval ctx (e1 :==: e2) = do
    lft <- eval ctx e1
    rght <- eval ctx e2
    lft #== rght
eval ctx (e1 :<: e2) = do
    lft <- eval ctx e1
    rght <- eval ctx e2
    lft #< rght
eval ctx (e1 :*: e2) = do
    lft <- eval ctx e1
    rght <- eval ctx e2
    lft #* rght
eval ctx (e1 :/: e2) = do
    lft <- eval ctx e1
    rght <- eval ctx e2
    lft #/ rght
eval ctx (e1 :-: e2) = do
    lft <- eval ctx e1
    rght <- eval ctx e2
    lft #- rght
eval ctx (Fun name args) = do 
    f <- lookupFun ctx name
    getRetValue ctx f args
eval ctx (Member ofObj name) = do
    ofObj' <- eval ctx ofObj
    getMember ofObj' name
eval ctx (Method ofObj name args) = do
    obj <- eval ctx ofObj
    mems <- getMems obj
    meth <- getMethod ctx obj name
    getRetValue ((mems):ctx) meth args 
eval ctx (Cons name fields) = consObj ctx name fields


varBinding::Context -> String -> Expression -> Maybe Binding
varBinding ctx name expr = do
    val <- eval ctx expr
    return $ VarBinding name val

getMems::RValue -> Maybe [Binding]
getMems (VObject _ mems) = Just mems
getMems _ = Nothing

bindMethod::Context -> String -> String -> [String] -> [Instruction] -> Maybe Context
bindMethod [] _ _ _ _ = Nothing
bindMethod ([]:_) _ _ _ _= Nothing
bindMethod ((bnd@(VarBinding cn (VObject oName mems)):bnds):ctx) objName mName mArgs mBody = if (cn == objName) then 
    Just (((VarBinding cn (VObject oName ((genMethod (mName, mArgs, mBody)):mems))):bnds):ctx)
    else do
        (bnds':ctx') <- bindMethod (bnds:ctx) objName mName mArgs mBody 
        return $ (bnd:bnds'):ctx'
bindMethod ((bnd:bnds):ctx) objName mName mArgs mBody = do
    (bnds':ctx') <- bindMethod(bnds:ctx) objName mName mArgs mBody 
    return $ (bnd:bnds'):ctx'

getClassName::RValue -> Maybe String
getClassName (VObject name _) = Just name
getClassName _ = Nothing

bindMember::Context -> Context -> [String] -> Expression -> Maybe Context
bindMember _ _ [] _ = Nothing
bindMember genCtx ctx [name] expr = bindVar' genCtx ctx name expr
bindMember genCtx ctx (name:rest) expr = do
    obj <- lookupVar ctx name
    oldMems <- getMems obj
    newMems <- bindMember genCtx (oldMems:ctx) rest expr
    cname <- getClassName obj
    bindVar' genCtx ctx name (Val (VObject cname (head newMems)))

bindVar'::Context -> Context -> String -> Expression -> Maybe Context
bindVar' _ [] _ _ = Nothing
bindVar' _ ([]:_) _ _ = Nothing
bindVar' genCtx ((bnd@(VarBinding vn _):bnds):ctx) name expr = if (vn == name) then do 
        vb <- varBinding genCtx name expr
        return $ (vb:bnds):ctx 
    else do
        (bnds':ctx') <- bindVar' genCtx (bnds:ctx) name expr
        return $ (bnd:bnds'):ctx'
bindVar' genCtx ((bnd:bnds):ctx) name expr = do
    (bnds':ctx') <- bindVar' genCtx (bnds:ctx) name expr
    return $ (bnd:bnds'):ctx'

bindVar::Context -> Context -> String -> Expression -> Maybe Context
bindVar genCtx [] name expr = do 
    vb <- varBinding genCtx name expr
    return $ [[vb]]
bindVar genCtx ([]:ctx) name expr = do 
    vb <- varBinding genCtx name expr
    return $ [vb]:ctx
bindVar genCtx ((bnd@(VarBinding vn _):bnds):ctx) name expr = if (vn == name) then do 
        vb <- varBinding genCtx name expr
        return $ (vb:bnds):ctx 
    else do
        (bnds':ctx') <- bindVar genCtx (bnds:ctx) name expr
        return $ (bnd:bnds'):ctx'
bindVar genCtx ((bnd:bnds):ctx) name expr = do
    (bnds':ctx') <- bindVar genCtx (bnds:ctx) name expr
    return $ (bnd:bnds'):ctx'

bindFun :: Context -> Context -> String -> [String] -> [Instruction] -> Context
bindFun _ [] name params body = [[FunBinding name params body]]
bindFun _([]:ctx) name params body = [FunBinding name params body]:ctx
bindFun genCtx ((bnd@(FunBinding fn _ _):bnds):ctx) name params body = if (fn == name) then ((FunBinding name params body):bnds):ctx else (bnd:bnds'):ctx' where
    (bnds':ctx') = bindFun genCtx (bnds:ctx) name params body
bindFun genCtx ((bnd:bnds):ctx) name params body = (bnd:bnds'):ctx' where
    (bnds':ctx') = bindFun genCtx (bnds:ctx) name params body

genMethod::(String, [String], Program) -> Binding
genMethod (name, params, body) = FunBinding name params body

bindClass::Context -> Context -> String -> [String] -> [(String, [String], [Instruction])] -> Context
bindClass _ [] name fields methods = [[ClassBinding name fields (map genMethod methods)]]
bindClass _ ([]:ctx) name fields methods = [ClassBinding name fields (map genMethod methods)]:ctx
bindClass genCtx ((bnd@(ClassBinding cn flds mthds):bnds):ctx) name fields methods = if (cn == name) then ((ClassBinding name flds ((map genMethod methods) ++ mthds)):bnds):ctx else (bnd:bnds'):ctx' where
    (bnds':ctx') = bindClass genCtx (bnds:ctx) name fields methods
bindClass genCtx ((bnd:bnds):ctx) name fields methods = (bnd:bnds'):ctx' where
    (bnds':ctx') = bindClass genCtx (bnds:ctx) name fields methods

genFields::Context -> [String] -> [Expression] -> Maybe [Binding]
genFields _ [] [] = Just []
genFields ctx (s:ss) (ex:exs) = do
    bnd <- varBinding ctx s ex
    rest <- genFields ctx ss exs
    return (bnd:rest)
genFields _ _ _ = Nothing

consObj::Context -> String -> [Expression] -> Maybe RValue
consObj ctx className fieldValues = do 
    (fields, _) <- lookupClass ctx className
    fields' <- genFields ctx fields fieldValues
    return $ VObject className fields'

data Instruction = WhileLoop Expression Program
                 | If Expression Program Program
                 | VarAssign String Expression
                 | FieldAssign [String] Expression
                 | DynAddMethod String String [String][Instruction]
                 | MethodCall [String] [Expression]
                 | FuncDeclare String [String] [Instruction]
                 | FuncCall String [Expression]
                 | Ret Expression
                 | ClassDeclare String [String] [(String, [String], [Instruction])] 
    deriving Show

type Program = [Instruction]

execute::Context -> Instruction -> Maybe Context
execute ctx wl@(WhileLoop cnd body) = do
    cond <- getBoolValue <$> eval ctx cnd
    if cond then do
            ctx' <- executeProgram ctx body
            execute ctx' wl 
        else return ctx 
execute ctx (If cnd bodyTrue bodyFalse) = do
    cond <- getBoolValue <$> eval ctx cnd
    if cond then executeProgram ctx bodyTrue
        else executeProgram ctx bodyFalse 
execute ctx (VarAssign s expr) = bindVar ctx ctx s expr
execute ctx (Ret expr) = do 
    vb <- varBinding ctx "retValue" expr
    return $ [vb]:(tail ctx)
execute ctx (FuncCall s args) = do
    f <- lookupFun ctx s
    tail <$> (callFun ctx f args)
execute ctx (FuncDeclare s params body) = return $ bindFun ctx ctx s params body
execute ctx (FieldAssign names expr) = bindMember ctx ctx names expr
execute ctx (ClassDeclare name fields methods) = return $ bindClass ctx ctx name fields methods
execute ctx (DynAddMethod cn mName mArgs mBody) = bindMethod ctx cn mName mArgs mBody 
execute _ _ = Nothing

executeProgram::Context -> Program -> Maybe Context
executeProgram ctx [] = Just ctx
executeProgram ctx (instruction:program) = do
    ctx' <- execute ctx instruction
    executeProgram ctx' program



 -- Parser

type Stream = [Char]
newtype Parser a = P (Stream -> [(a, Stream)])

run:: Parser a -> Stream -> [(a, Stream)]
run (P f) = f

pReturn::a -> Parser a
pReturn x = P $ \s -> [(x, s)]

instance Functor Parser where
    fmap f (P p) = P $ fmap (fmap (first f)) p where
        first g (a, b) = (g a, b)

apply :: Parser (a -> b) -> Parser a -> Parser b
apply (P pf) (P px) = P $ \inp ->
    flip concatMap (pf inp) $ \ (f, tl1) ->
    flip concatMap (px tl1) $ \ (x, tl2) ->
    [(f x, tl2)]

instance Applicative Parser where
    pure = pReturn
    (<*>) = apply

pFail::Parser a
pFail = P (const [])

alt::Parser a -> Parser a -> Parser a
alt (P p1) (P p2) = P (\ inp -> p1 inp ++ p2 inp)

altOr::Parser a -> Parser a -> Parser a
altOr (P p1) (P p2) = P (\ inp -> let prsd1 = p1 inp in 
    if null prsd1 then p2 inp else prsd1)

instance Monad Parser where
    return = pure
    P pa >>= a2pb = P $ \inp -> 
        flip concatMap (pa inp) $ \(x, tl) ->
        run (a2pb x) tl

pSym :: Char -> Parser Char
pSym x = P helper where
    helper [] = []
    helper (c:s) = if c == x then [(c, s)] else []

pLetter :: Parser Char
pLetter = P helper where
    helper [] = []
    helper (c:s) = if isLetter c then [(c, s)] else []

pDigit :: Parser Char
pDigit = P helper where
    helper [] = []
    helper (c:s) = if isDigit c then [(c, s)] else []


-- Parser of Letters, Digits and Underscores
pLDU :: Parser Char
pLDU = P helper where
    helper [] = []
    helper (c:s) = if isLetter c || isDigit c || c == '_'
        then [(c, s)] 
        else []    

pMany::Parser a -> Parser [a]
pMany p = do
    l <- p
    r <- altOr (pMany p) (pReturn [])
    return (l:r)

pAny::Parser a -> Parser [a]
pAny p = altOr (pMany p) (pReturn [])
  
pLDUs::Parser [Char]
pLDUs = pAny pLDU

pIdentifier::Parser [Char]
pIdentifier = do
    l <- pLetter
    r <- pLDUs
    return (l:r)

pVariable::Parser Expression
pVariable = do
    name <- pIdentifier
    return (Var name)

isEmptyMember::Expression -> Bool
isEmptyMember (Member _ "") = True
isEmptyMember _ = False

pMember::Expression -> Parser Expression
pMember left = do
    _ <- pSym '.'
    name <- pIdentifier
    more <- altOr (altOr (pMethodCall (Member left name))
        (pMember (Member left name)))
        (pReturn (Member left ""))
    return (if (isEmptyMember more) then (Member left name) 
        else more)

pMethodCall::Expression -> Parser Expression
pMethodCall left = do
    _ <- pSym '.'
    name <- pIdentifier
    _ <- pSym '('
    args <- altOr (pListOf pExpr) (pReturn [])
    _ <- pSpaces
    _ <- pSym ')'
    more <- altOr (altOr (pMethodCall (Method left name args))
        (pMember (Method left name args)))
        (pReturn (Member left ""))
    return (if (isEmptyMember more) then 
        (Method left name args) else more)

pMembersExp::Parser Expression
pMembersExp = do
    left <- altOr (altOr pCons pFuncCall) pVariable
    mems <- altOr (altOr (pMethodCall left) (pMember left))
        (pReturn (Member left ""))
    return (if (isEmptyMember mems) then left else mems)

pIdHelper::Parser [String]
pIdHelper = do
    identifier <- pIdentifier
    return [identifier]

pMemberAss::Parser[String]
pMemberAss = do
    left <- pIdentifier
    _ <- pSym '.'
    right <- altOr pMemberAss pIdHelper
    return (left:right)

pCons::Parser Expression
pCons = do
    name <- pIdentifier
    _ <- pSym '.'
    _ <- pString "new"
    _ <- pSym '('
    args <- altOr (pListOf pExpr) (pReturn [])
    _ <- pSpaces
    _ <- pSym ')'
    return (Cons name args)

intFromString :: [Char] -> Int
intFromString = foldl (\a b -> 10 * a + digitToInt b) 0
    
pInt::Parser RValue
pInt = do
    val <- pMany pDigit
    return (VInt (intFromString val))

pow10::Int -> Int
pow10 0 = 1
pow10 n | n > 0 = 10 * pow10 (n - 1)
        | otherwise = 1

pFloatForced::Parser RValue
pFloatForced = do
    l <- pMany pDigit
    r <- pFrac
    return (VFloat (fromIntegral (intFromString l) + r)) where
        pFrac = do
            _ <- pSym '.'
            r <- pMany pDigit
            return (fromIntegral (intFromString r) /
                fromIntegral (pow10 (length r)))

pSpaces::Parser String
pSpaces = pAny (pSym ' ')

pNotSym::Char -> Parser Char
pNotSym x = P helper where
    helper [] = []
    helper (c:s) = if c == x then [] else [(c, s)]

pCharConst::Parser RValue
pCharConst = do
    _ <- pSym '\"'
    c <- pNotSym '\"'
    _ <- pSym '\"'
    return (VChar c)

pStringConst::Parser RValue
pStringConst = do
    _ <- pSym '\"'
    s <- pAny (pNotSym '\"')
    _ <- pSym '\"'
    return $ VArray (fmap VChar s)

pTrue::Parser RValue
pTrue = do
    _ <- pString "true"
    return (VBool True)

pFalse::Parser RValue
pFalse = do
    _ <- pString "false"
    return (VBool False)

pBool::Parser RValue
pBool = alt pTrue pFalse

pConst::Parser Expression
pConst = do
    res <- altOr (altOr pBool pCharConst) (altOr pStringConst(altOr pFloatForced pInt))
    return (Val res)

data Sgn = AND | OR | MUL | DIV | SUM | DIF | LESS | EQUAL deriving (Eq, Show)

genExpr::Expression -> Sgn -> Expression -> Expression
genExpr exp1 sgn exp2 | sgn == AND = exp1 :*: exp2
                      | sgn == OR = exp1 :+: exp2
                      | sgn == MUL = exp1 :*: exp2
                      | sgn == DIV = exp1 :/: exp2
                      | sgn == SUM = exp1 :+: exp2
                      | sgn == DIF = exp1 :-: exp2
                      | sgn == LESS = exp1 :<: exp2
                      | otherwise = exp1 :==: exp2

pAND::Parser Sgn
pAND = do
    _ <- pSym '&'
    return AND

pOR::Parser Sgn
pOR = do
    _ <- pSym '|'
    return OR

pMUL::Parser Sgn
pMUL = do
    _ <- pSym '*'
    return MUL

pDIV::Parser Sgn
pDIV = do
    _ <- pSym '/'
    return DIV

pSUM::Parser Sgn
pSUM = do
    _ <- pSym '+'
    return SUM

pDIF::Parser Sgn
pDIF = do
    _ <- pSym '-'
    return DIF

pLESS::Parser Sgn
pLESS = do
    _ <- pSym '<'
    return LESS

pEQUAL::Parser Sgn
pEQUAL = do
    _ <- pString "=="
    return EQUAL


pBracketed::[(Expression, Sgn)] -> Parser Expression
pBracketed ops = do
    _ <- pSym '('
    _ <- pSpaces
    expr <- pExpr
    _ <- pSpaces
    _ <- pSym ')'
    return (if length ops == 0 then expr else 
        genExpr exprL sgn expr) where
            (exprL, sgn) = head ops

pNeg::[(Expression, Sgn)] -> Parser Expression
pNeg ops = do
    _ <- alt (pSym '-') (pSym '!')
    _ <- pSpaces
    expr <- pExpr
    return (if length ops == 0 then Neg expr else 
        genExpr exprL nsgn (Neg expr)) where
            (exprL, nsgn) = head ops

pSingleExp::[(Expression, Sgn)] -> Parser Expression
pSingleExp ops = do
    _ <- pSpaces
    val <- altOr pConst pMembersExp
    _ <- pSpaces
    return (if length ops == 0 then val else 
        genExpr exprL sgn val) where
            (exprL, sgn) = head ops

pProd::[(Expression, Sgn)] -> Parser Expression
pProd ops = do
    exprL <- altOr (pSingleExp []) (pBracketed [])
    _ <- pSpaces
    sgn <- alt pAND (alt pMUL pDIV)
    _ <- pSpaces
    exprR <- altOr (pProd (nops exprL sgn))(altOr 
        (pSingleExp (nops exprL sgn)) 
        (pBracketed (nops exprL sgn)))
    return exprR where
                nops exprL sgn = if length ops == 0 then
                    [(exprL, sgn)] else
                    (genExpr exprLn nsgn exprL, sgn):(tail ops) 
                        where (exprLn, nsgn) = head ops

pProd'::[(Expression, Sgn)] -> Parser Expression
pProd' ops = do
    expr <- pProd []
    return (if length ops == 0 then expr else 
        genExpr exprL sgn expr) where
            (exprL, sgn) = head ops

pSum::[(Expression, Sgn)] -> Parser Expression
pSum ops = do
    exprL <- altOr (altOr (pProd []) (pNeg []))
        (altOr (pSingleExp []) (pBracketed []))
    _ <- pSpaces
    sgn <- alt pOR (alt pSUM pDIF)
    _ <- pSpaces
    exprR <- altOr (pNeg (nops exprL sgn)) 
        (altOr (altOr (pSum (nops exprL sgn)) 
        (pProd' (nops exprL sgn)))
        (altOr (pSingleExp (nops exprL sgn)) 
        (pBracketed (nops exprL sgn))))
    return exprR where
                nops exprL sgn = if length ops == 0 then
                    [(exprL, sgn)] else
                    (genExpr exprLn nsgn exprL, sgn):(tail ops) 
                        where (exprLn, nsgn) = head ops

pSum'::[(Expression, Sgn)] -> Parser Expression
pSum' ops = do
    expr <- pSum []
    return (if length ops == 0 then expr else 
        genExpr exprL sgn expr) where
            (exprL, sgn) = head ops

pCmp::[(Expression, Sgn)] -> Parser Expression
pCmp ops = do
    exprL <- altOr (altOr (altOr (pSum []) (pProd [])) 
                          (pNeg []))
                   (altOr (pSingleExp []) (pBracketed []))
    _ <- pSpaces
    sgn <- alt pLESS pEQUAL
    _ <- pSpaces
    exprR <- altOr (pNeg (nops exprL sgn)) 
        (altOr (altOr (pCmp (nops exprL sgn)) 
        (pSum' (nops exprL sgn)))
        (altOr (pSingleExp (nops exprL sgn)) 
        (pBracketed (nops exprL sgn))))
    return exprR where
                nops exprL sgn = if length ops == 0 then
                    [(exprL, sgn)] else
                    (genExpr exprLn nsgn exprL, sgn):(tail ops) 
                        where (exprLn, nsgn) = head ops



pExpr :: Parser Expression
pExpr = altOr (altOr (altOr (pCmp []) (pSum []))
                     (altOr (pProd []) (pNeg [])))
              (altOr (pBracketed []) (pSingleExp []))

pFuncCall::Parser Expression
pFuncCall = do
    l <- pIdentifier
    _ <- pSym '('
    args <- altOr (pListOf pExpr) (pReturn [])
    _ <- pSpaces
    _ <- pSym ')'
    return (Fun l args)

pRet::Parser Instruction
pRet = do
    _ <- pString "return"
    _ <- pSpaces
    expr <- pExpr
    return (Ret expr)

pIndent :: Int -> Parser Char
pIndent 0 = return ' '
pIndent 1 = pSym ' '
pIndent k = do
    _ <- pSym ' '
    ret <- pIndent (k - 1)
    return ret

pFieldAss::Parser Instruction
pFieldAss = do 
    lst <- pMemberAss
    _ <- pSpaces
    _ <- pSym '='
    _ <- pSpaces
    r <- pExpr
    return (FieldAssign lst r)

pVarAss::Parser Instruction
pVarAss = do
    l <- pIdentifier
    _ <- pSpaces
    _ <- pSym '='
    _ <- pSpaces
    r <- pExpr
    return (VarAssign l r)

pAss::Parser Instruction
pAss = altOr pFieldAss pVarAss

pString::String -> Parser String
pString [] = return []
pString (c:s) = do
    h <- pSym c
    t <- pString s
    return (h:t)

pListOf::Parser a -> Parser [a]
pListOf p = do
    _ <- pSpaces
    l <- p
    _ <- pSpaces
    r <- altOr (pRest p) (pReturn [])
    return (l:r) where
        pRest p1 = do
            _ <- pSym ','
            _ <- pSpaces
            res <- pListOf p1
            return res

pFunDeclare::Int -> Parser Instruction
pFunDeclare indent = do
    _ <- pString "def"
    _ <- pSpaces
    name <- pIdentifier
    _ <- pSym '('
    args <- pListOf pIdentifier
    _ <- pSpaces
    _ <- pSym ')'
    _ <- pSpaces
    _ <- pSym '\n'
    prog <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "end"
    return (FuncDeclare name args prog)

pMethod::Int -> Parser (String, [String], Program)
pMethod indent = do
    _ <- pIndent indent
    _ <- pString "def"
    _ <- pSpaces
    name <- pIdentifier
    _ <- pSym '('
    args <- pListOf pIdentifier
    _ <- pSpaces
    _ <- pSym ')'
    _ <- pSpaces
    _ <- pSym '\n'
    prog <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "end"
    _ <- pSpaces
    _ <- pSym '\n'
    return (name, args, prog)

pAccessor::Parser String
pAccessor = pSym ':' >>= (\_ -> pIdentifier)

pFields::Int -> Parser [String]
pFields indent = do
    _ <- pIndent indent
    _ <- pString "attr_accessor"
    _ <- pSpaces
    fields <- pListOf pAccessor
    _ <- pSpaces
    _ <- pSym '\n'
    return fields

pClassDeclare::Parser Instruction
pClassDeclare = do
    _ <- pString "class"
    _ <- pSpaces
    name <- pIdentifier
    _ <- pSpaces
    _ <- pSym '\n'
    fields <- pFields 4
    methods <- pAny (pMethod 4)
    _ <- pString "end"
    return (ClassDeclare name fields methods)

pWhile::Int -> Parser Instruction
pWhile indent = do
    _ <- pString "while"
    _ <- pSpaces
    cond <- pExpr
    _ <- pSpaces
    _ <- altOr (pString "do") (pReturn [])
    _ <- pSpaces
    _ <- pSym '\n'
    body <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "end"
    return (WhileLoop cond body)

pIf::Int -> Parser Instruction
pIf indent = do
    _ <- pString "if"
    _ <- pSpaces
    cond <- pExpr
    _ <- pSpaces
    _ <- pSym '\n'
    bodyT <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "else"
    _ <- pSpaces
    _ <- pSym '\n'
    bodyF <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "end"
    return (If cond bodyT bodyF)

pInstr::Int -> Parser Instruction
pInstr indent = altOr
    (altOr pClassDeclare (pFunDeclare indent))
    (altOr (altOr (pIf indent) (pWhile indent))
          (altOr pAss pRet))

pProg::Int -> Parser Program
pProg indent = do
    _ <- pIndent indent
    l <- pInstr indent
    _ <- pSpaces
    _ <- pSym '\n'
    r <- altOr (pProg indent) (pReturn [])
    return (l:r)

myParse::Stream -> Program
myParse x = let parsed = run (pProg 0) x in 
    if null parsed then [] else fst (head parsed)

myRun::Program -> Maybe Context
myRun = executeProgram [[]]
