module Pomogator(cleanup) where

import Prelude
import HRuby
import Data.List

data BndType = VarBnd | FunBnd deriving (Eq, Show)

walk::Expression -> [(String, BndType)]
walk  (Bracketed e) = walk  e
walk  (Neg e) = walk  e
walk  (Val _) = []
walk  (Var s) = [(s, VarBnd)]
walk  (e1 :+: e2) = (walk  e1) ++ (walk  e2)
walk  (e1 :==: e2) = (walk  e1) ++ (walk  e2)
walk  (e1 :<: e2) = (walk  e1) ++ (walk  e2)
walk  (e1 :*: e2) = (walk  e1) ++ (walk  e2)
walk  (e1 :/: e2) = (walk  e1) ++ (walk  e2)
walk  (e1 :-: e2) = (walk  e1) ++ (walk  e2)
walk  (Fun name args) = (name, FunBnd) : concatMap walk args
walk  (Member ofObj _) = walk  ofObj
walk  (Method ofObj _ args) = walk  ofObj ++ concatMap walk args
walk  (Cons _ fields) = concatMap walk fields

visit::Instruction -> [(String, BndType)]
visit (WhileLoop cnd body) = walk  cnd ++ visitProgram  body
visit  (If cnd bodyTrue bodyFalse) = walk  cnd ++ visitProgram  bodyTrue 
    ++ visitProgram  bodyFalse 
visit  (VarAssign _ expr) = walk  expr
visit  (Ret expr) = walk  expr
visit  (FuncCall _ args) = concatMap walk args
visit  (FuncDeclare _ params body) = filter 
    (not . flip elem params')(visitProgram  body) where
        params' = map (\x -> (x, VarBnd)) params
visit  (FieldAssign names expr) = (head names, VarBnd) : walk  expr
visit  (ClassDeclare _ fields methods) = filter
    (not . flip elem (fields' ++ methods')) 
    (concatMap (visit ) methBodies) where
        fields' = map (\x -> (x, VarBnd)) fields
        methods' = map (\(x, _, _) -> (x, FunBnd)) methods
        methBodies = map (\(a, b, c) -> FuncDeclare a b c) methods
visit  (DynAddMethod _ _ mArgs mBody) = filter 
    (not . flip elem params')(visitProgram  mBody) where
        params' = map (\x -> (x, VarBnd)) mArgs
visit  _ = []

visitProgram'::Program -> [(String, BndType)]
visitProgram'  [] = []
visitProgram'  (instruction:program) = visit  instruction ++ visitProgram'  program

visitProgram::Program -> [(String, BndType)]
visitProgram program = nub (visitProgram' program)


revisitProgram::[(String, BndType)] -> Program -> Program
revisitProgram _ [] = []
revisitProgram needed (bnd@(VarAssign s _):program) = if 
    (elem (s, VarBnd) needed) then bnd:program' else program' where
        program' = revisitProgram needed program
revisitProgram needed (bnd@(FuncDeclare s _ _):prog) =
    if (elem (s, FunBnd) needed) then bnd:program' else program' where
        program' = revisitProgram needed prog
revisitProgram needed (bnd : prog) = bnd : revisitProgram needed prog

cleanup::Program -> Program
cleanup prog = revisitProgram (visitProgram prog) prog
