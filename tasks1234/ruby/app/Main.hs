module Main(main) where

import Prelude
import HRuby
import Pomogator

main :: IO ()
main = do
    program <- readFile "test/prog1.ruby"
    putStrLn program
    let parsed = myParse program
    putStrLn $ show $ parsed
    putStrLn ""
    let cleansed = cleanup parsed
    putStrLn $ show $ cleansed
    putStrLn ""
    putStrLn $ show $ myRun cleansed