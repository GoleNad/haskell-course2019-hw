{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns -fwarn-incomplete-uni-patterns #-}

module Interpreter where

import qualified Data.Map.Strict as Map
import AST
import Numeric.Extra (intToDouble)
import Data.Fixed (mod')


newtype Context = Context {getContext :: Map.Map Literal Literal} deriving Show

createContext :: Context
createContext = Context {getContext = Map.empty}

getValue :: Context -> Literal -> Either String Literal
getValue ctx idx = case (Map.lookup (simpleName idx) $ getContext ctx) of
    Just x -> return (simpleName x)
    Nothing -> Left ("Missed variable " ++ (show $ simpleName idx) ++ ".")

isInTable :: Literal -> Literal -> Bool
isInTable t k = case k of
    BiOp Fld l r -> t == l || isInTable t l
    otherwise    -> False

deleteTable :: Literal -> Literal -> Literal -> Literal
deleteTable t k v = if isInTable (simpleName t) (simpleName k)
    then Nil
    else v

setValue :: Context -> Literal -> Literal -> Either String Context
setValue ctx name value = return $ Context {getContext = Map.insert (simpleName name) value $ Map.mapWithKey (deleteTable $ simpleName name) $ getContext ctx}

simpleName :: Literal -> Literal
simpleName (Var v) = simpleName v
simpleName (BiOp Fld l r) = BiOp Fld (simpleName l) (simpleName r)
simpleName x = x

localContext :: Context -> Context -> [Literal] -> [Literal] -> Either String Context
localContext mctx ctx (p:ps) (a:as) = do
        a' <- evalRVal ctx a
        ctx' <- setValue (fst a') p (snd a')
        localContext mctx ctx' ps as
localContext _ ctx [] [] = return ctx
localContext _ _ _ _ = Left "Wrong number of arguments."

restore :: Context -> Context -> [Literal] -> Either String Context
restore ctx glbl (p:ps) = case getValue glbl p of
    Left _ -> do
        ctx' <- setValue ctx p Nil
        restore ctx' glbl ps
    Right p' -> do
        ctx' <- setValue ctx p p'
        restore ctx' glbl ps 
restore ctx _ [] = return ctx

evalProg :: Statement -> Either String Context
evalProg = evalBlock createContext

evalBlock :: Context -> Statement -> Either String Context
evalBlock ctx (Block prog) = evalProg' ctx prog where
    evalProg' ctx (x:xs) = do
        ctx' <- evalStmt ctx x
        evalProg' ctx' xs
    evalProg' ctx [] = return ctx

evalBlock ctx _ = Left "Code isn't in 'Block' statement."

evalStmt :: Context -> Statement -> Either String Context
evalStmt ctx (Assign (l:ls) (r:rs)) = case r of
    Left l' -> do
        v <- evalRVal ctx l'
        ctx' <- setValue (fst v) l (snd v)
        evalStmt ctx' (Assign ls rs)
    Right (Block (t:ts)) -> case t of
        Assign tl tr -> do
            ctx' <- evalStmt ctx (Assign (fmap (simpleName . BiOp Fld l) tl) tr)
            evalStmt ctx' (Assign (l:ls) (Right (Block ts) : rs))
        otherwise -> Left "Impossible due to parser."
    Right (Block []) -> evalStmt ctx (Assign ls rs)
    otherwise -> Left "Impossible due to parser."

evalStmt ctx (Assign (l:ls) []) = do
    ctx' <- setValue ctx l Nil
    evalStmt ctx' (Assign ls [])

evalStmt ctx (Assign [] _) = return ctx

evalStmt ctx (Define name args body) = setValue ctx name $ Call $ Define name args body

evalStmt ctx (Function name args) = case getValue ctx name of
        Right (Call (Define _ params body)) -> do
            ctx' <- localContext ctx ctx params args 
            ctx'' <- evalStmt ctx' body
            ans <- getValue ctx'' (SConst "return") 
            case simpleName ans of
                SConst _ -> return ctx''
                Call _ -> return ctx'' 
                otherwise -> restore ctx'' ctx params
        Right (Call (ALambda prms body)) -> do
            ctx' <- localContext ctx ctx prms args
            ctx'' <- evalStmt ctx' body
            ans <- getValue ctx'' (SConst "return")
            case simpleName ans of
                SConst _ -> return ctx''
                Call _ -> return ctx''
                otherwise -> restore ctx'' ctx prms
        otherwise -> Left $ "Missed declaration of function " ++ (show $ simpleName name) ++ "." 

evalStmt ctx (ALambda params body) = do
    res <- evalRVal ctx (Call (ALambda params body))
    return $ fst res

evalStmt ctx (Lambda params body args) = do
    ctx' <- localContext ctx ctx params args
    ctx'' <- evalStmt ctx' body
    ans <- getValue ctx'' (SConst "return")
    case simpleName ans of
        SConst _ -> return ctx''
        Call _ -> return ctx'' 
        otherwise -> restore ctx'' ctx params

evalStmt ctx b@(Block _) = evalBlock ctx b

evalStmt ctx (IfThenElse cond th el) = do
    cond' <- evalRVal ctx cond
    if snd cond' == BConst True
        then evalStmt (fst cond') th
        else case el of
            Just el' -> evalStmt (fst cond') el'
            otherwise -> return (fst cond')

evalStmt ctx (For var init fin step body) = do
    init' <- evalRVal ctx init
    fin' <- evalRVal (fst init') fin
    step' <- evalRVal (fst fin') step
    ctx' <- setValue (fst step') var (snd init')
    ctx'' <- evalFor ctx' var (snd fin') (snd step') body
    return ctx''
    
evalStmt ctx (ForEach var (i:is) body) = do
    res <- evalRVal ctx i
    ctx' <- setValue (fst res) var (snd res)
    ctx'' <- evalBlock ctx' body
    evalStmt ctx'' (ForEach var is body)

evalStmt ctx (ForEach _ [] _) = return ctx

evalStmt ctx (While cond body) = do
    cond' <- evalRVal ctx cond
    if snd cond' == BConst True
        then do
            ctx' <- evalBlock (fst cond') body
            evalStmt ctx' (While cond body)
        else return (fst cond')

evalStmt ctx (Repeat body cond) = do
    ctx' <- evalBlock ctx body
    cond' <- evalRVal ctx' cond
    if snd cond' == BConst True
        then evalStmt (fst cond') (Repeat body cond)
        else return (fst cond')

evalStmt ctx (Local body) = do
    ctx1 <- setValue ctx (SConst "return") Nil
    ctx2 <- evalBlock ctx1  body
    res <- getValue ctx2 (SConst "return")
    ctx3 <- setValue ctx (SConst "return") res 
    return ctx3

evalStmt ctx (Return res) = do
    res' <- evalRVal ctx res
    ctx' <- setValue (fst res') (SConst "return") (snd res')
    return ctx'

evalStmt _ (Label _) = Left "Not implemented."
evalStmt _ (Goto _)  = Left "Not implemented."
evalStmt _ Break     = Left "Not implemented."

evalFor :: Context -> Literal -> Literal -> Literal -> Statement -> Either String Context
evalFor ctx var fin step body = do
    ctx' <- evalBlock ctx body
    res <- evalRVal ctx' (BiOp Add var step)
    ctx'' <- setValue (fst res) var (snd res)
    cond <- evalRVal ctx'' (BiOp Le var fin)
    if snd cond == BConst True
        then evalFor (fst cond) var fin step body
        else return $ fst cond

evalRVal :: Context -> Literal -> Either String (Context, Literal)
evalRVal ctx (Var v) = do
    res <- getValue ctx v
    return (ctx, res)

evalRVal ctx v@(BiOp Fld _ _) = do
    res <- getValue ctx (simpleName v)
    return (ctx, res)

evalRVal ctx (Call al@(ALambda{})) = do
    ctx' <- setValue ctx (SConst "return") (Call al)
    return (ctx', Call al)

evalRVal ctx (Call f) = do
    ctx' <- evalStmt ctx f
    res <- getValue ctx' (SConst "return")
    return (ctx', res)

evalRVal ctx c@(AConst _) = return (ctx, c)

evalRVal ctx c@(BConst _) = return (ctx, c)

evalRVal ctx c@(SConst _) = case getValue ctx c of
    Left _ -> return (ctx, c)
    Right x -> evalRVal ctx x

evalRVal ctx Nil = return (ctx, Nil)

evalRVal ctx (UnOp Neg v) = do
    f <- evalRVal ctx v
    case snd f of
        AConst v' -> return (fst f, AConst (negate v'))
        otherwise -> Left "Wrong type of operand."

evalRVal ctx (UnOp Len v) = do
    f <- evalRVal ctx v
    case snd f of
        SConst v' -> return (fst f, AConst (intToDouble $ length v'))
        otherwise -> Left "Wrong type of operand."

evalRVal ctx (UnOp Not v) = do
    v' <- evalRVal ctx v
    if snd v' == Nil || snd v' == BConst False
        then return (fst v', BConst True)
        else return (fst v', BConst False)

evalRVal ctx (UnOp{}) = Left "Impossible due to parser."

evalRVal ctx (BiOp Con l r) = evalSBiOp ctx (++) l r

evalRVal ctx (BiOp Pow l r) = evalABiOp ctx (**) l r

evalRVal ctx (BiOp Mul l r) = evalABiOp ctx (*) l r

evalRVal ctx (BiOp Div l r) = evalABiOp ctx (/) l r

evalRVal ctx (BiOp Mod l r) = evalABiOp ctx (mod') l r

evalRVal ctx (BiOp Add l r) = evalABiOp ctx (+) l r

evalRVal ctx (BiOp Sub l r) = evalABiOp ctx (-) l r

evalRVal ctx (BiOp Le l r) = evalCBiOp ctx (<=) l r

evalRVal ctx (BiOp Lt l r) = evalCBiOp ctx (<) l r

evalRVal ctx (BiOp Ge l r) = evalCBiOp ctx (>=) l r

evalRVal ctx (BiOp Gt l r) = evalCBiOp ctx (>) l r

evalRVal ctx (BiOp Eq l r) = do
    l' <- evalRVal ctx l
    case snd l' of
        AConst _ -> evalCBiOp ctx (==) l r
        BConst _ -> evalBBiOp ctx (==) l r
        Nil -> evalBBiOp ctx (==) l r
        otherwise -> Left "Wrong type of left operand in equation."

evalRVal ctx (BiOp Ne l r) = do 
    l' <- evalRVal ctx l
    case snd l' of
        AConst _ -> evalCBiOp ctx (/=) l r
        BConst _ -> evalBBiOp ctx (/=) l r
        Nil -> evalBBiOp ctx (/=) l r
        otherwise -> Left "Wrong type of left operand in equation."

evalRVal ctx (BiOp And l r) = evalBBiOp ctx (&&) l r

evalRVal ctx (BiOp Or l r) = evalBBiOp ctx (||) l r

evalRVal ctx (BiOp{}) = Left "Impossible due to parser."

evalSBiOp :: Context -> (String -> String -> String) -> Literal -> Literal -> Either String (Context, Literal)
evalSBiOp ctx f l r = do
    l' <- evalRVal ctx l
    case snd l' of
        SConst l'' -> do
            r' <- evalRVal (fst l') r
            case snd r' of
                SConst r'' -> return (fst r', SConst (f l'' r''))
                otherwise -> Left $ "Wrong type of right string operand " ++ show (simpleName r) ++ "."
        otherwise -> Left $ "Wrong type of left string operand " ++ show (simpleName l) ++ "."


evalABiOp :: Context -> (Double -> Double -> Double) -> Literal -> Literal -> Either String (Context, Literal)
evalABiOp ctx f l r = do
    l' <- evalRVal ctx l
    case snd l' of
        AConst l'' -> do
            r' <- evalRVal (fst l') r
            case snd r' of
                AConst r'' -> return (fst r', AConst (f l'' r''))
                otherwise -> Left $ "Wrong type of right arithmetics operand " ++ show (simpleName r)  ++ "."
        otherwise -> Left $ "Wrong type of left arithmetics operand " ++ show (simpleName l) ++ "."

evalBBiOp :: Context -> (Bool -> Bool -> Bool) -> Literal -> Literal -> Either String (Context, Literal)
evalBBiOp ctx f l r = do
    l' <- evalRVal ctx l
    case snd l' of
        BConst l'' -> do
            r' <- evalRVal (fst l') r
            case snd r' of
                BConst r'' -> return (fst r', BConst (f l'' r''))
                Nil -> return (fst r', BConst (f l'' False))
                otherwise -> Left $ "Wrong type of right boolean operand " ++ show (simpleName r)  ++ "."
        Nil -> do
            r' <- evalRVal (fst l') r
            case snd r' of
                BConst r'' -> return (fst r', BConst (f False r''))
                Nil -> return (fst r', BConst (f False False))
                otherwise -> Left $ "Wrong type of right boolean operand " ++ show (simpleName r)  ++ "."
        otherwise -> Left $ "Wrong type of left boolean operand " ++ show (simpleName l) ++ "."

evalCBiOp :: Context -> (Double -> Double -> Bool) -> Literal -> Literal -> Either String (Context, Literal)
evalCBiOp ctx f l r = do
    l' <- evalRVal ctx l
    case snd l' of
        AConst l'' -> do
            r' <- evalRVal (fst l') r
            case snd r' of
                AConst r'' -> return (fst r', BConst (f l'' r''))
                otherwise -> Left $ "Wrong type of right comparison operand " ++ show (simpleName r)  ++ "."
        otherwise -> Left $ "Wrong type of left comparison operand " ++ show (simpleName l) ++ "."
