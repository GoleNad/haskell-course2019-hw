{-# OPTIONS_GHC -fwarn-incomplete-patterns -fwarn-incomplete-uni-patterns #-}

module Optimization where

import AST
import qualified Data.Map.Strict as Map
import qualified Data.Maybe as Maybe


newtype Context = Context {getContext :: Map.Map Literal Literal} deriving Show

createContext :: Context
createContext = Context {getContext = Map.empty}

simpleName :: Literal -> Literal
simpleName (Var v) = simpleName v
simpleName (BiOp Fld l r) = BiOp Fld (simpleName l) (simpleName r)
simpleName x = x

getValue :: Context -> Literal -> Maybe Literal
getValue ctx idx = case Map.lookup (simpleName idx) $ getContext ctx of
    Just a -> Just a
    Nothing -> if null $ Map.toList (Map.filterWithKey (isInTable (simpleName idx)) (getContext ctx))
        then Nothing 
        else getValue (setValue ctx idx Nil) (simpleName idx)

isInTable :: Literal -> Literal -> Literal -> Bool
isInTable t k _ = case k of
    BiOp Fld l _ -> t == l || isInTable t l k
    otherwise    -> False

setValue :: Context -> Literal -> Literal -> Context
setValue ctx name value = Context {getContext = Map.insert (simpleName name) value $ getContext ctx}

addCtx :: Context -> Literal -> Context
addCtx ctx v = if Maybe.isJust $ getValue ctx v
    then ctx
    else case simpleName v of
        UnOp _ a -> addCtx ctx a
        BiOp Fld a b -> setValue ctx (BiOp Fld a b) Nil
        BiOp _ a b -> addCtx (addCtx ctx a) b
        Call (Function name args) -> setValue (addsCtx ctx args) name Nil
        x -> setValue ctx x Nil

addsCtx :: Context -> [Literal] -> Context
addsCtx = foldl addCtx

dceProg :: Statement -> Statement
dceProg b = Block res where
    (_, res, _) = dce (createContext, [], [b])

dce :: (Context, [Statement], [Statement]) -> (Context, [Statement], [Statement])
dce (ctx, es, []) = (ctx, es, [])

dce (ctx, es, fs) = case last fs of

    Assign [] _ -> (ctx, es, [])

    Assign (l:ls) [] ->
        let (ctx'', res'', _) = dce (ctx, [], [Assign ls []]) in
                if Maybe.isNothing $ getValue ctx'' l
                    then dce (ctx'', res'', init fs)
                    else dce (ctx'', Assign [l] [Left Nil] : res'', init fs)
    
    Assign (l:ls) (r:rs) -> case last (r:rs) of
        Right Break -> let
            (ctx', res', _)   = dce (ctx, [], [Assign ls rs])
            (ctx'', res'', _) = dce (ctx', [], [Assign ls rs]) in
                if res' /= res''
                    then dce (ctx'', [], fs)
                    else if Maybe.isNothing $ getValue ctx'' l
                        then case r of 
                            Left r' -> (ctx'', res'', fs)
                            Right Break -> dce (ctx'', res'', [Assign (l:ls) []])
                            Right (Block (Assign tl tr : ts)) -> 
                                dce (ctx'', res'', [Assign (fmap (simpleName . BiOp Fld l) tl) tr, Assign [l] [Right $ Block ts]])
                            otherwise -> (ctx'', res'', fs)
                        else case r of
                            Left r' -> (addCtx ctx'' r', Assign [l] [r] : res'', fs)
                            Right (Block (Assign tl tr : ts)) -> 
                                dce (ctx'', res'', [Assign (fmap (simpleName . BiOp Fld l) tl) tr, Assign [l] [Right $ Block ts]])
                            otherwise -> (ctx'', res'', fs)
        otherwise -> let 
            (ctx', res', _)   = dce (ctx, [], [Assign (l:ls) (r:rs ++ [Right Break])])
            (ctx'', res'', _) = dce (ctx', [], [Assign (l:ls) (r:rs ++ [Right Break])]) in
                if res' /= res''
                    then dce (ctx'', es, fs)
                    else dce (ctx'', res'' ++ es, init fs)

    Block xs -> dce (f, Block s : es, init fs)
        where  (f, s, _) = dce (ctx, [], xs)
    
    Define name ps body -> if Maybe.isNothing $ getValue ctx name
        then dce (ctx, es, init fs)
        else dce (ctx, Define name ps (Block body') : es, init fs) 
        where (_, body', _) = dce (createContext, [], [body])

    For i l1 l2 l3 body -> let (ctx', body', _) = dce (ctx, [], [body]) in if null body'
        then dce (ctx, es, init fs)
        else let (ctx'', body'', _) = dce (ctx', [], [body]) 
            in if body'' == body'
                then dce (ctx', For i l1 l2 l3 (Block body') : es, init fs)
                else dce (ctx'', es, fs)

    ForEach i is body -> let (ctx', body', _) = dce (ctx, [], [body]) in if null body'
        then dce (ctx, es, init fs)
        else let (ctx'', body'', _) = dce (ctx', [], [body]) 
            in if body'' == body'
                then dce (ctx', ForEach i is (Block body') : es, init fs)
                else dce (ctx'', es, fs)
    
    Function name args -> if Maybe.isNothing $ getValue ctx name
        then dce (ctx, es, init fs)
        else dce (addsCtx ctx args, Function name args : es, init fs)

    l@(Lambda{}) -> dce (ctx, l:es, init fs)

    l@(ALambda{}) -> dce (ctx, l:es, init fs)

    IfThenElse cond thenb elseb -> case elseb of
        Nothing -> let (ctx',thenb',_) = dce (ctx, [], [thenb])
            in if null thenb'
            then dce (ctx, es, init fs)
            else dce (addCtx ctx' cond, IfThenElse cond (Block thenb') Nothing : es, init fs)
        Just e -> let (ctx', thenb', _) = dce (ctx, [], [thenb])
                      (ctx'', elseb', _) = dce (ctx', [], [e])
            in if null thenb' && null elseb'
            then dce (ctx, es, init fs)
            else dce (addCtx ctx'' cond, IfThenElse cond (Block thenb') (Just (Block elseb')) : es, init fs)

    Local _ -> dce (ctx, es, init fs)

    Return v -> dce (addCtx ctx v, last fs : es, init fs)
    
    Repeat body cond -> let (ctx', body', _) = dce (ctx, [], [body])
        in if null body'
            then dce (ctx, es, init fs)
            else let (ctx'', body'', _) = dce (ctx', [], [body]) 
            in if body'' == body'
                then dce (addCtx ctx' cond, Repeat (Block body') cond : es, init fs)
                else dce (ctx'', es, fs)

    While cond body -> let (ctx', body', _) = dce (ctx, [], [body])
        in if null body'
            then dce (ctx, es, init fs)
            else let (ctx'', body'', _) = dce (ctx', [], [body]) 
            in if body'' == body'
                then dce (addCtx ctx' cond, While cond (Block body') : es, init fs)
                else dce (ctx'', es, fs)

    Goto _ -> dce (ctx, es, init fs)
    Label _ -> dce (ctx, es, init fs)
    Break -> dce (ctx, es, init fs)
