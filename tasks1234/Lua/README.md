# Tasks 1, 2, 3, 4

## Already done:

#### Data types

1. No type definition before execution except constants. 
2. Hash table is implemented by map of literals.  
3. Global\local scope are maps.

#### Parser

1. Set priorities to operations.
2. Basic string content.
3. Correct expressions without parentheses.

#### Interpreter

1. Basic intepreter.

#### Optimisation

1. Dead code elimination.

## Planned:

#### Parser

1. More string content.
2. Values by default in for cycle.

#### Interpreter

1. Break, label, goto statements.
